package edu.pa165.wineinventoryservice.service;

import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.dto.WineUpdateRequest;
import edu.pa165.wineinventoryservice.mapping.WineDtoMapper;
import edu.pa165.wineinventoryservice.model.Wine;
import edu.pa165.wineinventoryservice.monitoring.WineInventoryMetricsEndpoint;
import edu.pa165.wineinventoryservice.persistence.WineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class WineServiceImpl implements WineService {
    private final WineRepository repository;
    private final WineInventoryMetricsEndpoint metricsEndpoint;
    private final WineDtoMapper wineDtoMapper;
    @Autowired
    WineServiceImpl(WineRepository repository, WineInventoryMetricsEndpoint metricsEndpoint, WineDtoMapper wineDtoMapper) {
        this.repository = repository;
        this.metricsEndpoint = metricsEndpoint;
        this.wineDtoMapper = wineDtoMapper;
    }

    @Override
    public WineResponse createWine(WineCreateRequest request) {
        Wine newWine = wineDtoMapper.convertFromRequestForCreate( request);
        repository.save(newWine);
        metricsEndpoint.getBottlesAddedCount();
        return wineDtoMapper.convertToResponse(newWine);
    }

    @Override
    @Transactional(readOnly = true)
    public WineResponse getWine(Long id) {
        Wine wine = repository.findById(id).orElse(null);
        if (wine == null) {
            return null;
        }
        return wineDtoMapper.convertToResponse(wine);
    }

    @Override
    @Transactional(readOnly = true)
    public List<WineResponse> getAllWines() {
        List<Wine> allWines = repository.findAll();
        return allWines.stream()
                .map(wineDtoMapper::convertToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public WineResponse updateWine(Long id, WineUpdateRequest request) {
        Wine wineToUpdate = wineDtoMapper.convertFromRequestForUpdate(request,repository.findById(id).orElseThrow(() -> new RuntimeException("Wine not found")));
        repository.save(wineToUpdate);
        return wineDtoMapper.convertToResponse(wineToUpdate);
    }

    @Override
    public void updateWinesQuantity(Map<Long, Integer> qtyMap) {
        for (Map.Entry<Long, Integer> entry : qtyMap.entrySet()) {
            Wine wineToUpdate = repository.findById(entry.getKey()).orElseThrow(() -> new RuntimeException("Wine not found"));
            wineToUpdate.setQuantityLeft(wineToUpdate.getQuantityLeft() - entry.getValue());
            repository.save(wineToUpdate);
        }
    }

    @Override
    public void deleteWine(Long id) {
        repository.deleteById(id);
    }

}