package edu.pa165.wineinventoryservice.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "AllReview")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer rating;
    @Column(length = 500)
    private String comment;
    private Long wineId;
}