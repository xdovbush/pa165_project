package edu.pa165.wineinventoryservice.controller;

import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.dto.WineUpdateRequest;
import edu.pa165.wineinventoryservice.service.WineService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static edu.pa165.wineinventoryservice.api.WineInventoryServiceApi.WINE_INTERNAL_ENDPOINT;

@RestController
@RequestMapping(WINE_INTERNAL_ENDPOINT)
public class WineInternalController {

    private final WineService service;

    @Autowired
    public WineInternalController( WineService service) {
        this.service = service;
    }

    @PostMapping
    @Operation(
            summary = "Create new wine bottles",
            description = """
                    Receives WineCreateRequest DTO that will be converted to entity and saved.
                    All fields of DTO must be filled.
                    Returns WineResponse DTO that contains saved entity data.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Wine was created",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    WineResponse createWine(@RequestBody WineCreateRequest request) {
        return this.service.createWine(request);
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Updates existing wine",
            description = """
                    Receives wine ID as URL parameter and WineUpdateRequest DTO as requested data update.
                    All fields of DTO must be filled
                    Returns WineResponse DTO that contains updated entity data.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine was updated",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineResponse.class))
                            }
                    )
            }
    )
    public WineResponse updateWine(@PathVariable Long id, @RequestBody WineUpdateRequest request) {
        return this.service.updateWine(id, request);
    }

    @PutMapping("/quantity")
    @Operation(
            summary = "Updates existing wine quantity",
            description = """
                    This endpoint exist for wine update of wine quantity.
                    Receives map of wine id and new quantity.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    public void updateWinesQuantity(@RequestBody Map<Long, Integer> qtyMap) {
        this.service.updateWinesQuantity(qtyMap);
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Deletes existing wine by ID",
            description = """
                    Deletes wine by given as URL param wine ID.
                    This action is idempotent.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    public void deleteWine(@PathVariable Long id) {
        this.service.deleteWine(id);
    }

}
