package edu.pa165.wineinventoryservice.controller;

import edu.pa165.wineinventoryservice.dto.ReviewCreateRequest;
import edu.pa165.wineinventoryservice.dto.ReviewResponse;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.service.ReviewService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static edu.pa165.wineinventoryservice.api.WineInventoryServiceApi.REVIEW_ENDPOINT;

@RestController
@RequestMapping(REVIEW_ENDPOINT)
public class ReviewController {

    private final ReviewService service;

    @Autowired
    ReviewController(ReviewService service) {
        this.service = service;
    }

    @PostMapping
    @Operation(
            summary = "Create new wine review",
            description = """
                    Receives ReviewCreateRequest DTO that will be converted to entity and saved.
                    All fields of DTO must be filled.
                    Returns ReviewResponse DTO that contains saved entity data.
                    """,
            security = @SecurityRequirement(name = "customer", scopes = {"SCOPE_test_2"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Wine review was created",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = ReviewResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public ReviewResponse createReview(@RequestBody ReviewCreateRequest request) {
        return this.service.createReview(request);
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Gets wine review info by batch ID",
            description = """
                    Returns wine review by given as URL param wine review ID.
                    If such wine review does not exist, returns empty response.
                    """,
            security = @SecurityRequirement(name = "customer", scopes = {"SCOPE_test_2"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Review was found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = ReviewResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public ReviewResponse getReview(@PathVariable("id") Long id) {
        return this.service.getReview(id);
    }

    @GetMapping
    @Operation(
            summary = "Gets all wine reviews",
            description = """
                    Returns a list of existing wine reviews.
                    """,
            security = @SecurityRequirement(name = "customer", scopes = {"SCOPE_test_2"})
    )
    @ResponseStatus(HttpStatus.OK)
    public List<ReviewResponse> getReviews() {
        return this.service.getReviews();
    }
}
