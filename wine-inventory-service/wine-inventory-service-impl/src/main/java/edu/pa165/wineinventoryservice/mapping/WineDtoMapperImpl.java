package edu.pa165.wineinventoryservice.mapping;

import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.dto.WineUpdateRequest;
import edu.pa165.wineinventoryservice.model.Wine;
import org.springframework.stereotype.Component;

@Component
public class WineDtoMapperImpl implements WineDtoMapper {

    @Override
    public WineResponse convertToResponse(Wine wine) {
        return WineResponse.builder()
                .id(wine.getId())
                .name(wine.getName())
                .dryness(wine.getDryness())
                .year(wine.getYear())
                .sugarLevel(wine.getSugarLevel())
                .acidity(wine.getAcidity())
                .alcoholLevel(wine.getAlcoholLevel())
                .litersInBottle(wine.getLitersInBottle())
                .batchNumber(wine.getBatchNumber())
                .pricePerBottle(wine.getPricePerBottle())
                .quantityLeft(wine.getQuantityLeft())
                .build();
    }

    @Override
    public Wine convertFromRequestForCreate(WineCreateRequest request) {
        return Wine.builder()
                .name(request.name())
                .dryness(request.dryness())
                .year(request.year())
                .sugarLevel(request.sugarLevel())
                .acidity(request.acidity())
                .alcoholLevel(request.alcoholLevel())
                .litersInBottle(request.litersInBottle())
                .batchNumber(request.batchNumber())
                .pricePerBottle(request.pricePerBottle())
                .quantityLeft(request.quantityLeft())
                .build();
    }

    @Override
    public Wine convertFromRequestForUpdate(WineUpdateRequest request, Wine originalWine) {
        return Wine.builder()
                .id(originalWine.getId())
                .name(request.name() != null ? request.name() : originalWine.getName())
                .dryness(request.dryness() != null ? request.dryness() : originalWine.getDryness())
                .year(request.year() != null ? request.year() : originalWine.getYear())
                .sugarLevel(request.sugarLevel() != null ? request.sugarLevel() : originalWine.getSugarLevel())
                .acidity(request.acidity() != null ? request.acidity() : originalWine.getAcidity())
                .alcoholLevel(request.alcoholLevel() != null ? request.alcoholLevel() : originalWine.getAlcoholLevel())
                .litersInBottle(request.litersInBottle() != null ? request.litersInBottle() : originalWine.getLitersInBottle())
                .batchNumber(request.batchNumber() != null ? request.batchNumber() : originalWine.getBatchNumber())
                .pricePerBottle(request.pricePerBottle() != null ? request.pricePerBottle() : originalWine.getPricePerBottle())
                .quantityLeft(request.quantityLeft() != null ? request.quantityLeft() : originalWine.getQuantityLeft())
                .build();
    }
}