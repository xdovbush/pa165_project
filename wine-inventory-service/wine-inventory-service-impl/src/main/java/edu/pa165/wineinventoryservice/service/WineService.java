package edu.pa165.wineinventoryservice.service;

import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.dto.WineUpdateRequest;
import jakarta.annotation.Nullable;

import java.util.List;
import java.util.Map;

public interface WineService {
    WineResponse createWine(WineCreateRequest request);

    @Nullable
    WineResponse getWine(Long id);

    List<WineResponse> getAllWines();

    WineResponse updateWine(Long id, WineUpdateRequest request);

    void updateWinesQuantity(Map<Long, Integer> qtyMap);

    void deleteWine(Long id);
}
