package edu.pa165.wineinventoryservice.mapping;

import edu.pa165.wineinventoryservice.dto.ReviewCreateRequest;
import edu.pa165.wineinventoryservice.dto.ReviewResponse;
import edu.pa165.wineinventoryservice.dto.ReviewUpdateRequest;
import edu.pa165.wineinventoryservice.model.Review;
import org.springframework.stereotype.Component;

@Component
public class ReviewDtoMapperImpl implements ReviewDtoMapper {

    @Override
    public ReviewResponse convertToResponse(Review review) {
        return ReviewResponse.builder()
                .id(review.getId())
                .rating(review.getRating())
                .comment(review.getComment())
                .wineId(review.getWineId())
                .build();
    }

    @Override
    public Review convertFromRequestForCreate(ReviewCreateRequest request) {
        return Review.builder()
                .rating(request.rating())
                .comment(request.comment())
                .wineId(request.wineId())
                .build();
    }

    @Override
    public Review convertFromRequestForUpdate(ReviewUpdateRequest request, Review originalReview) {
        return Review.builder()
                .id(originalReview.getId())
                .rating(request.rating() != null ? request.rating() : originalReview.getRating())
                .comment(request.comment() != null ? request.comment() : originalReview.getComment())
                .wineId(request.wineId() != null ? request.wineId() : originalReview.getWineId())
                .build();
    }
}
