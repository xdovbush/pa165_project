package edu.pa165.wineinventoryservice.datamanager;

import edu.pa165.wineinventoryservice.model.Review;
import edu.pa165.wineinventoryservice.model.Wine;
import edu.pa165.wineinventoryservice.persistence.ReviewRepository;
import edu.pa165.wineinventoryservice.persistence.WineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Profile({"dev", "docker"})
@Transactional
public class DataManager implements ApplicationRunner {

    private final WineRepository wineRepository;
    private final ReviewRepository reviewRepository;

    @Autowired
    public DataManager(
            WineRepository wineRepository,
            ReviewRepository reviewRepository
    ) {
        this.wineRepository = wineRepository;
        this.reviewRepository = reviewRepository;
    }

    public void cleanDb() {
        wineRepository.deleteAll();
        reviewRepository.deleteAll();
    }

    public void fillDb() {
        if (wineRepository.count() == 0L && reviewRepository.count() == 0L)
        {
            wineRepository.save(
                    Wine.builder()
                    .name("Chardonnay")
                    .dryness("Dry")
                    .year(2023)
                    .sugarLevel(5.0)
                    .acidity(6.5)
                    .alcoholLevel(13.5)
                    .litersInBottle(0.75)
                    .batchNumber(1L)
                    .pricePerBottle(25.0)
                    .quantityLeft(50)
                    .build()
            );
            wineRepository.save(
                    Wine.builder()
                            .name("Merlot")
                            .dryness("Medium")
                            .year(2023)
                            .sugarLevel(7.0)
                            .acidity(6.0)
                            .alcoholLevel(14.0)
                            .litersInBottle(0.75)
                            .batchNumber(2L)
                            .pricePerBottle(30.0)
                            .quantityLeft(40)
                            .build()
            );
            wineRepository.save(
                    Wine.builder()
                            .name("Cabernet Sauvignon")
                            .dryness("Dry")
                            .year(2023)
                            .sugarLevel(4.5)
                            .acidity(6.8)
                            .alcoholLevel(14.5)
                            .litersInBottle(0.75)
                            .batchNumber(3L)
                            .pricePerBottle(35.0)
                            .quantityLeft(60)
                            .build()
            );
            wineRepository.save(
                    Wine.builder()
                            .name("Pinot Noir")
                            .dryness("Dry")
                            .year(2023)
                            .sugarLevel(4.0)
                            .acidity(6.3)
                            .alcoholLevel(13.0)
                            .litersInBottle(0.75)
                            .batchNumber(4L)
                            .pricePerBottle(40.0)
                            .quantityLeft(30)
                            .build()
            );
            List<Wine> createdWines = wineRepository.findAll();
            if (createdWines.size() >= 4) {
                reviewRepository.save(
                        Review.builder()
                                .wineId(createdWines.get(0).getId())
                                .rating(5)
                                .comment("Probably the best one, since it has the lowest ID.")
                                .build()
                );
                reviewRepository.save(
                        Review.builder()
                                .wineId(createdWines.get(1).getId())
                                .rating(4)
                                .comment("Good, but wine from Mikulov is better.")
                                .build()
                );
                reviewRepository.save(
                        Review.builder()
                                .wineId(createdWines.get(2).getId())
                                .rating(1)
                                .comment("Branik is superior to this.")
                                .build()
                );
                reviewRepository.save(
                        Review.builder()
                                .wineId(createdWines.get(3).getId())
                                .rating(5)
                                .comment("Nice.")
                                .build()
                );
            }
        }
    }

    @Autowired
    public void run(ApplicationArguments args) {
        fillDb();
    }
}
