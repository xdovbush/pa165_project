package edu.pa165.wineinventoryservice.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "AllWine")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Wine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 100)
    private String name;
    @Column(length = 25)
    private String dryness;
    @Column(name = "productionYear")
    private Integer year;
    private Double sugarLevel;
    private Double acidity;
    private Double alcoholLevel;
    private Double litersInBottle;
    private Long batchNumber;
    private Double pricePerBottle;
    private Integer quantityLeft;
}