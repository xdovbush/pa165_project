package edu.pa165.wineinventoryservice.mapping;

import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.dto.WineUpdateRequest;
import edu.pa165.wineinventoryservice.model.Wine;

public interface WineDtoMapper {
    WineResponse convertToResponse(Wine wine);
    Wine convertFromRequestForCreate(WineCreateRequest request);
    Wine convertFromRequestForUpdate(WineUpdateRequest request, Wine originalWine);
}