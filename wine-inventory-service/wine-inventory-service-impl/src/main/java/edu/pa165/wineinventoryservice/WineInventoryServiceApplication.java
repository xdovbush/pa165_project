package edu.pa165.wineinventoryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static edu.pa165.wineinventoryservice.api.WineInventoryServiceApi.NAME;

/**
 * Wine inventory service runner
 *
 * @author andrii.dovbush
 * @since 2024.03.10
 */
@SpringBootApplication
public class WineInventoryServiceApplication {

    public static void main(String[] args) {
        setStaticCommonProperties();

        SpringApplication.run(WineInventoryServiceApplication.class, args);
    }

    private static void setStaticCommonProperties() {
        System.setProperty("spring.application.name", NAME);
    }

}
