package edu.pa165.wineinventoryservice.mapping;

import edu.pa165.wineinventoryservice.dto.ReviewCreateRequest;
import edu.pa165.wineinventoryservice.dto.ReviewResponse;
import edu.pa165.wineinventoryservice.dto.ReviewUpdateRequest;
import edu.pa165.wineinventoryservice.model.Review;

public interface ReviewDtoMapper {
    ReviewResponse convertToResponse(Review review);
    Review convertFromRequestForCreate(ReviewCreateRequest request);
    Review convertFromRequestForUpdate(ReviewUpdateRequest request, Review originalReview);
}
