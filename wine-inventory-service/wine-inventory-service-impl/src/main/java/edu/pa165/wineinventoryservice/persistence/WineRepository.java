package edu.pa165.wineinventoryservice.persistence;

import edu.pa165.wineinventoryservice.model.Wine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WineRepository extends JpaRepository<Wine, Long> {
    @Query("SELECT w FROM Wine w WHERE w.name = ?1")
    List<Wine> findByName(String name);

    @Query("SELECT w FROM Wine w WHERE w.quantityLeft < ?1")
    List<Wine> findByQuantityLeftLessThan(Integer quantity);
}
