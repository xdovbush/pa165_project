package edu.pa165.wineinventoryservice.monitoring;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Endpoint(id = "wineInventoryMetrics")
public class WineInventoryMetricsEndpoint {

    private final MeterRegistry meterRegistry;
    private final Counter bottlesAddedCounter;
    private final Map<Integer, Counter> reviewCountersByRating = new HashMap<>();

    public WineInventoryMetricsEndpoint(MeterRegistry meterRegistry) {
        this.bottlesAddedCounter = meterRegistry.counter("wine_inventory.bottles_added");
        this.meterRegistry = meterRegistry;
    }

    @ReadOperation
    public Map<String, Number> wineInventoryMetrics() {
        Map<String, Number> metrics = new HashMap<>();
        metrics.put("bottlesAdded", bottlesAddedCounter.count());
        reviewCountersByRating.forEach((rating, counter) -> metrics.put("reviewsRating" + rating, counter.count()));
        return metrics;
    }

    public double getBottlesAddedCount() {
        return bottlesAddedCounter.count();
    }

    public void incrementReviewCounter(int rating) {
        reviewCountersByRating.computeIfAbsent(rating, r -> Counter.builder("reviews_by_rating")
                        .tag("rating", String.valueOf(r))
                        .register(meterRegistry))
                .increment();
    }
}