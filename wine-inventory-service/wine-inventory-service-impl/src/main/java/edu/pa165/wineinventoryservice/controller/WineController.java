package edu.pa165.wineinventoryservice.controller;

import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.service.WineService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static edu.pa165.wineinventoryservice.api.WineInventoryServiceApi.WINE_ENDPOINT;

@RestController
@OpenAPIDefinition(
        info = @Info(title = "Wine Inventory Service",
                version = "1.0",
                description = """
                        Service for wine batch and recipe management. The API has operations for:
                        - Create wine
                        - Read wine: all/by ID
                        - Update wine: by ID
                        - Open wine: by ID
                        - Create wine review
                        - Read wine review: all/by ID
                        - Update wine review: by ID
                        - Delete wine review: by ID
                        - Seed dummy DB data
                        - Clean DB data
                        """,
                contact = @Contact(name = "Nicolas Quillien", email = "556685@mail.muni.cz")
        ),
        servers = @Server(description = "Localhost", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "9084"),
        })
)
@Tag(name = "Wine inventory management", description = "Microservice for wine inventory management")
@RequestMapping(WINE_ENDPOINT)
public class WineController {

    private final WineService service;

    @Autowired
    WineController( WineService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Gets wine info by batch ID",
            description = """
                    Returns wine by given as URL param wine ID.
                    If such wine does not exist, returns empty response.
                    """,
            security = @SecurityRequirement(name = "customer", scopes = {"SCOPE_test_2"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine was found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public WineResponse getWine(@PathVariable("id") Long id) {
        return this.service.getWine(id);
    }

    @GetMapping
    @Operation(
            summary = "Gets all wines",
            description = """
                    Returns a list of existing wines.
                    """,
            security = @SecurityRequirement(name = "customer", scopes = {"SCOPE_test_2"})
    )
    @ResponseStatus(HttpStatus.OK)
    public List<WineResponse> getAllWines() {
        return this.service.getAllWines();
    }

}
