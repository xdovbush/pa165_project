package edu.pa165.wineinventoryservice.controller;

import edu.pa165.wineinventoryservice.dto.ReviewResponse;
import edu.pa165.wineinventoryservice.dto.ReviewUpdateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.service.ReviewService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static edu.pa165.wineinventoryservice.api.WineInventoryServiceApi.REVIEW_INTERNAL_ENDPOINT;

@RestController
@RequestMapping(REVIEW_INTERNAL_ENDPOINT)
public class ReviewInternalController {

    private final ReviewService service;

    @Autowired
    ReviewInternalController(ReviewService service) {
        this.service = service;
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Updates existing wine review",
            description = """
                    Receives wine review ID as URL parameter and ReviewUpdateRequest DTO as requested data update.
                    All fields of DTO must be filled
                    Returns ReviewResponse DTO that contains updated entity data.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine review was updated",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = ReviewResponse.class))
                            }
                    )
            }
    )
    public ReviewResponse updateReview(@PathVariable Long id, @RequestBody ReviewUpdateRequest request) {
        return this.service.updateReview(id, request);
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Deletes existing wine review by ID",
            description = """
                    Deletes wine review by given as URL param wine review ID.
                    This action is idempotent.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    public void deleteReview(@PathVariable Long id) {
        this.service.deleteReview(id);
    }

}
