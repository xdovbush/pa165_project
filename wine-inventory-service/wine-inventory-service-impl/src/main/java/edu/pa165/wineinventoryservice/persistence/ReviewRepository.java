package edu.pa165.wineinventoryservice.persistence;

import edu.pa165.wineinventoryservice.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {

    List<Review> findReviewsByRating(Integer rating);

    List<Review> findReviewsByWineId(Long wineId);
}
