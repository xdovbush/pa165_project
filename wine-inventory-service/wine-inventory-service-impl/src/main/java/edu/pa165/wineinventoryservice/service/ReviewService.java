package edu.pa165.wineinventoryservice.service;

import edu.pa165.wineinventoryservice.dto.ReviewCreateRequest;
import edu.pa165.wineinventoryservice.dto.ReviewResponse;
import edu.pa165.wineinventoryservice.mapping.ReviewDtoMapper;
import edu.pa165.wineinventoryservice.dto.ReviewUpdateRequest;
import edu.pa165.wineinventoryservice.model.Review;
import edu.pa165.wineinventoryservice.monitoring.WineInventoryMetricsEndpoint;
import edu.pa165.wineinventoryservice.persistence.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ReviewService {

    private final ReviewRepository reviewRepository;
    private final WineInventoryMetricsEndpoint metricsEndpoint;
    private final ReviewDtoMapper reviewDtoMapper;

    @Autowired
    public ReviewService(ReviewRepository reviewRepository, WineInventoryMetricsEndpoint metricsEndpoint, ReviewDtoMapper reviewDtoMapper) {
        this.reviewRepository = reviewRepository;
        this.metricsEndpoint = metricsEndpoint;
        this.reviewDtoMapper = reviewDtoMapper;
    }


    public ReviewResponse createReview(ReviewCreateRequest request) {
        Review review = reviewDtoMapper.convertFromRequestForCreate(request);
        review = reviewRepository.save(review);
        metricsEndpoint.incrementReviewCounter(request.rating());
        return reviewDtoMapper.convertToResponse(review);
    }

    public ReviewResponse updateReview(Long id, ReviewUpdateRequest request) {
        Review originalReview = reviewRepository.findById(id).orElseThrow();
        Review updatedReview = reviewDtoMapper.convertFromRequestForUpdate(request, originalReview);
        updatedReview = reviewRepository.save(updatedReview);
        return reviewDtoMapper.convertToResponse(updatedReview);
    }

    @Transactional(readOnly = true)
    public ReviewResponse getReview(Long id) {
        Review review = reviewRepository.findById(id).orElseThrow();
        return reviewDtoMapper.convertToResponse(review);
    }

    @Transactional(readOnly = true)
    public List<ReviewResponse> getReviews() {
        List<Review> reviews = reviewRepository.findAll();
        return reviews.stream()
                .map(reviewDtoMapper::convertToResponse)
                .toList();
    }

    public void deleteReview(Long id) {
        reviewRepository.deleteById(id);
    }
}