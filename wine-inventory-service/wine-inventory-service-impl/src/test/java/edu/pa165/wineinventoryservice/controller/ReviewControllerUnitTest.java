package edu.pa165.wineinventoryservice.controller;

import edu.pa165.wineinventoryservice.dto.ReviewCreateRequest;
import edu.pa165.wineinventoryservice.dto.ReviewResponse;
import edu.pa165.wineinventoryservice.service.ReviewService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ReviewControllerUnitTest {

    @Mock
    private ReviewService reviewService;

    private ReviewController reviewController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        reviewController = new ReviewController(reviewService);
    }

    @AfterEach
    public void tearDown() {
        Mockito.framework().clearInlineMocks();
    }

    @Test
    void createReview() {
        ReviewCreateRequest request = new ReviewCreateRequest(5, "Excellent", 1L);
        ReviewResponse expectedResponse = new ReviewResponse(
                1L,
                5,
                "Excellent review",
                1L
        );

        when(reviewService.createReview(any(ReviewCreateRequest.class))).thenReturn(expectedResponse);

        ReviewResponse response = reviewController.createReview(request);

        assertNotNull(response);
        assertEquals(expectedResponse, response);
        verify(reviewService).createReview(request);
    }

    @Test
    void getReview() {
        Long reviewId = 1L;
        ReviewResponse mockResponse = new ReviewResponse(
                reviewId,
                5,
                "Excellent",
                1L
        );

        when(reviewService.getReview(reviewId)).thenReturn(mockResponse);

        ReviewResponse result = reviewController.getReview(reviewId);

        assertNotNull(result);
        assertEquals(mockResponse.id(), result.id());
        assertEquals(mockResponse.rating(), result.rating());
        assertEquals(mockResponse.comment(), result.comment());
        assertEquals(mockResponse.wineId(), result.wineId());

        verify(reviewService).getReview(reviewId);
    }
}