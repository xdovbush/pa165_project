package edu.pa165.wineinventoryservice.controller;

import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.service.WineService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class WineControllerTest {

    @Mock
    private WineService wineService;

    private WineController wineController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        wineController = new WineController(wineService);
    }

    @AfterEach
    public void tearDown() {
        Mockito.framework().clearInlineMocks();
    }

    @Test
    void getWine() {
        Long wineId = 1L;
        WineResponse mockResponse = new WineResponse(
                wineId,
                "Merlot",
                "Dry",
                2019,
                2.5,
                3.5,
                12.5,
                0.75,
                1001L,
                120.0,
                50
        );

        when(wineService.getWine(wineId)).thenReturn(mockResponse);

        WineResponse response = wineController.getWine(wineId);

        assertNotNull(response);
        assertEquals(mockResponse.id(), response.id());
        assertEquals(mockResponse.name(), response.name());

        verify(wineService).getWine(wineId);
    }

    @Test
    void getAllWines() {
        List<WineResponse> mockResponseList = List.of(
                new WineResponse(
                        1L,
                        "Merlot",
                        "Dry",
                        2019,
                        2.5,
                        3.5,
                        12.5,
                        0.75,
                        1001L,
                        120.0,
                        50
                ),

                new WineResponse(
                        2L,
                        "Cabernet Sauvignon",
                        "Semi-Dry",
                        2018,
                        2.0,
                        3.0,
                        13.0,
                        0.75,
                        1002L,
                        150.0,
                        60
                )
        );

        when(wineService.getAllWines()).thenReturn(mockResponseList);

        List<WineResponse> responseList = wineController.getAllWines();

        assertNotNull(responseList);
        assertEquals(mockResponseList.size(), responseList.size());
        assertEquals(mockResponseList.get(0).id(), responseList.get(0).id());
        assertEquals(mockResponseList.get(1).id(), responseList.get(1).id());

        verify(wineService).getAllWines();
    }
}