package edu.pa165.wineinventoryservice.persistence;

import edu.pa165.wineinventoryservice.e2e.annotation.FullStackTest;
import edu.pa165.wineinventoryservice.e2e.annotation.UseTestData;
import edu.pa165.wineinventoryservice.e2e.environment.ReviewFullStackTestEnvironment;
import edu.pa165.wineinventoryservice.model.Review;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

@FullStackTest
@UseTestData
public class ReviewRepositoryFullStackTest {
    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    //@Qualifier("getReviewFullStackEnvironment")
    private ReviewFullStackTestEnvironment environment;

    @BeforeAll
    void initialize() {
        environment.initialize();
    }

    @AfterAll
    void cleanUp() {
        environment.cleanUp();
    }

    @Test
    void getAllReviews() {
        List<Review> reviews = reviewRepository.findAll();

        assertThat(reviews).isNotEmpty();
    }

    @Test
    void findReviewsByRating() {
        Integer testRating = 5;
        List<Review> reviews = reviewRepository.findReviewsByRating(testRating);

        assertSoftly(softly -> reviews.forEach(
                r -> softly.assertThat(r.getRating()).isEqualTo(testRating)
        ));
    }

    @Test
    void findReviewsByWineId() {
        Long testWineId = 1L;
        List<Review> reviews = reviewRepository.findReviewsByWineId(testWineId);

        assertSoftly(softly -> reviews.forEach(
                r -> softly.assertThat(r.getWineId()).isEqualTo(testWineId)
        ));
    }
}