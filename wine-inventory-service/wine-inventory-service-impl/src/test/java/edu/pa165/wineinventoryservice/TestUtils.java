package edu.pa165.wineinventoryservice;

import edu.pa165.wineinventoryservice.model.Review;
import edu.pa165.wineinventoryservice.model.Wine;

public class TestUtils {

    public static Wine createDummyWine() {
        return createDummyWine("Test Wine");
    }

    public static Wine createDummyWine(String name) {
        return Wine.builder()
                .name(name)
                .dryness("Dry")
                .year(2019)
                .sugarLevel(2.5)
                .acidity(3.5)
                .alcoholLevel(12.5)
                .litersInBottle(0.75)
                .batchNumber(1001L)
                .pricePerBottle(120.0)
                .quantityLeft(5)
                .build();
    }

    public static Review createDummyReview() {
        return createDummyReview(5, "Great wine!", 1L);
    }

    public static Review createDummyReview(Integer rating, String comment, Long wineId) {
        return Review.builder()
                .rating(rating)
                .comment(comment)
                .wineId(wineId)
                .build();
    }
}