package edu.pa165.wineinventoryservice.controller;

import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.dto.WineUpdateRequest;
import edu.pa165.wineinventoryservice.service.WineService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class WineInternalControllerTest {

    @Mock
    private WineService wineService;

    private WineInternalController wineInternalController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        wineInternalController = new WineInternalController(wineService);
    }

    @AfterEach
    public void tearDown() {
        Mockito.framework().clearInlineMocks();
    }

    @Test
    void createWine() {
        WineCreateRequest request = new WineCreateRequest(
                "Pinot Noir",
                "Dry",
                2018,
                2.5,
                3.5,
                13.5,
                0.75,
                1001L,
                150.0,
                100
        );

        WineResponse expectedResponse = new WineResponse(
                1L,
                "Pinot Noir",
                "Dry",
                2018,
                2.5,
                3.5,
                13.5,
                0.75,
                1001L,
                150.0,
                100
        );

        when(wineService.createWine(any(WineCreateRequest.class))).thenReturn(expectedResponse);

        WineResponse response = wineInternalController.createWine(request);

        assertNotNull(response);
        assertEquals(expectedResponse, response);
        verify(wineService).createWine(any(WineCreateRequest.class));
    }

    @Test
    void updateWine() {
        Long id = 1L;
        WineUpdateRequest request = new WineUpdateRequest(
                "Merlot",
                "Semi-Dry",
                2019,
                3.0,
                3.5,
                14.0,
                0.75,
                1002L,
                120.0,
                150
        );

        WineResponse expectedResponse = new WineResponse(
                id,
                "Merlot",
                "Semi-Dry",
                2019,
                3.0,
                3.5,
                14.0,
                0.75,
                1002L,
                120.0,
                150
        );

        when(wineService.updateWine(eq(id), any(WineUpdateRequest.class))).thenReturn(expectedResponse);

        WineResponse response = wineInternalController.updateWine(id, request);

        assertNotNull(response);
        assertEquals(expectedResponse, response);
        verify(wineService).updateWine(eq(id), any(WineUpdateRequest.class));
    }

    @Test
    void deleteWine() {
        Long id = 1L;
        doNothing().when(wineService).deleteWine(id);

        wineInternalController.deleteWine(id);

        verify(wineService).deleteWine(id);
    }
}