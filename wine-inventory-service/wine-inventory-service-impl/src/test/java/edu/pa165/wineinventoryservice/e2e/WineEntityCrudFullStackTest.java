package edu.pa165.wineinventoryservice.e2e;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.dto.WineUpdateRequest;
import edu.pa165.wineinventoryservice.e2e.annotation.FullStackTest;
import edu.pa165.wineinventoryservice.e2e.annotation.UseTestData;
import edu.pa165.wineinventoryservice.e2e.environment.WineFullStackTestEnvironment;
import edu.pa165.wineinventoryservice.model.Wine;
import edu.pa165.wineinventoryservice.persistence.WineRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static edu.pa165.wineinventoryservice.api.WineInventoryServiceApi.WINE_INTERNAL_ENDPOINT;
import static edu.pa165.wineinventoryservice.api.WineInventoryServiceApi.WINE_ENDPOINT;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FullStackTest
@UseTestData
public class WineEntityCrudFullStackTest {
    @Autowired
    private WineRepository wineRepository;

    @Autowired
    @Qualifier("getWineFullStackEnvironment")
    private WineFullStackTestEnvironment environment;

    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    void initialize() {
        environment.initialize();
    }

    @AfterAll
    void cleanUp() {
        environment.cleanUp();
    }

    @Test
    void testCreateWineEntity() {
        WineCreateRequest request = WineCreateRequest.builder()
                .name("Test Wine")
                .dryness("Dry")
                .year(2020)
                .sugarLevel(1.2)
                .acidity(3.4)
                .alcoholLevel(13.5)
                .litersInBottle(0.75)
                .batchNumber(123456L)
                .pricePerBottle(15.99)
                .quantityLeft(100)
                .build();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(post(WINE_INTERNAL_ENDPOINT)
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isCreated())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
        }

        assertNotNull(response);

        WineResponse wineResponse = convertServletResponseToDto(response);
        Wine expectedWine = wineRepository.findById(wineResponse.id()).orElse(null);

        assertSoftly(softly -> {
            softly.assertThat(wineResponse.name())
                    .isEqualTo(request.name());
            softly.assertThat(wineResponse.dryness())
                    .isEqualTo(request.dryness());
            softly.assertThat(wineResponse.year())
                    .isEqualTo(request.year());
            softly.assertThat(wineResponse.sugarLevel())
                    .isEqualTo(request.sugarLevel());
            softly.assertThat(wineResponse.acidity())
                    .isEqualTo(request.acidity());
            softly.assertThat(wineResponse.alcoholLevel())
                    .isEqualTo(request.alcoholLevel());
            softly.assertThat(wineResponse.litersInBottle())
                    .isEqualTo(request.litersInBottle());
            softly.assertThat(wineResponse.batchNumber())
                    .isEqualTo(request.batchNumber());
            softly.assertThat(wineResponse.pricePerBottle())
                    .isEqualTo(request.pricePerBottle());
            softly.assertThat(wineResponse.quantityLeft())
                    .isEqualTo(request.quantityLeft());
        });

        assertNotNull(expectedWine);
        assertSoftly(softly -> {
            softly.assertThat(expectedWine.getName())
                    .isEqualTo(request.name());
            softly.assertThat(expectedWine.getDryness())
                    .isEqualTo(request.dryness());
        });
    }

    @Test
    void testUpdateWineEntity() throws Exception {
        Long wineId = 1L;

        WineUpdateRequest request = WineUpdateRequest.builder()
                .name("Updated Wine")
                .dryness("Semi-dry")
                .year(2021)
                .sugarLevel(1.3)
                .acidity(3.5)
                .alcoholLevel(13.6)
                .litersInBottle(0.75)
                .batchNumber(123457L)
                .pricePerBottle(16.99)
                .quantityLeft(99)
                .build();

        MockHttpServletResponse response = mockMvc.perform(put(WINE_INTERNAL_ENDPOINT + "/{id}", wineId)
                        .content(convertRequestToJson(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        WineResponse wineResponse = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            softly.assertThat(wineResponse.name())
                    .isEqualTo(request.name());
            softly.assertThat(wineResponse.dryness())
                    .isEqualTo(request.dryness());
            softly.assertThat(wineResponse.year())
                    .isEqualTo(request.year());
            softly.assertThat(wineResponse.sugarLevel())
                    .isEqualTo(request.sugarLevel());
            softly.assertThat(wineResponse.acidity())
                    .isEqualTo(request.acidity());
            softly.assertThat(wineResponse.alcoholLevel())
                    .isEqualTo(request.alcoholLevel());
            softly.assertThat(wineResponse.litersInBottle())
                    .isEqualTo(request.litersInBottle());
            softly.assertThat(wineResponse.batchNumber())
                    .isEqualTo(request.batchNumber());
            softly.assertThat(wineResponse.pricePerBottle())
                    .isEqualTo(request.pricePerBottle());
            softly.assertThat(wineResponse.quantityLeft())
                    .isEqualTo(request.quantityLeft());
        });
    }

    @Test
    void testGetWineEntity() throws Exception {
        Long wineId = 1L;

        MockHttpServletResponse response = mockMvc.perform(get(WINE_ENDPOINT + "/{id}", wineId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        WineResponse wineResponse = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            softly.assertThat(wineResponse.name())
                    .isEqualTo("Test Wine");
            softly.assertThat(wineResponse.dryness())
                    .isEqualTo("Dry");
        });
    }

    @Test
    void testGetAllWineEntity() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get(WINE_ENDPOINT)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        WineResponse[] wineResponses = convertServletResponseToDtoArray(response);

        WineResponse wineResponse = wineResponses[0];

        assertSoftly(softly -> {
            softly.assertThat(wineResponse.name())
                    .isEqualTo("Test Wine");
            softly.assertThat(wineResponse.dryness())
                    .isEqualTo("Dry");
        });
    }

    @Test
    void testDeleteWineEntity() throws Exception {
        Long wineId = 2L;

        Wine wine = Wine.builder()
                .id(wineId)
                .name("Test Wine")
                .dryness("Dry")
                .year(2020)
                .sugarLevel(1.2)
                .acidity(3.4)
                .alcoholLevel(13.5)
                .litersInBottle(0.75)
                .batchNumber(123456L)
                .pricePerBottle(15.99)
                .quantityLeft(100)
                .build();

        wineRepository.save(wine);

        MockHttpServletResponse response = mockMvc.perform(delete(WINE_INTERNAL_ENDPOINT + "/{id}", wineId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        assertTrue(wineRepository.findById(wineId).isEmpty());
    }

    private String convertRequestToJson(Object request) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.writeValueAsString(request);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return "";
        }
    }

    private WineResponse convertServletResponseToDto(MockHttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), WineResponse.class);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return WineResponse.builder().build();
        }
    }

    private WineResponse[] convertServletResponseToDtoArray(MockHttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), WineResponse[].class);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return new WineResponse[0];
        }
    }
}