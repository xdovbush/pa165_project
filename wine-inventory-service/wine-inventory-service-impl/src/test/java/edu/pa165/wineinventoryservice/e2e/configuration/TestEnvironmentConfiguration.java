package edu.pa165.wineinventoryservice.e2e.configuration;

import edu.pa165.wineinventoryservice.e2e.environment.WineFullStackTestEnvironment;
import edu.pa165.wineinventoryservice.e2e.environment.ReviewFullStackTestEnvironment;
import edu.pa165.wineinventoryservice.persistence.WineRepository;
import edu.pa165.wineinventoryservice.persistence.ReviewRepository;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TestEnvironmentConfiguration {
    @Bean
    public WineFullStackTestEnvironment getWineFullStackEnvironment(WineRepository repository) {
        return new WineFullStackTestEnvironment(repository);
    }

    @Bean
    public ReviewFullStackTestEnvironment getReviewFullStackEnvironment(ReviewRepository repository) {
        return new ReviewFullStackTestEnvironment(repository);
    }
}