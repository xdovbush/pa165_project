package edu.pa165.wineinventoryservice.service;

import edu.pa165.wineinventoryservice.dto.ReviewCreateRequest;
import edu.pa165.wineinventoryservice.dto.ReviewResponse;
import edu.pa165.wineinventoryservice.dto.ReviewUpdateRequest;
import edu.pa165.wineinventoryservice.model.Review;
import edu.pa165.wineinventoryservice.persistence.ReviewRepository;
import edu.pa165.wineinventoryservice.mapping.ReviewDtoMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import  edu.pa165.wineinventoryservice.monitoring.WineInventoryMetricsEndpoint;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class ReviewServiceUnitTest {

    @Mock
    private ReviewRepository reviewRepository;

    @Mock
    private WineInventoryMetricsEndpoint metricsEndpoint;
    @Mock
    private ReviewDtoMapper reviewDtoMapper;

    @InjectMocks
    private ReviewService reviewService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    public void tearDown() {
        Mockito.framework().clearInlineMocks();
    }
    @Test
    void createReview(){
        ReviewCreateRequest request = new ReviewCreateRequest(5, "Excellent", 1L);
        Review review = new Review();
        ReviewResponse expectedResponse = new ReviewResponse(1L, 5, "Excellent", 1L);

        when(reviewDtoMapper.convertFromRequestForCreate(request)).thenReturn(review);
        when(reviewRepository.save(review)).thenReturn(review);
        when(reviewDtoMapper.convertToResponse(review)).thenReturn(expectedResponse);

        ReviewResponse response = reviewService.createReview(request);

        verify(reviewRepository).save(review);
        verify(metricsEndpoint).incrementReviewCounter(eq(request.rating()));
        assertNotNull(response);
        assertEquals(expectedResponse, response);
    }

    @Test
    void updateReviewSuccessfully() {
        Long reviewId = 1L;
        ReviewUpdateRequest request = new ReviewUpdateRequest(
                1L,
                4,
                "Very Good",
                1L
        );
        Review existingReview = new Review();
        existingReview.setId(reviewId);
        existingReview.setRating(3);
        existingReview.setComment("Good");
        existingReview.setWineId(1L);

        Review updatedReview = new Review();
        updatedReview.setId(reviewId);
        updatedReview.setRating(4);
        updatedReview.setComment("Very Good");
        updatedReview.setWineId(1L);

        ReviewResponse expectedResponse = new ReviewResponse(
                1L,
                4,
                "Very Good",
                1L
        );

        when(reviewRepository.findById(reviewId)).thenReturn(Optional.of(existingReview));
        when(reviewDtoMapper.convertFromRequestForUpdate(request, existingReview)).thenReturn(updatedReview);
        when(reviewRepository.save(updatedReview)).thenReturn(updatedReview);
        when(reviewDtoMapper.convertToResponse(updatedReview)).thenReturn(expectedResponse);

        ReviewResponse response = reviewService.updateReview(reviewId, request);

        assertEquals(expectedResponse, response);
    }

    @Test
    void updateReviewThrowsExceptionWhenReviewNotFound() {
        Long reviewId = 1L;
        ReviewUpdateRequest request = new ReviewUpdateRequest(
                1L,
                4,
                "Very Good",
                1L
        );

        when(reviewRepository.findById(reviewId)).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> reviewService.updateReview(reviewId, request));
    }

    @Test
    void getReviewSuccessfully() {
        Long reviewId = 1L;
        Review review = new Review();
        review.setId(reviewId);
        review.setRating(4);
        review.setComment("Very Good");
        review.setWineId(1L);

        ReviewResponse expectedResponse = new ReviewResponse(
                1L,
                4,
                "Very Good",
                1L
        );

        when(reviewRepository.findById(reviewId)).thenReturn(Optional.of(review));
        when(reviewDtoMapper.convertToResponse(review)).thenReturn(expectedResponse);

        ReviewResponse response = reviewService.getReview(reviewId);

        assertEquals(expectedResponse, response);
    }

    @Test
    void getReviewThrowsExceptionWhenReviewNotFound() {
        Long reviewId = 1L;

        when(reviewRepository.findById(reviewId)).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> reviewService.getReview(reviewId));
    }

    @Test
    void deleteReview(){
        Long reviewId = 1L;
        reviewService.deleteReview(reviewId);

        verify(reviewRepository).deleteById(eq(reviewId));
    }
}
