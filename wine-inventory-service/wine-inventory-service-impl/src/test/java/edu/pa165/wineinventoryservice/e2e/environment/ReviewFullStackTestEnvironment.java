package edu.pa165.wineinventoryservice.e2e.environment;

import edu.pa165.wineinventoryservice.model.Review;
import edu.pa165.wineinventoryservice.persistence.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;

import static edu.pa165.wineinventoryservice.TestUtils.createDummyReview;

@TestComponent
public class ReviewFullStackTestEnvironment {

    private final ReviewRepository repository;

    @Autowired
    public ReviewFullStackTestEnvironment(ReviewRepository repository) {
        this.repository = repository;
    }

    public void initialize() {
        repository.save(createDummyReview());
        Review review = createDummyReview();
        review.setId(5L);
        repository.save(review);
    }

    public void cleanUp() {
        repository.deleteAll();
    }
}