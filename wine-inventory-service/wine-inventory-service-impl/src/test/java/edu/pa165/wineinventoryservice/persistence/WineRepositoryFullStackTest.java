package edu.pa165.wineinventoryservice.persistence;

import edu.pa165.wineinventoryservice.e2e.annotation.FullStackTest;
import edu.pa165.wineinventoryservice.e2e.annotation.UseTestData;
import edu.pa165.wineinventoryservice.e2e.environment.WineFullStackTestEnvironment;
import edu.pa165.wineinventoryservice.model.Wine;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

@FullStackTest
@UseTestData
public class WineRepositoryFullStackTest {
    @Autowired
    private WineRepository wineRepository;

    @Autowired
    @Qualifier("getWineFullStackEnvironment")
    private WineFullStackTestEnvironment environment;

    @BeforeAll
    void initialize() {
        environment.initialize();
    }

    @AfterAll
    void cleanUp() {
        environment.cleanUp();
    }

    @Test
    void getAllWines() {
        List<Wine> wines = wineRepository.findAll();

        assertThat(wines).isNotEmpty();
    }

    @Test
    void findByName() {
        String testName = "Test Wine";
        List<Wine> wines = wineRepository.findByName(testName);

        assertSoftly(softly -> wines.forEach(
                w -> softly.assertThat(w.getName()).isEqualTo(testName)
        ));
    }

    @Test
    void findByQuantityLeftLessThan() {
        Integer testQuantity = 10;
        List<Wine> wines = wineRepository.findByQuantityLeftLessThan(testQuantity);

        assertSoftly(softly -> wines.forEach(
                w -> softly.assertThat(w.getQuantityLeft()).isLessThan(testQuantity)
        ));
    }
}