package edu.pa165.wineinventoryservice.e2e;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pa165.wineinventoryservice.dto.ReviewCreateRequest;
import edu.pa165.wineinventoryservice.dto.ReviewResponse;
import edu.pa165.wineinventoryservice.dto.ReviewUpdateRequest;
import edu.pa165.wineinventoryservice.e2e.annotation.FullStackTest;
import edu.pa165.wineinventoryservice.e2e.annotation.UseTestData;
import edu.pa165.wineinventoryservice.e2e.environment.ReviewFullStackTestEnvironment;
import edu.pa165.wineinventoryservice.model.Review;
import edu.pa165.wineinventoryservice.persistence.ReviewRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static edu.pa165.wineinventoryservice.api.WineInventoryServiceApi.REVIEW_INTERNAL_ENDPOINT;
import static edu.pa165.wineinventoryservice.api.WineInventoryServiceApi.REVIEW_ENDPOINT;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FullStackTest
@UseTestData
public class ReviewEntityCrudFullStackTest {
    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    @Qualifier("getReviewFullStackEnvironment")
    private ReviewFullStackTestEnvironment environment;

    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    void initialize() {
        environment.initialize();
    }

    @AfterAll
    void cleanUp() {
        environment.cleanUp();
    }

    @Test
    void testDeleteReviewEntity() throws Exception {

        Long reviewId = 5L;

        MockHttpServletResponse response = mockMvc.perform(delete(REVIEW_INTERNAL_ENDPOINT + "/{id}", reviewId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        assertTrue(reviewRepository.findById(reviewId).isEmpty());

    }

    @Test
    void testCreateReviewEntity() {
        ReviewCreateRequest request = ReviewCreateRequest.builder()
                .comment("This wine was wonderful! Thank you France!")
                .rating(5)
                .wineId(1L)
                .build();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(post(REVIEW_ENDPOINT)
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isCreated())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
        }

        assertNotNull(response);

        ReviewResponse reviewResponse = convertServletResponseToDto(response);
        Review expectedReview = reviewRepository.findById(reviewResponse.id()).orElse(null);

        assertSoftly(softly -> {
            softly.assertThat(reviewResponse.comment())
                    .isEqualTo(request.comment());
            softly.assertThat(reviewResponse.rating())
                    .isEqualTo(request.rating());
            softly.assertThat(reviewResponse.wineId())
                    .isEqualTo(request.wineId());
        });

        assertNotNull(expectedReview);
        assertSoftly(softly -> {
            softly.assertThat(expectedReview.getComment())
                    .isEqualTo(request.comment());
            softly.assertThat(expectedReview.getRating())
                    .isEqualTo(request.rating());
            softly.assertThat(expectedReview.getWineId())
                    .isEqualTo(request.wineId());
        });
    }

    @Test
    void testUpdateReviewEntity() throws Exception {
        Long reviewId = 1L;

        ReviewUpdateRequest request = ReviewUpdateRequest.builder()
                .comment("Updated comment!")
                .rating(4)
                .wineId(1L)
                .build();

        MockHttpServletResponse response = mockMvc.perform(put(REVIEW_INTERNAL_ENDPOINT + "/{id}", reviewId)
                        .content(convertRequestToJson(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        ReviewResponse reviewResponse = convertServletResponseToDto(response);
        Review expectedReview = reviewRepository.findById(reviewResponse.id()).orElse(null);

        assertSoftly(softly -> {
            softly.assertThat(reviewResponse.comment())
                    .isEqualTo(request.comment());
            softly.assertThat(reviewResponse.rating())
                    .isEqualTo(request.rating());
            softly.assertThat(reviewResponse.wineId())
                    .isEqualTo(request.wineId());
        });

        assertNotNull(expectedReview);
        assertSoftly(softly -> {
            softly.assertThat(expectedReview.getComment())
                    .isEqualTo(request.comment());
            softly.assertThat(expectedReview.getRating())
                    .isEqualTo(request.rating());
            softly.assertThat(expectedReview.getWineId())
                    .isEqualTo(request.wineId());
        });
    }

    @Test
    void testGetReviewEntity() throws Exception {
        Long reviewId = 1L;

        MockHttpServletResponse response = mockMvc.perform(get(REVIEW_ENDPOINT + "/{id}", reviewId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        ReviewResponse reviewResponse = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            softly.assertThat(reviewResponse.comment())
                    .isEqualTo("Great wine!");
            softly.assertThat(reviewResponse.rating())
                    .isEqualTo(5);
            softly.assertThat(reviewResponse.wineId())
                    .isEqualTo(1L);
        });
    }

    private String convertRequestToJson(Object request) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.writeValueAsString(request);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return "";
        }
    }

    private ReviewResponse convertServletResponseToDto(MockHttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), ReviewResponse.class);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return ReviewResponse.builder().build();
        }
    }
}