package edu.pa165.wineinventoryservice.service;

import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.dto.WineUpdateRequest;
import edu.pa165.wineinventoryservice.mapping.WineDtoMapper;
import edu.pa165.wineinventoryservice.model.Wine;
import edu.pa165.wineinventoryservice.persistence.WineRepository;
import edu.pa165.wineinventoryservice.monitoring.WineInventoryMetricsEndpoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class WineServiceUnitTest {

    @Mock
    private WineRepository wineRepository;

    @Mock
    private WineInventoryMetricsEndpoint metricsEndpoint;
    @Mock
    private WineDtoMapper wineDtoMapper;
    @InjectMocks
    private WineServiceImpl wineService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    public void tearDown() {
        Mockito.framework().clearInlineMocks();
    }

    @Test
    void createWineShouldReturnWineResponseWhenGivenValidRequest() {
        WineCreateRequest request = new WineCreateRequest(
                "Merlot",
                "Dry",
                2019,
                2.5,
                3.5,
                12.5,
                0.75,
                1001L,
                120.0,
                50
        );

        Wine expectedWine = new Wine(
                1L,
                "Merlot",
                "Dry",
                2019,
                2.5,
                3.5,
                12.5,
                0.75,
                1001L,
                120.0,
                50
        );

        WineResponse expectedResponse = new WineResponse(
                1L,
                "Merlot",
                "Dry",
                2019,
                2.5,
                3.5,
                12.5,
                0.75,
                1001L,
                120.0,
                50
        );

        when(wineDtoMapper.convertFromRequestForCreate(request)).thenReturn(expectedWine);
        when(wineRepository.save(any(Wine.class))).thenReturn(expectedWine);
        when(wineDtoMapper.convertToResponse(expectedWine)).thenReturn(expectedResponse);
        when(metricsEndpoint.getBottlesAddedCount()).thenReturn(1.0);

        WineResponse actualResponse = wineService.createWine(request);

        assertNotNull(actualResponse);
        assertEquals(expectedResponse, actualResponse);
        verify(metricsEndpoint).getBottlesAddedCount();
    }

    @Test
    void getWine(){
        Long wineId = 1L;
        Wine wine = new Wine(
                wineId,
                "Chardonnay",
                "Semi-Dry",
                2018,
                3.0,
                3.5,
                13.0,
                0.75,
                1002L,
                100.0,
                40
        );

        WineResponse expectedResponse = new WineResponse(
                wineId,
                "Chardonnay",
                "Semi-Dry",
                2018,
                3.0,
                3.5,
                13.0,
                0.75,
                1002L,
                100.0,
                40
        );

        when(wineRepository.findById(wineId)).thenReturn(Optional.of(wine));
        when(wineService.getWine(wineId)).thenReturn(expectedResponse);

        WineResponse response = wineService.getWine(wineId);

        assertNotNull(response);
        assertEquals("Chardonnay", response.name());
        assertEquals(2018, response.year());
    }
    @Test
    void getAllWinesShouldReturnListOfWineResponsesWhenWinesExist() {
        List<Wine> wines = List.of(
                new Wine(
                        1L,
                        "Merlot",
                        "Dry",
                        2019,
                        2.5,
                        3.5,
                        12.5,
                        0.75,
                        1001L,
                        120.0,
                        50
                ),
                new Wine(
                        2L,
                        "Cabernet",
                        "Dry",
                        2020,
                        2.0,
                        3.0,
                        14.0,
                        0.75,
                        1002L,
                        130.0,
                        60
                )
        );

        when(wineRepository.findAll()).thenReturn(wines);

        List<WineResponse> expectedResponses = wines.stream()
                .map(wineDtoMapper::convertToResponse)
                .collect(Collectors.toList());

        List<WineResponse> actualResponses = wineService.getAllWines();

        assertNotNull(actualResponses);
        assertEquals(expectedResponses.size(), actualResponses.size());
        assertEquals(expectedResponses, actualResponses);
    }

    @Test
    void getAllWinesShouldReturnEmptyListWhenNoWinesExist() {
        when(wineRepository.findAll()).thenReturn(Collections.emptyList());

        List<WineResponse> actualResponses = wineService.getAllWines();

        assertNotNull(actualResponses);
        assertTrue(actualResponses.isEmpty());
    }

    @Test
    void updateWineShouldReturnUpdatedWineResponseWhenGivenValidRequestAndId() {
        Long wineId = 1L;
        Wine originalWine = new Wine(
                wineId,
                "Merlot",
                "Dry",
                2019,
                2.5,
                3.5,
                12.5,
                0.75,
                1001L,
                120.0,
                50
        );

        WineUpdateRequest request = new WineUpdateRequest(
                "Pinot Noir",
                "Semi-Sweet",
                2021,
                3.0,
                3.5,
                13.0,
                0.75,
                1003L,
                150.0,
                40
        );

        Wine updatedWine = new Wine(
                wineId,
                "Pinot Noir",
                "Semi-Sweet",
                2021,
                3.0,
                3.5,
                13.0,
                0.75,
                1003L,
                150.0,
                40
        );

        WineResponse expectedResponse = new WineResponse(
                wineId,
                "Pinot Noir",
                "Semi-Sweet",
                2021,
                3.0,
                3.5,
                13.0,
                0.75,
                1003L,
                150.0,
                40
        );

        when(wineRepository.findById(wineId)).thenReturn(Optional.of(originalWine));
        when(wineDtoMapper.convertFromRequestForUpdate(request, originalWine)).thenReturn(updatedWine);
        when(wineRepository.save(updatedWine)).thenReturn(updatedWine);
        when(wineDtoMapper.convertToResponse(updatedWine)).thenReturn(expectedResponse);

        WineResponse actualResponse = wineService.updateWine(wineId, request);

        assertNotNull(actualResponse);
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    void updateWineShouldThrowExceptionWhenGivenInvalidId() {
        Long wineId = 1L;
        WineUpdateRequest request = new WineUpdateRequest(
                "Pinot Noir",
                "Semi-Sweet",
                2021,
                3.0,
                3.5,
                13.0,
                0.75,
                1003L,
                150.0,
                40
        );

        when(wineRepository.findById(wineId)).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> wineService.updateWine(wineId, request));
    }

    @Test
    void deleteWine(){
        Long wineId = 1L;

        wineService.deleteWine(wineId);

        verify(wineRepository).deleteById(wineId);
    }
}
