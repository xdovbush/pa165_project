package edu.pa165.wineinventoryservice.e2e.environment;

import edu.pa165.wineinventoryservice.persistence.WineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;

import static edu.pa165.wineinventoryservice.TestUtils.createDummyWine;

@TestComponent
public class WineFullStackTestEnvironment {

    private final WineRepository repository;

    @Autowired
    public WineFullStackTestEnvironment(WineRepository repository) {
        this.repository = repository;
    }

    public void initialize() {
        repository.save(createDummyWine());
    }

    public void cleanUp() {
        repository.deleteAll();
    }
}