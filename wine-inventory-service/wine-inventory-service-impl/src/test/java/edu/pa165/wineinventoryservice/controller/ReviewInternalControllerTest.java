package edu.pa165.wineinventoryservice.controller;

import edu.pa165.wineinventoryservice.dto.ReviewResponse;
import edu.pa165.wineinventoryservice.dto.ReviewUpdateRequest;
import edu.pa165.wineinventoryservice.service.ReviewService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class ReviewInternalControllerTest {

    @Mock
    private ReviewService reviewService;

    private ReviewInternalController reviewInternalController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        reviewInternalController = new ReviewInternalController(reviewService);
    }

    @AfterEach
    public void tearDown() {
        Mockito.framework().clearInlineMocks();
    }

    @Test
    void updateReview() {
        Long reviewId = 1L;
        ReviewUpdateRequest request = new ReviewUpdateRequest(reviewId, 4, "Excellent", 1L);
        ReviewResponse expectedResponse = new ReviewResponse(reviewId, 4, "Good review", 1L);
        when(reviewService.updateReview(eq(reviewId), any(ReviewUpdateRequest.class))).thenReturn(expectedResponse);

        ReviewResponse response = reviewInternalController.updateReview(reviewId, request);

        assertNotNull(response);
        assertEquals(expectedResponse, response);
        verify(reviewService).updateReview(reviewId, request);
    }

    @Test
    void deleteReview() {
        Long reviewId = 1L;
        doNothing().when(reviewService).deleteReview(reviewId);

        reviewInternalController.deleteReview(reviewId);

        verify(reviewService).deleteReview(reviewId);
    }
}