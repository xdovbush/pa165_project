package edu.pa165.wineinventoryservice.dto;

import lombok.Builder;

@Builder
public record ReviewResponse(
        Long id,
        Integer rating,
        String comment,
        Long wineId
) {}