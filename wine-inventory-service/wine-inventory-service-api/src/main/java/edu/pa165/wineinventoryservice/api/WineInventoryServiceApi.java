package edu.pa165.wineinventoryservice.api;

/**
 * This is exposed class that can be used in other modules.
 * Please do not put in API module internal classes that should not be used in other services.
 * Such exposed classes should be used only to keep things consistent throughout application.
 * So under this category are: names, endpoints, request/response DTOs.
 */
public interface WineInventoryServiceApi {
    String NAME = "wine-inventory-service";
    String WINE_ENDPOINT = "/api/wines";
    String REVIEW_ENDPOINT = "/api/reviews";
    String REVIEW_INTERNAL_ENDPOINT = "/internal/reviews";
    String WINE_INTERNAL_ENDPOINT = "/internal/wines";
    String DATA_ENDPOINT = "/internal/data";
}