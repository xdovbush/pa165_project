package edu.pa165.wineinventoryservice.dto;

import lombok.Builder;

@Builder
public record ReviewCreateRequest(
         Integer rating,
         String comment,
         Long wineId
)   {

}