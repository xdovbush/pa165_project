package edu.pa165.wineinventoryservice.dto;

import lombok.Builder;

@Builder
public record WineCreateRequest(
        String name,
        String dryness,
        Integer year,
        Double sugarLevel,
        Double acidity,
        Double alcoholLevel,
        Double litersInBottle,
        Long batchNumber,
        Double pricePerBottle,
        Integer quantityLeft
)   {

}