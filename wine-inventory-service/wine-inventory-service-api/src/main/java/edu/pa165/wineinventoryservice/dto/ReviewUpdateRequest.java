package edu.pa165.wineinventoryservice.dto;

import lombok.Builder;

@Builder
public record ReviewUpdateRequest(
        Long id,
        Integer rating,
        String comment,
        Long wineId
) {}