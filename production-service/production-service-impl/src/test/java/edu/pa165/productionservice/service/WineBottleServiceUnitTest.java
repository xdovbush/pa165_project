package edu.pa165.productionservice.service;

import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.persistence.webservice.wineinventory.WineInventoryRepository;
import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static edu.pa165.productionservice.TestUtils.createDummyWineBatch;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class WineBottleServiceUnitTest {
    private WineInventoryRepository wineInventory;

    private WineBottleService service;

    @BeforeEach
    void setUp() {
        wineInventory = Mockito.mock(WineInventoryRepository.class);
        service = new WineBottleService(wineInventory);
    }
    @Test
    void bottleWineBatch() {
        WineBatch batch = createDummyWineBatch();
        Double litersInBottle = 1.0;
        BigDecimal price = BigDecimal.TEN;

        ArgumentCaptor<WineCreateRequest> argumentCaptor = ArgumentCaptor.forClass(WineCreateRequest.class);

        service.bottleWineBatch(batch, litersInBottle, price);

        verify(wineInventory).saveWineBottles(argumentCaptor.capture());
        verifyNoMoreInteractions(wineInventory);

        WineCreateRequest capturedRequest = argumentCaptor.getValue();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(capturedRequest.name()).isEqualTo(batch.getWineType());
            softly.assertThat(capturedRequest.dryness()).isEqualTo(batch.getDryness().getName());
            softly.assertThat(capturedRequest.year()).isEqualTo(batch.getMadeTime().getYear());
            softly.assertThat(capturedRequest.sugarLevel()).isEqualTo(batch.getSugarLevel());
            softly.assertThat(capturedRequest.acidity()).isEqualTo(batch.getAcidityLevel());
            softly.assertThat(capturedRequest.alcoholLevel()).isEqualTo(batch.getAlcoholLevel());
            softly.assertThat(capturedRequest.batchNumber()).isEqualTo(batch.getNumber());
            softly.assertThat(capturedRequest.quantityLeft()).isEqualTo(1221);
            softly.assertThat(capturedRequest.pricePerBottle()).isEqualTo(10.0);
        });
    }
}