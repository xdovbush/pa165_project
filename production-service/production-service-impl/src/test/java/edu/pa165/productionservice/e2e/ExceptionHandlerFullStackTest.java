package edu.pa165.productionservice.e2e;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pa165.productionservice.e2e.annotation.FullStackTest;
import edu.pa165.productionservice.model.exception.InvalidRecordException;
import edu.pa165.productionservice.persistence.webservice.exception.ServiceUnavailableException;
import edu.pa165.productionservice.service.WineRecipeService;
import edu.pa165.productionservice.service.exception.CouldNotPerformOperationException;
import edu.pa165.productionservice.service.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static edu.pa165.productionservice.api.ProductionServiceApi.WINE_RECIPE_ENDPOINT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FullStackTest
public class ExceptionHandlerFullStackTest {

    private static final String TEST_MESSAGE = "Test exception message";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WineRecipeService recipeService;

    @Test
    void handleResourceNotFound() {
        when(recipeService.getRecipe(any())).thenThrow(new ResourceNotFoundException(TEST_MESSAGE));

        MockHttpServletResponse response = null;
        try {
             response = mockMvc.perform(get(WINE_RECIPE_ENDPOINT + "/{id}", 0L)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isNotFound())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }
        assertThat(response).isNotNull();

        MockHttpServletResponse actualResponse = response;
        assertSoftly(softly -> {
            softly.assertThat(actualResponse.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
            softly.assertThat(getExceptionMessageFromResponse(actualResponse)).isEqualTo(TEST_MESSAGE);
        });
    }

    @Test
    void handleCouldNotPerformOperation() {
        when(recipeService.getRecipe(any())).thenThrow(new CouldNotPerformOperationException(TEST_MESSAGE));

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(get(WINE_RECIPE_ENDPOINT + "/{id}", 0L)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isMethodNotAllowed())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        MockHttpServletResponse actualResponse = response;
        assertSoftly(softly -> {
            softly.assertThat(actualResponse.getStatus()).isEqualTo(HttpStatus.METHOD_NOT_ALLOWED.value());
            softly.assertThat(getExceptionMessageFromResponse(actualResponse)).isEqualTo(TEST_MESSAGE);
        });
    }

    @Test
    void handleServiceUnavailable() {
        when(recipeService.getRecipe(any())).thenThrow(new ServiceUnavailableException(TEST_MESSAGE));

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(get(WINE_RECIPE_ENDPOINT + "/{id}", 0L)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isServiceUnavailable())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        MockHttpServletResponse actualResponse = response;
        assertSoftly(softly -> {
            softly.assertThat(actualResponse.getStatus()).isEqualTo(HttpStatus.SERVICE_UNAVAILABLE.value());
            softly.assertThat(getExceptionMessageFromResponse(actualResponse)).isEqualTo(TEST_MESSAGE);
        });
    }

    @Test
    void handleInvalidRecord() {
        when(recipeService.getRecipe(any())).thenThrow(new InvalidRecordException(TEST_MESSAGE));

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(get(WINE_RECIPE_ENDPOINT + "/{id}", 0L)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isUnprocessableEntity())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        MockHttpServletResponse actualResponse = response;
        assertSoftly(softly -> {
            softly.assertThat(actualResponse.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
            softly.assertThat(getExceptionMessageFromResponse(actualResponse)).isEqualTo(TEST_MESSAGE);
        });
    }

    @Test
    void handleAll() {
        when(recipeService.getRecipe(any())).thenThrow(new RuntimeException(TEST_MESSAGE));

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(get(WINE_RECIPE_ENDPOINT + "/{id}", 0L)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isInternalServerError())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        MockHttpServletResponse actualResponse = response;
        assertSoftly(softly -> {
            softly.assertThat(actualResponse.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
            softly.assertThat(getExceptionMessageFromResponse(actualResponse)).isEqualTo(TEST_MESSAGE);
        });
    }

    private String getExceptionMessageFromResponse(MockHttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode apiErrorNode = mapper.readTree(response.getContentAsString(StandardCharsets.UTF_8));
            return apiErrorNode.get("message").asText();
        } catch (Exception e) {
            return null;
        }
    }
}
