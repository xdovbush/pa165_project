package edu.pa165.productionservice.e2e.configuration;

import edu.pa165.productionservice.e2e.environment.WineBatchFullStackTestEnvironment;
import edu.pa165.productionservice.e2e.environment.WineRecipeFullStackTestEnvironment;
import edu.pa165.productionservice.persistence.winebatch.WineBatchRepository;
import edu.pa165.productionservice.persistence.winerecipe.WineRecipeRepository;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TestEnvironmentConfiguration {
    @Bean
    public WineRecipeFullStackTestEnvironment getWineRecipeFullStackEnvironment(WineRecipeRepository repository) {
        return new WineRecipeFullStackTestEnvironment(repository);
    }

    @Bean
    public WineBatchFullStackTestEnvironment getWineBatchFullStackEnvironment(WineBatchRepository repository) {
        return new WineBatchFullStackTestEnvironment(repository);
    }
}
