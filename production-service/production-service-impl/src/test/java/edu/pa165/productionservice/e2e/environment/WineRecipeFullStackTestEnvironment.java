package edu.pa165.productionservice.e2e.environment;

import edu.pa165.productionservice.TestUtils;
import edu.pa165.productionservice.persistence.winerecipe.WineRecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;

@TestComponent
public class WineRecipeFullStackTestEnvironment {

    private final WineRecipeRepository repository;

    @Autowired
    public WineRecipeFullStackTestEnvironment(WineRecipeRepository repository) {
        this.repository = repository;
    }

    public void initialize() {
        repository.save(TestUtils.createDummyWineRecipe("Test recipe 1"));
        repository.save(TestUtils.createDummyWineRecipe("Test recipe 2"));
        repository.save(TestUtils.createDummyWineRecipe("Test recipe 3"));
    }

    public void cleanUp() {
        repository.deleteAll();
    }
}
