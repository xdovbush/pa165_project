package edu.pa165.productionservice.controller.winebatch;

import edu.pa165.productionservice.dto.*;
import edu.pa165.productionservice.service.WineBatchService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class WineBatchControllerUnitTest {

    @Mock
    private WineBatchService service;

    @InjectMocks
    private WineBatchController controller;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllWineBatches() {
        Map<Long, Double> ingredients = new HashMap<>();
        WineBatchResponse response = new WineBatchResponse(1L, 1L, "Red", ingredients, "Status", LocalDate.now(), LocalDate.now(), LocalDate.now(), 1.0, 1.0, 1.0, 1.0, "Dry");
        when(service.getAllBatches()).thenReturn(Collections.singletonList(response));

        List<WineBatchResponse> result = controller.getAllWineBatches();

        verify(service, times(1)).getAllBatches();
        assertEquals(Collections.singletonList(response), result);
    }

    @Test
    void getWineBatch() {
        Long id = 1L;
        Map<Long, Double> ingredients = new HashMap<>();
        WineBatchResponse response = new WineBatchResponse(1L, 1L, "Red", ingredients, "Status", LocalDate.now(), LocalDate.now(), LocalDate.now(), 1.0, 1.0, 1.0, 1.0, "Dry");
        when(service.getBatch(id)).thenReturn(response);

        WineBatchResponse result = controller.getWineBatch(id);

        verify(service, times(1)).getBatch(id);
        assertEquals(response, result);
    }

    @Test
    void createWineBatch() {
        WineBatchCreateRequest request = WineBatchCreateRequest.builder()
                .number(1L)
                .wineType(Optional.of("Red"))
                .ingredients(Optional.of(new HashMap<>()))
                .madeTime(Optional.of(LocalDate.now()))
                .litersInBatch(Optional.of(1.0))
                .build();
        Map<Long, Double> ingredients = new HashMap<>();
        WineBatchResponse response = new WineBatchResponse(1L, 1L, "Red", ingredients, "Status", LocalDate.now(), LocalDate.now(), LocalDate.now(), 1.0, 1.0, 1.0, 1.0, "Dry");
        when(service.createBatch(request)).thenReturn(response);

        WineBatchResponse result = controller.createWineBatch(request);

        verify(service, times(1)).createBatch(request);
        assertEquals(response, result);
    }

    @Test
    void updateWineBatch() {
        Long id = 1L;
        WineBatchUpdateRequest request = WineBatchUpdateRequest.builder()
                .number(Optional.of(1L))
                .wineType(Optional.of("Red"))
                .ingredients(Optional.of(new HashMap<>()))
                .madeTime(Optional.of(LocalDate.now()))
                .openedTime(Optional.of(LocalDate.now()))
                .bottledTime(Optional.of(LocalDate.now()))
                .litersInBatch(Optional.of(1.0))
                .sugarLevel(Optional.of(1.0))
                .acidityLevel(Optional.of(1.0))
                .alcoholLevel(Optional.of(1.0))
                .dryness(Optional.of("Dry"))
                .build();
        Map<Long, Double> ingredients = new HashMap<>();
        WineBatchResponse response = new WineBatchResponse(1L, 1L, "Red", ingredients, "Status", LocalDate.now(), LocalDate.now(), LocalDate.now(), 1.0, 1.0, 1.0, 1.0, "Dry");
        when(service.updateBatch(id, request)).thenReturn(response);

        WineBatchResponse result = controller.updateWineBatch(id, request);

        verify(service, times(1)).updateBatch(id, request);
        assertEquals(response, result);
    }

    @Test
    void deleteWineBatch() {
        Long id = 1L;
        Map<Long, Double> ingredients = new HashMap<>();
        WineBatchResponse response = new WineBatchResponse(1L, 1L, "Red", ingredients, "Status", LocalDate.now(), LocalDate.now(), LocalDate.now(), 1.0, 1.0, 1.0, 1.0, "Dry");
        when(service.deleteBatch(id)).thenReturn(response);

        WineBatchResponse result = controller.deleteWineBatch(id);

        verify(service, times(1)).deleteBatch(id);
        assertEquals(response, result);
    }

    @Test
    void openWineBatch() {
        Long id = 1L;
        WineBatchOpenRequest request = new WineBatchOpenRequest(
                Optional.of(LocalDate.now()),
                Optional.of(1.0),
                Optional.of(1.0),
                Optional.of(1.0),
                Optional.of("Dry")
        );
        Map<Long, Double> ingredients = new HashMap<>();
        WineBatchResponse response = new WineBatchResponse(1L, 1L, "Red", ingredients, "Status", LocalDate.now(), LocalDate.now(), LocalDate.now(), 1.0, 1.0, 1.0, 1.0, "Dry");
        when(service.openBatch(id, request)).thenReturn(response);

        WineBatchResponse result = controller.openWineBatch(id, request);

        verify(service, times(1)).openBatch(id, request);
        assertEquals(response, result);
    }

    @Test
    void bottleWineBatch() {
        Long id = 1L;
        WineBatchBottleRequest request = new WineBatchBottleRequest(
                Optional.of(LocalDate.now()),
                Optional.of(1.0),
                Optional.of(new BigDecimal("10.0"))
        );
        Map<Long, Double> ingredients = new HashMap<>();
        WineBatchResponse response = new WineBatchResponse(1L, 1L, "Red", ingredients, "Status", LocalDate.now(), LocalDate.now(), LocalDate.now(), 1.0, 1.0, 1.0, 1.0, "Dry");
        when(service.bottleBatch(id, request)).thenReturn(response);

        WineBatchResponse result = controller.bottleWineBatch(id, request);

        verify(service, times(1)).bottleBatch(id, request);
        assertEquals(response, result);
    }
}