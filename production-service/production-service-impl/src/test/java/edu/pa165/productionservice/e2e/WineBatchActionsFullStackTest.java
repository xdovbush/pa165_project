package edu.pa165.productionservice.e2e;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pa165.productionservice.dto.WineBatchBottleRequest;
import edu.pa165.productionservice.dto.WineBatchOpenRequest;
import edu.pa165.productionservice.dto.WineBatchResponse;
import edu.pa165.productionservice.e2e.annotation.FullStackTest;
import edu.pa165.productionservice.e2e.annotation.UseTestData;
import edu.pa165.productionservice.e2e.environment.WineBatchFullStackTestEnvironment;
import edu.pa165.productionservice.model.winebatch.BatchStatus;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.persistence.webservice.harvest.HarvestInventoryRepositoryImpl;
import edu.pa165.productionservice.persistence.webservice.wineinventory.WineInventoryRepositoryImpl;
import edu.pa165.productionservice.persistence.winebatch.WineBatchRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static edu.pa165.productionservice.api.ProductionServiceApi.WINE_BATCH_ENDPOINT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FullStackTest
@UseTestData
public class WineBatchActionsFullStackTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WineBatchRepository wineBatchRepository;

    @Autowired
    private WineBatchFullStackTestEnvironment environment;

    @MockBean
    private HarvestInventoryRepositoryImpl harvestInventoryRepository;

    @MockBean
    private WineInventoryRepositoryImpl wineInventoryRepository;

    @BeforeEach
    void initialize() {
        environment.initialize();

        when(harvestInventoryRepository.isGrapesAvailable(any(), any())).thenReturn(Boolean.TRUE);
    }

    @AfterEach
    void cleanUp() {
        environment.cleanUp();
    }

    @Test
    void openWineBatch() {
        List<WineBatch> preparedWineBatches = wineBatchRepository.getPreparedWineBatches();

        if (preparedWineBatches.size() < 1) {
            fail("No prepared batches are in test database");
            return;
        }

        WineBatch expectedBatch = preparedWineBatches.get(0);

        WineBatchOpenRequest request = WineBatchOpenRequest.builder().build();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(post(WINE_BATCH_ENDPOINT + "/{id}/open", expectedBatch.getId())
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineBatchResponse actualWineBatch = convertServletResponseToDto(response);

        assertThat(actualWineBatch.status()).isEqualTo(BatchStatus.OPENED.getName());
    }

    @Test
    void bottleWineBatch() {
        List<WineBatch> openedWineBatches = wineBatchRepository.getOpenedWineBatches();

        if (openedWineBatches.size() < 1) {
            fail("No opened batches are in test database");
            return;
        }

        WineBatch expectedBatch = openedWineBatches.get(0);

        WineBatchBottleRequest request = WineBatchBottleRequest.builder().build();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(post(WINE_BATCH_ENDPOINT + "/{id}/bottle", expectedBatch.getId())
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineBatchResponse actualWineBatch = convertServletResponseToDto(response);

        assertThat(actualWineBatch.status()).isEqualTo(BatchStatus.BOTTLED.getName());
    }

    private String convertRequestToJson(Object request) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.writeValueAsString(request);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return "";
        }
    }

    private WineBatchResponse convertServletResponseToDto(MockHttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), WineBatchResponse.class);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return WineBatchResponse.builder().build();
        }
    }
}
