package edu.pa165.productionservice.model.winebatch;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import static edu.pa165.productionservice.TestUtils.createDummyWineBatch;

class WineBatchBehaviorUnitTest {
    
    private final WineBatchBehavior behavior = new WineBatchBehavior();

    @Test
    void sourceValuesForCreationWhenStatusIsPrepared() {
        WineBatch batch = createDummyWineBatch();
        batch.setStatus(BatchStatus.PREPARED);

        behavior.sourceValuesForCreation(batch);

        // Assert that correct fields are nullified
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(batch.getBottledTime()).isNull();
            softly.assertThat(batch.getOpenedTime()).isNull();
            softly.assertThat(batch.getSugarLevel()).isNull();
            softly.assertThat(batch.getAcidityLevel()).isNull();
            softly.assertThat(batch.getAlcoholLevel()).isNull();
            softly.assertThat(batch.getDryness()).isNull();
        });
        
        // Assert that audit fields are set correctly
        SoftAssertions.assertSoftly(softly ->
                softly.assertThat(batch.getCreatedDate()).isEqualTo(batch.getLastModifiedDate())
        );
    }

    @Test
    void sourceValuesForCreationWhenStatusIsOpen() {
        // Create dummy WineBatch
        WineBatch batch = createDummyWineBatch();
        batch.setStatus(BatchStatus.OPENED);

        behavior.sourceValuesForCreation(batch);

        // Assert that correct fields are nullified
        SoftAssertions.assertSoftly(softly -> softly.assertThat(batch.getBottledTime()).isNull());

        // Assert that audit fields are set correctly
        SoftAssertions.assertSoftly(
                softly -> softly.assertThat(batch.getCreatedDate()).isEqualTo(batch.getLastModifiedDate())
        );
    }

    @Test
    void sourceValuesForCreationWhenStatusIsBottled() {
        WineBatch batch = createDummyWineBatch();
        batch.setStatus(BatchStatus.BOTTLED);

        behavior.sourceValuesForCreation(batch);

        // Assert that audit fields are set correctly
        SoftAssertions.assertSoftly(
                softly -> softly.assertThat(batch.getCreatedDate()).isEqualTo(batch.getLastModifiedDate())
        );
    }

    @Test
    void sourceValuesForUpdateWhenStatusIsPrepared() {
        WineBatch originalBatch = createDummyWineBatch();
        WineBatch updatedBatch = createDummyWineBatch();
        updatedBatch.setStatus(BatchStatus.PREPARED);

        behavior.sourceValuesForUpdate(originalBatch, updatedBatch);

        // Assert that correct fields are nullified
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(updatedBatch.getBottledTime()).isNull();
            softly.assertThat(updatedBatch.getOpenedTime()).isNull();
            softly.assertThat(updatedBatch.getSugarLevel()).isNull();
            softly.assertThat(updatedBatch.getAcidityLevel()).isNull();
            softly.assertThat(updatedBatch.getAlcoholLevel()).isNull();
            softly.assertThat(updatedBatch.getDryness()).isNull();
        });

        // Assert that audit fields are set correctly
        SoftAssertions.assertSoftly(
                softly -> softly.assertThat(updatedBatch.getCreatedDate()).isEqualTo(originalBatch.getCreatedDate())
        );
    }

    @Test
    void sourceValuesForUpdateWhenStatusIsOpen() {
        WineBatch originalBatch = createDummyWineBatch();
        WineBatch updatedBatch = createDummyWineBatch();
        updatedBatch.setStatus(BatchStatus.OPENED);

        behavior.sourceValuesForUpdate(originalBatch, updatedBatch);

        // Assert that correct fields are nullified
        SoftAssertions.assertSoftly(softly -> softly.assertThat(updatedBatch.getBottledTime()).isNull());

        // Assert that audit fields are set correctly
        SoftAssertions.assertSoftly(
                softly -> softly.assertThat(updatedBatch.getCreatedDate()).isEqualTo(originalBatch.getCreatedDate())
        );
    }

    @Test
    void sourceValuesForUpdateWhenStatusIsBottled() {
        WineBatch originalBatch = createDummyWineBatch();
        WineBatch updatedBatch = createDummyWineBatch();
        updatedBatch.setStatus(BatchStatus.BOTTLED);

        behavior.sourceValuesForUpdate(originalBatch, updatedBatch);

        // Assert that audit fields are set correctly
        SoftAssertions.assertSoftly(
                softly -> softly.assertThat(updatedBatch.getCreatedDate()).isEqualTo(originalBatch.getCreatedDate())
        );
    }
}