package edu.pa165.productionservice.controller.winerecipe;

import edu.pa165.productionservice.dto.WineRecipeCreateRequest;
import edu.pa165.productionservice.dto.WineRecipeResponse;
import edu.pa165.productionservice.dto.WineRecipeUpdateRequest;
import edu.pa165.productionservice.service.WineRecipeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class WineRecipeControllerUnitTest {

    private WineRecipeService service;
    private WineRecipeController controller;

    @BeforeEach
    public void setup() {
        service = mock(WineRecipeService.class);
        controller = new WineRecipeController(service);
    }

    @Test
    void getAllWineRecipes() {
        WineRecipeResponse response = new WineRecipeResponse(1L, "Test Wine", Collections.emptyMap());
        when(service.getAllRecipes()).thenReturn(Collections.singletonList(response));

        var result = controller.getAllWineRecipes();

        verify(service, times(1)).getAllRecipes();
        assertEquals(Collections.singletonList(response), result);
    }

    @Test
    void getWineRecipe() {
        WineRecipeResponse response = new WineRecipeResponse(1L, "Test Wine", Collections.emptyMap());
        when(service.getRecipe(1L)).thenReturn(response);

        var result = controller.getWineRecipe(1L);

        verify(service, times(1)).getRecipe(1L);
        assertEquals(response, result);
    }

    @Test
    void createWineRecipe() {
        WineRecipeCreateRequest request = new WineRecipeCreateRequest("Test Wine", Collections.emptyMap());
        WineRecipeResponse response = new WineRecipeResponse(1L, "Test Wine", Collections.emptyMap());
        when(service.createRecipe(request)).thenReturn(response);

        var result = controller.createWineRecipe(request);

        verify(service, times(1)).createRecipe(request);
        assertEquals(response, result);
    }

    @Test
    void updateWineRecipe() {
        WineRecipeUpdateRequest request = new WineRecipeUpdateRequest(Optional.of("Test Wine"), Optional.of(Collections.emptyMap()));
        WineRecipeResponse response = new WineRecipeResponse(1L, "Test Wine", Collections.emptyMap());
        when(service.updateRecipe(1L, request)).thenReturn(response);

        var result = controller.updateWineRecipe(1L, request);

        verify(service, times(1)).updateRecipe(1L, request);
        assertEquals(response, result);
    }

    @Test
    void deleteWineRecipe() {
        Long id = 1L;
        WineRecipeResponse response = new WineRecipeResponse(1L, "Test Wine", Collections.emptyMap());
        when(service.deleteRecipe(id)).thenReturn(response);

        WineRecipeResponse result = controller.deleteWineRecipe(id);

        verify(service, times(1)).deleteRecipe(id);
        assertEquals(response, result);
    }
}