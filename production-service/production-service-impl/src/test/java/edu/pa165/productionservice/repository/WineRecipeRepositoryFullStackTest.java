package edu.pa165.productionservice.repository;

import edu.pa165.productionservice.e2e.annotation.FullStackTest;
import edu.pa165.productionservice.e2e.annotation.UseTestData;
import edu.pa165.productionservice.e2e.environment.WineRecipeFullStackTestEnvironment;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import edu.pa165.productionservice.persistence.winerecipe.WineRecipeRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

@FullStackTest
@UseTestData
public class WineRecipeRepositoryFullStackTest {
    @Autowired
    private WineRecipeRepository wineRecipeRepository;

    @Autowired
    private WineRecipeFullStackTestEnvironment environment;

    @BeforeAll
    void initialize() {
        environment.initialize();
    }

    @AfterAll
    void cleanUp() {
        environment.cleanUp();
    }

    @Test
    void getWineRecipesWithIngredient() {
        String ingredientName = "Super special test grapes";
        WineRecipe expectedRecipe = WineRecipe.builder()
                .name("I wanna be found")
                .ingredients(Map.of(ingredientName, 100.0))
                .build();
        wineRecipeRepository.save(expectedRecipe);

        List<WineRecipe> actualRecipes = wineRecipeRepository.getWineRecipesWithIngredient(ingredientName);

        assertThat(actualRecipes).hasSize(1);

        WineRecipe actualRecipe = actualRecipes.get(0);

        assertSoftly(softly -> {
            softly.assertThat(actualRecipe.getName()).isEqualTo(expectedRecipe.getName());
            softly.assertThat(actualRecipe.getIngredients()).isEqualTo(expectedRecipe.getIngredients());
        });
    }

    @Test
    void getWineRecipesWithoutIngredients() {
        WineRecipe expectedRecipe = WineRecipe.builder()
                .name("I wanna be found")
                .ingredients(Map.of())
                .build();
        wineRecipeRepository.save(expectedRecipe);

        List<WineRecipe> actualRecipes = wineRecipeRepository.getWineRecipesWithoutIngredients();

        assertThat(actualRecipes).hasSize(1);

        WineRecipe actualRecipe = actualRecipes.get(0);

        assertSoftly(softly -> {
            softly.assertThat(actualRecipe.getName()).isEqualTo(expectedRecipe.getName());
            softly.assertThat(actualRecipe.getIngredients()).isEmpty();
        });
    }
}
