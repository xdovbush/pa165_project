package edu.pa165.productionservice.e2e.environment;

import edu.pa165.productionservice.model.winebatch.BatchStatus;
import edu.pa165.productionservice.persistence.winebatch.WineBatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;

import static edu.pa165.productionservice.TestUtils.createDummyWineBatch;

@TestComponent
public class WineBatchFullStackTestEnvironment {

    private final WineBatchRepository repository;

    @Autowired
    public WineBatchFullStackTestEnvironment(WineBatchRepository repository) {
        this.repository = repository;
    }

    public void initialize() {
        repository.save(createDummyWineBatch(BatchStatus.PREPARED));
        repository.save(createDummyWineBatch(BatchStatus.OPENED));
        repository.save(createDummyWineBatch(BatchStatus.BOTTLED));
    }

    public void cleanUp() {
        repository.deleteAll();
    }
}
