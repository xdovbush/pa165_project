package edu.pa165.productionservice.repository;

import edu.pa165.productionservice.e2e.annotation.FullStackTest;
import edu.pa165.productionservice.e2e.annotation.UseTestData;
import edu.pa165.productionservice.e2e.environment.WineBatchFullStackTestEnvironment;
import edu.pa165.productionservice.model.winebatch.BatchStatus;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.persistence.winebatch.WineBatchRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.SoftAssertions.assertSoftly;

@FullStackTest
@UseTestData
public class WineBatchRepositoryFullStackTest {
    @Autowired
    private WineBatchRepository wineBatchRepository;

    @Autowired
    private WineBatchFullStackTestEnvironment environment;

    @BeforeAll
    void initialize() {
        environment.initialize();
    }

    @AfterAll
    void cleanUp() {
        environment.cleanUp();
    }

    @Test
    void getPreparedWineBatches() {
        List<WineBatch> preparedBatches = wineBatchRepository.getPreparedWineBatches();

        assertSoftly(softly -> preparedBatches.forEach(
                b -> softly.assertThat(b.getStatus()).isEqualTo(BatchStatus.PREPARED)
        ));
    }

    @Test
    void getOpenedWineBatches() {
        List<WineBatch> preparedBatches = wineBatchRepository.getOpenedWineBatches();

        assertSoftly(softly -> preparedBatches.forEach(
                b -> softly.assertThat(b.getStatus()).isEqualTo(BatchStatus.OPENED)
        ));
    }

    @Test
    void getBottledWineBatches() {
        List<WineBatch> preparedBatches = wineBatchRepository.getBottledWineBatches();

        assertSoftly(softly -> preparedBatches.forEach(
                b -> softly.assertThat(b.getStatus()).isEqualTo(BatchStatus.BOTTLED)
        ));
    }
}
