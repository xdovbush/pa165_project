package edu.pa165.productionservice.exceptionhandling;

import edu.pa165.productionservice.model.exception.InvalidRecordException;
import edu.pa165.productionservice.persistence.webservice.exception.ServiceUnavailableException;
import edu.pa165.productionservice.service.exception.*;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GlobalExceptionHandlerUnitTest {

    private GlobalExceptionHandler handler;
    private HttpServletRequest request;

    @BeforeEach
    void setUp() {
        handler = new GlobalExceptionHandler();
        request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/test-uri");
    }

    @Test
    void handleResourceNotFound() {
        ResourceNotFoundException ex = new ResourceNotFoundException("Test message");
        ResponseEntity<ApiError> response = handler.handleResourceNotFound(ex, request);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        if (response.getBody() != null) {
            assertEquals("Test message", response.getBody().getMessage());
            assertEquals("/test-uri", response.getBody().getPath());
        }
    }

    @Test
    void handleCouldNotPerformOperation() {
        CouldNotPerformOperationException ex = new CouldNotPerformOperationException("Test message");
        ResponseEntity<ApiError> response = handler.handleCouldNotPerformOperation(ex, request);

        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
        if (response.getBody() != null) {
            assertEquals("Test message", response.getBody().getMessage());
            assertEquals("/test-uri", response.getBody().getPath());
        }
    }

    @Test
    void handleServiceUnavailable() {
        ServiceUnavailableException ex = new ServiceUnavailableException("Test message");
        ResponseEntity<ApiError> response = handler.handleServiceUnavailable(ex, request);

        assertEquals(HttpStatus.SERVICE_UNAVAILABLE, response.getStatusCode());
        if (response.getBody() != null) {
            assertEquals("Test message", response.getBody().getMessage());
            assertEquals("/test-uri", response.getBody().getPath());
        }
    }

    @Test
    void handleInvalidRecord() {
        InvalidRecordException ex = new InvalidRecordException("Test message");
        ResponseEntity<ApiError> response = handler.handleInvalidRecord(ex, request);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
        if (response.getBody() != null) {
            assertEquals("Test message", response.getBody().getMessage());
            assertEquals("/test-uri", response.getBody().getPath());
        }
    }

    @Test
    void handleAll() {
        Exception ex = new Exception("Test message");
        ResponseEntity<ApiError> response = handler.handleAll(ex, request);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        if (response.getBody() != null) {
            assertEquals("Test message", response.getBody().getMessage());
            assertEquals("/test-uri", response.getBody().getPath());
        }
    }
}