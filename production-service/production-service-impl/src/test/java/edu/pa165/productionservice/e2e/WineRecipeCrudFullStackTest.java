package edu.pa165.productionservice.e2e;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pa165.productionservice.dto.WineRecipeCreateRequest;
import edu.pa165.productionservice.dto.WineRecipeResponse;
import edu.pa165.productionservice.e2e.annotation.FullStackTest;
import edu.pa165.productionservice.e2e.annotation.UseTestData;
import edu.pa165.productionservice.e2e.environment.WineRecipeFullStackTestEnvironment;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import edu.pa165.productionservice.persistence.winerecipe.WineRecipeRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import static edu.pa165.productionservice.api.ProductionServiceApi.WINE_RECIPE_ENDPOINT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FullStackTest
@UseTestData
public class WineRecipeCrudFullStackTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WineRecipeRepository wineRecipeRepository;

    @Autowired
    private WineRecipeFullStackTestEnvironment environment;

    @BeforeAll
    void initialize() {
        environment.initialize();
    }

    @AfterAll
    void cleanUp() {
        environment.cleanUp();
    }

    @Test
    void createWineRecipe() {
        WineRecipeCreateRequest request = WineRecipeCreateRequest.builder()
                .name("New name")
                .ingredients(
                        Map.of(
                                "My ingredient 1", 40.0,
                                "My ingredient 2", 60.0
                        )
                )
                .build();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(post(WINE_RECIPE_ENDPOINT)
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isCreated())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineRecipeResponse actualWineRecipe = convertServletResponseToDto(response);
        WineRecipe persistedWineRecipe = wineRecipeRepository.findById(actualWineRecipe.id()).orElseThrow();

        assertSoftly(softly -> {
            softly.assertThat(actualWineRecipe.name())
                    .isEqualTo(request.name());
            softly.assertThat(actualWineRecipe.ingredients())
                    .containsExactlyInAnyOrderEntriesOf(request.ingredients());
        });

        assertSoftly(softly -> {
            softly.assertThat(persistedWineRecipe.getName())
                    .isEqualTo(request.name());
            softly.assertThat(persistedWineRecipe.getIngredients())
                    .containsExactlyInAnyOrderEntriesOf(request.ingredients());
        });
    }

    @Test
    void readWineRecipe() {
        Long wineRecipeId = 1L;
        WineRecipe expectedWineRecipe = wineRecipeRepository.findById(wineRecipeId).orElseThrow();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(get(WINE_RECIPE_ENDPOINT + "/{id}", wineRecipeId)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineRecipeResponse actualWineRecipe = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            softly.assertThat(actualWineRecipe.id())
                    .isEqualTo(expectedWineRecipe.getId());
            softly.assertThat(actualWineRecipe.name())
                    .isEqualTo(expectedWineRecipe.getName());
            softly.assertThat(actualWineRecipe.ingredients())
                    .containsExactlyInAnyOrderEntriesOf(expectedWineRecipe.getIngredients());
        });
    }

    @Test
    void updateWineRecipe() {
        Long wineRecipeId = 1L;
        WineRecipeCreateRequest request = WineRecipeCreateRequest.builder()
                .name("New name")
                .ingredients(
                        Map.of(
                                "My ingredient 1", 40.0,
                                "My ingredient 2", 60.0
                        )
                )
                .build();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(put(WINE_RECIPE_ENDPOINT + "/{id}", wineRecipeId)
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineRecipeResponse actualWineRecipe = convertServletResponseToDto(response);
        WineRecipe persistedWineRecipe = wineRecipeRepository.findById(wineRecipeId).orElseThrow();

        assertSoftly(softly -> {
            softly.assertThat(actualWineRecipe.name())
                    .isEqualTo(request.name());
            softly.assertThat(actualWineRecipe.ingredients())
                    .containsExactlyInAnyOrderEntriesOf(request.ingredients());
        });

        assertSoftly(softly -> {
            softly.assertThat(persistedWineRecipe.getName())
                    .isEqualTo(request.name());
            softly.assertThat(persistedWineRecipe.getIngredients())
                    .containsExactlyInAnyOrderEntriesOf(request.ingredients());
        });
    }

    @Test
    void deleteWineRecipe() {
        Long wineRecipeId = 2L;
        WineRecipe expectedWineRecipe = wineRecipeRepository.findById(wineRecipeId).orElseThrow();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(delete(WINE_RECIPE_ENDPOINT + "/{id}", wineRecipeId)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineRecipeResponse actualWineRecipe = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            softly.assertThat(actualWineRecipe.id())
                    .isEqualTo(expectedWineRecipe.getId());
            softly.assertThat(actualWineRecipe.name())
                    .isEqualTo(expectedWineRecipe.getName());
            softly.assertThat(actualWineRecipe.ingredients())
                    .containsExactlyInAnyOrderEntriesOf(expectedWineRecipe.getIngredients());
        });

        assertThat(wineRecipeRepository.findById(wineRecipeId)).isEmpty();
    }

    @Test
    void validateWineRecipe() {
        WineRecipeCreateRequest request = WineRecipeCreateRequest.builder()
                .name("New name")
                .ingredients(
                        Map.of(
                                "My ingredient 1", 40.0,
                                "My ingredient 2", 40.0
                        )
                )
                .build();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(post(WINE_RECIPE_ENDPOINT)
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isUnprocessableEntity())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    private String convertRequestToJson(Object request) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.writeValueAsString(request);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return "";
        }
    }

    private WineRecipeResponse convertServletResponseToDto(MockHttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), WineRecipeResponse.class);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return WineRecipeResponse.builder().build();
        }
    }
}
