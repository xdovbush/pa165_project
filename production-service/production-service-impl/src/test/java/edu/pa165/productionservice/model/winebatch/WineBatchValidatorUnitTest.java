package edu.pa165.productionservice.model.winebatch;

import edu.pa165.productionservice.model.exception.InvalidRecordException;
import edu.pa165.productionservice.service.exception.CouldNotPerformOperationException;
import edu.pa165.productionservice.translation.ErrorStringTemplates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static edu.pa165.productionservice.TestUtils.createDummyWineBatch;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class WineBatchValidatorUnitTest {

    private WineBatchValidator validator;

    @BeforeEach
    void setUp() {
        ErrorStringTemplates templates = Mockito.mock(ErrorStringTemplates.class);
        templates.RECORD_DOES_NOT_HAVE_MANDATORY_FIELDS_SET_PROPERLY = "Test error";
        templates.BOTTLED_BATCH_COULD_NOT_BE_DELETED = "Test error";
        validator = new WineBatchValidator(templates);
    }

    @Test
    void validateCreation() {
        // Create dummy WineBatch
        WineBatch batch = createDummyWineBatch();

        assertDoesNotThrow(() -> validator.validateCreation(batch));

        // Invalid field set
        batch.setStatus(BatchStatus.OPENED);
        assertThrows(InvalidRecordException.class, () -> validator.validateCreation(batch));
    }

    @Test
    void validateUpdateWithStatusOpened() {
        // Create dummy WineBatch
        WineBatch batch = createDummyWineBatch();
        batch.setStatus(BatchStatus.OPENED);

        assertDoesNotThrow(() -> validator.validateUpdate(batch));

        // Invalid field set
        batch.setNumber(null);
        assertThrows(InvalidRecordException.class, () -> validator.validateUpdate(batch));
    }

    @Test
    void validateUpdateWithStatusBottled() {
        // Create dummy WineBatch
        WineBatch batch = createDummyWineBatch();
        batch.setStatus(BatchStatus.BOTTLED);

        assertDoesNotThrow(() -> validator.validateUpdate(batch));

        // Invalid field set from opened status validation
        batch.setNumber(null);
        assertThrows(InvalidRecordException.class, () -> validator.validateUpdate(batch));

        // Invalid field set from bottled status validation
        batch.setNumber(1234L);
        batch.setSugarLevel(null);
        assertThrows(InvalidRecordException.class, () -> validator.validateUpdate(batch));
    }

    @Test
    void validateDelete() {
        // Create dummy WineBatch
        WineBatch batch = createDummyWineBatch();

        assertDoesNotThrow(() -> validator.validateDelete(batch));

        // Invalid field set
        batch.setStatus(BatchStatus.BOTTLED);
        assertThrows(CouldNotPerformOperationException.class, () -> validator.validateDelete(batch));
    }
}