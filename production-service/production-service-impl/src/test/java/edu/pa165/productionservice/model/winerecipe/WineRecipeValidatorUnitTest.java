package edu.pa165.productionservice.model.winerecipe;

import edu.pa165.productionservice.model.exception.InvalidRecordException;
import edu.pa165.productionservice.translation.ErrorStringTemplates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Map;

import static edu.pa165.productionservice.TestUtils.createDummyWineRecipe;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class WineRecipeValidatorUnitTest {

    private WineRecipeValidator validator;

    @BeforeEach
    void setUp() {
        ErrorStringTemplates templates = Mockito.mock(ErrorStringTemplates.class);
        templates.RECORD_DOES_NOT_HAVE_MANDATORY_FIELDS_SET_PROPERLY = "Test error";
        validator = new WineRecipeValidator(templates);
    }

    @Test
    void validateCreation() {
        // Create dummy WineRecipe
        WineRecipe recipe = createDummyWineRecipe();

        assertDoesNotThrow(() -> validator.validateCreation(recipe));

        // Invalid field set
        recipe.setIngredients(Map.of("Type1", 30.0));
        assertThrows(InvalidRecordException.class, () -> validator.validateCreation(recipe));
    }

    @Test
    void validateUpdate() {
        // Create dummy WineRecipe
        WineRecipe recipe = createDummyWineRecipe();

        assertDoesNotThrow(() -> validator.validateUpdate(recipe));

        // Invalid field set
        recipe.setIngredients(Map.of("Type1", 30.0));
        assertThrows(InvalidRecordException.class, () -> validator.validateUpdate(recipe));
    }
}