package edu.pa165.productionservice.service;

import edu.pa165.productionservice.dto.*;
import edu.pa165.productionservice.mapping.winebatch.WineBatchDtoMapper;
import edu.pa165.productionservice.model.winebatch.BatchStatus;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.persistence.RecordOrchestrator;
import edu.pa165.productionservice.persistence.winebatch.WineBatchOrchestrator;
import edu.pa165.productionservice.translation.ErrorStringTemplates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class WineBatchServiceUnitTest {
    private static final Long BATCH_ID = 1L;

    private WineBatchDtoMapper mapper;
    private WineBottleService bottleService;
    private WineHarvestService harvestService;
    private RecordOrchestrator<WineBatch> orchestrator;
    private ErrorStringTemplates errors;

    private WineBatchService service;

    private WineBatch oldBatch;
    private WineBatch newBatch;
    private WineBatchResponse oldResponse;
    private WineBatchResponse newResponse;

    @BeforeEach
    void setUp() {
        oldBatch = new WineBatch();
        newBatch = new WineBatch();
        oldBatch.setId(BATCH_ID);
        newBatch.setId(BATCH_ID);

        oldResponse = WineBatchResponse.builder().build();
        newResponse = WineBatchResponse.builder().build();

        mapper = mock(WineBatchDtoMapper.class);
        bottleService = mock(WineBottleService.class);
        harvestService = mock(WineHarvestService.class);
        orchestrator = mock(WineBatchOrchestrator.class);
        errors = mock(ErrorStringTemplates.class);

        when(mapper.convertToResponse(oldBatch)).thenReturn(oldResponse);
        when(mapper.convertToResponse(newBatch)).thenReturn(newResponse);

        when(harvestService.grapesAvailableForCreate(any())).thenReturn(Boolean.TRUE);
        when(harvestService.grapesAvailableForUpdate(any(), any())).thenReturn(Boolean.TRUE);

        when(orchestrator.getById(BATCH_ID)).thenReturn(Optional.of(oldBatch));
        when(orchestrator.getAll()).thenReturn(List.of(oldBatch, newBatch));
        when(orchestrator.create(any())).thenReturn(newBatch);
        when(orchestrator.update(any())).thenReturn(newBatch);
        when(orchestrator.update(any(), any())).thenReturn(newBatch);
        when(orchestrator.deleteById(BATCH_ID)).thenReturn(oldBatch);

        service = new WineBatchService(
                mapper,
                bottleService,
                harvestService,
                orchestrator,
                errors
        );
    }

    @Test
    void getBatch() {
        WineBatchResponse actualResponse = service.getBatch(BATCH_ID);

        assertThat(actualResponse).isSameAs(oldResponse);

        verify(orchestrator).getById(BATCH_ID);
        verify(mapper).convertToResponse(oldBatch);
        verifyNoMoreInteractions(
                mapper,
                bottleService,
                harvestService,
                orchestrator,
                errors
        );
    }

    @Test
    void getAllBatches() {
        List<WineBatchResponse> actualResponses = service.getAllBatches();

        assertThat(actualResponses).containsExactlyInAnyOrder(oldResponse, newResponse);

        verify(orchestrator).getAll();
        verify(mapper).convertToResponse(oldBatch);
        verify(mapper).convertToResponse(newBatch);
        verifyNoMoreInteractions(
                mapper,
                bottleService,
                harvestService,
                orchestrator,
                errors
        );
    }

    @Test
    void createBatch() {
        WineBatchCreateRequest createRequest = WineBatchCreateRequest.builder().build();
        when(mapper.convertFromRequestForCreate(createRequest)).thenReturn(newBatch);

        WineBatchResponse actualResponse = service.createBatch(createRequest);

        assertThat(actualResponse).isSameAs(newResponse);

        verify(mapper).convertFromRequestForCreate(createRequest);
        verify(harvestService).grapesAvailableForCreate(newBatch);
        verify(orchestrator).create(newBatch);
        verify(harvestService).onBatchCreate(newBatch);
        verify(mapper).convertToResponse(newBatch);
        verifyNoMoreInteractions(
                mapper,
                bottleService,
                harvestService,
                orchestrator,
                errors
        );
    }

    @Test
    void updateBatch() {
        WineBatchUpdateRequest updateRequest = WineBatchUpdateRequest.builder().build();
        when(mapper.convertFromRequestForUpdate(updateRequest, oldBatch)).thenReturn(newBatch);

        WineBatchResponse actualResponse = service.updateBatch(BATCH_ID, updateRequest);

        assertThat(actualResponse).isSameAs(newResponse);

        verify(orchestrator).getById(BATCH_ID);
        verify(mapper).convertFromRequestForUpdate(updateRequest, oldBatch);
        verify(harvestService).grapesAvailableForUpdate(oldBatch, newBatch);
        verify(orchestrator).update(oldBatch, newBatch);
        verify(harvestService).onBatchUpdate(oldBatch, newBatch);
        verify(mapper).convertToResponse(newBatch);
        verifyNoMoreInteractions(
                mapper,
                bottleService,
                harvestService,
                orchestrator,
                errors
        );
    }

    @Test
    void deleteBatch() {
        WineBatchResponse actualResponse = service.deleteBatch(BATCH_ID);

        assertThat(actualResponse).isSameAs(oldResponse);

        verify(orchestrator).deleteById(BATCH_ID);
        verify(harvestService).onBatchDelete(oldBatch);
        verify(mapper).convertToResponse(oldBatch);
        verifyNoMoreInteractions(
                mapper,
                bottleService,
                harvestService,
                orchestrator,
                errors
        );
    }

    @Test
    void openBatch() {
        oldBatch.setStatus(BatchStatus.PREPARED);
        WineBatchOpenRequest openRequest = WineBatchOpenRequest.builder()
                .openedTime(Optional.empty())
                .sugarLevel(Optional.empty())
                .acidityLevel(Optional.empty())
                .alcoholLevel(Optional.empty())
                .dryness(Optional.empty())
                .build();

        WineBatchResponse actualResponse = service.openBatch(BATCH_ID, openRequest);

        assertThat(actualResponse).isSameAs(newResponse);

        verify(orchestrator).getById(BATCH_ID);
        verify(orchestrator).update(oldBatch);
        verify(mapper).convertToResponse(newBatch);
        verifyNoMoreInteractions(
                mapper,
                bottleService,
                harvestService,
                orchestrator,
                errors
        );
    }

    @Test
    void bottleBatch() {
        oldBatch.setStatus(BatchStatus.OPENED);
        WineBatchBottleRequest openRequest = WineBatchBottleRequest.builder()
                .bottledTime(Optional.empty())
                .litersInBottle(Optional.empty())
                .price(Optional.empty())
                .build();

        WineBatchResponse actualResponse = service.bottleBatch(BATCH_ID, openRequest);

        assertThat(actualResponse).isSameAs(newResponse);

        verify(orchestrator).getById(BATCH_ID);
        verify(orchestrator).update(oldBatch);
        verify(bottleService).bottleWineBatch(same(newBatch), any(), any());
        verify(mapper).convertToResponse(newBatch);
        verifyNoMoreInteractions(
                mapper,
                bottleService,
                harvestService,
                orchestrator,
                errors
        );
    }
}