package edu.pa165.productionservice.mapping.winebatch;

import edu.pa165.productionservice.dto.*;
import edu.pa165.productionservice.model.winebatch.BatchStatus;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

import static edu.pa165.productionservice.TestUtils.createDummyWineBatch;
import static org.junit.jupiter.api.Assertions.assertEquals;

class WineBatchDtoMapperUnitTest {

    private WineBatchDtoMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new WineBatchDtoMapperImpl();
    }

    @Test
    void convertToResponseWithStatusPrepared() {
        WineBatch batch = createDummyWineBatch();
        batch.setStatus(BatchStatus.PREPARED);

        WineBatchResponse response = mapper.convertToResponse(batch);

        assertEquals(batch.getId(), response.id());
        assertEquals(batch.getNumber(), response.number());
        assertEquals(batch.getWineType(), response.wineType());
        assertEquals(batch.getIngredients(), response.ingredients());
        assertEquals(batch.getMadeTime(), response.madeTime());
        assertEquals(batch.getLitersInBatch(), response.litersInBatch());
        assertEquals(batch.getStatus().name().substring(0, 1).toUpperCase() + batch.getStatus().name().substring(1).toLowerCase(), response.status());
    }

    @Test
    void convertToResponseWithStatusOpened() {
        WineBatch batch = createDummyWineBatch();
        batch.setStatus(BatchStatus.OPENED);

        WineBatchResponse response = mapper.convertToResponse(batch);

        assertEquals(batch.getId(), response.id());
        assertEquals(batch.getNumber(), response.number());
        assertEquals(batch.getWineType(), response.wineType());
        assertEquals(batch.getIngredients(), response.ingredients());
        assertEquals(batch.getMadeTime(), response.madeTime());
        assertEquals(batch.getLitersInBatch(), response.litersInBatch());
        assertEquals(batch.getStatus().name().substring(0, 1).toUpperCase() + batch.getStatus().name().substring(1).toLowerCase(), response.status());
    }

    @Test
    void convertToResponseWithStatusBottled() {
        WineBatch batch = createDummyWineBatch();
        batch.setStatus(BatchStatus.BOTTLED);

        WineBatchResponse response = mapper.convertToResponse(batch);

        assertEquals(batch.getId(), response.id());
        assertEquals(batch.getNumber(), response.number());
        assertEquals(batch.getWineType(), response.wineType());
        assertEquals(batch.getIngredients(), response.ingredients());
        assertEquals(batch.getMadeTime(), response.madeTime());
        assertEquals(batch.getLitersInBatch(), response.litersInBatch());
        assertEquals(batch.getStatus().name().substring(0, 1).toUpperCase() + batch.getStatus().name().substring(1).toLowerCase(), response.status());
    }

    @Test
    void convertFromRequestForCreate() {
        WineBatchCreateRequest request = new WineBatchCreateRequest(
                123L,
                Optional.of("Test Wine"),
                Optional.of(Collections.emptyMap()),
                Optional.of(LocalDate.now()),
                Optional.of(1000.0)
        );

        WineBatch batch = mapper.convertFromRequestForCreate(request);

        assertEquals(request.number(), batch.getNumber());
        assertEquals(request.wineType().orElse(null), batch.getWineType());
        assertEquals(request.ingredients().orElse(null), batch.getIngredients());
        assertEquals(request.madeTime().orElse(null), batch.getMadeTime());
        assertEquals(request.litersInBatch().orElse(null), batch.getLitersInBatch());
    }

    @Test
    void convertFromRequestForUpdate() {
        WineBatchUpdateRequest request = new WineBatchUpdateRequest(
                Optional.of(123L),
                Optional.of("Test Wine"),
                Optional.of(Collections.emptyMap()),
                Optional.of(LocalDate.now()),
                Optional.empty(),
                Optional.empty(),
                Optional.of(1000.0),
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.of("Dry")
        );
        WineBatch originalBatch = createDummyWineBatch();

        WineBatch batch = mapper.convertFromRequestForUpdate(request, originalBatch);

        assertEquals(request.number().orElse(null), batch.getNumber());
        assertEquals(request.wineType().orElse(null), batch.getWineType());
        assertEquals(request.ingredients().orElse(null), batch.getIngredients());
        assertEquals(request.madeTime().orElse(null), batch.getMadeTime());
        assertEquals(request.litersInBatch().orElse(null), batch.getLitersInBatch());
        assertEquals(request.dryness().orElse(null), batch.getDryness().name().substring(0, 1).toUpperCase() + batch.getDryness().name().substring(1).toLowerCase());
    }
}