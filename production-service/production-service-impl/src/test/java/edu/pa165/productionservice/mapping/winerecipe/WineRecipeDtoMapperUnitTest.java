package edu.pa165.productionservice.mapping.winerecipe;

import edu.pa165.productionservice.dto.*;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Optional;

import static edu.pa165.productionservice.TestUtils.createDummyWineRecipe;
import static org.junit.jupiter.api.Assertions.assertEquals;

class WineRecipeDtoMapperUnitTest {

    private WineRecipeDtoMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new WineRecipeDtoMapperImpl();
    }

    @Test
    void convertToResponse() {
        WineRecipe recipe = createDummyWineRecipe();

        WineRecipeResponse response = mapper.convertToResponse(recipe);

        assertEquals(recipe.getId(), response.id());
        assertEquals(recipe.getName(), response.name());
        assertEquals(recipe.getIngredients(), response.ingredients());
    }

    @Test
    void convertFromRequestForCreate() {
        WineRecipeCreateRequest request = new WineRecipeCreateRequest(
                "Test Recipe",
                Collections.emptyMap()
        );

        WineRecipe recipe = mapper.convertFromRequestForCreate(request);

        assertEquals(request.name(), recipe.getName());
        assertEquals(request.ingredients(), recipe.getIngredients());
    }

    @Test
    void convertFromRequestForUpdate() {
        WineRecipeUpdateRequest request = new WineRecipeUpdateRequest(
                Optional.of("Test Wine"),
                Optional.of(Collections.emptyMap())
        );
        WineRecipe originalRecipe = createDummyWineRecipe();

        WineRecipe recipe = mapper.convertFromRequestForUpdate(request, originalRecipe);

        assertEquals(request.name().orElse(null), recipe.getName());
        assertEquals(request.ingredients().orElse(null), recipe.getIngredients());
    }
}