package edu.pa165.productionservice.e2e;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pa165.productionservice.dto.WineBatchCreateRequest;
import edu.pa165.productionservice.dto.WineBatchResponse;
import edu.pa165.productionservice.dto.WineBatchUpdateRequest;
import edu.pa165.productionservice.e2e.annotation.FullStackTest;
import edu.pa165.productionservice.e2e.annotation.UseTestData;
import edu.pa165.productionservice.e2e.environment.WineBatchFullStackTestEnvironment;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.persistence.webservice.harvest.HarvestInventoryRepositoryImpl;
import edu.pa165.productionservice.persistence.webservice.wineinventory.WineInventoryRepositoryImpl;
import edu.pa165.productionservice.persistence.winebatch.WineBatchRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static edu.pa165.productionservice.api.ProductionServiceApi.WINE_BATCH_ENDPOINT;
import static java.time.LocalDate.EPOCH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FullStackTest
@UseTestData
public class WineBatchCrudFullStackTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WineBatchRepository wineBatchRepository;

    @Autowired
    private WineBatchFullStackTestEnvironment environment;

    @MockBean
    private HarvestInventoryRepositoryImpl harvestInventoryRepository;

    @MockBean
    private WineInventoryRepositoryImpl wineInventoryRepository;

    @BeforeEach
    void initialize() {
        environment.initialize();

        when(harvestInventoryRepository.isGrapesAvailable(any(), any())).thenReturn(Boolean.TRUE);
    }

    @AfterEach
    void cleanUp() {
        environment.cleanUp();
    }

    @Test
    void createWineBatch() {
        WineBatchCreateRequest request = WineBatchCreateRequest.builder()
                .number(12345L)
                .wineType(Optional.of("Test wine type"))
                .litersInBatch(Optional.of(1200.0))
                .madeTime(Optional.of(LocalDate.of(2024, 4, 13)))
                .build();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(post(WINE_BATCH_ENDPOINT)
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isCreated())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineBatchResponse actualWineBatch = convertServletResponseToDto(response);
        WineBatch persistedWineBatch = wineBatchRepository.findById(actualWineBatch.id()).orElseThrow();

        assertSoftly(softly -> {
            softly.assertThat(actualWineBatch.number())
                    .isEqualTo(request.number());
            softly.assertThat(actualWineBatch.wineType())
                    .isEqualTo(request.wineType().orElse(""));
            softly.assertThat(actualWineBatch.litersInBatch())
                    .isEqualTo(request.litersInBatch().orElse(0.0));
            softly.assertThat(actualWineBatch.madeTime())
                    .isEqualTo(request.madeTime().orElse(EPOCH));
        });

        assertSoftly(softly -> {
            softly.assertThat(persistedWineBatch.getNumber())
                    .isEqualTo(request.number());
            softly.assertThat(persistedWineBatch.getWineType())
                    .isEqualTo(request.wineType().orElse(""));
            softly.assertThat(persistedWineBatch.getLitersInBatch())
                    .isEqualTo(request.litersInBatch().orElse(0.0));
            softly.assertThat(persistedWineBatch.getMadeTime())
                    .isEqualTo(request.madeTime().orElse(EPOCH));
        });
    }

    @Test
    void readPreparedWineBatch() {
        List<WineBatch> preparedWineBatches = wineBatchRepository.getPreparedWineBatches();

        if (preparedWineBatches.size() < 1) {
            fail("No prepared batches are in test database");
            return;
        }

        WineBatch expectedBatch = preparedWineBatches.get(0);

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(get(WINE_BATCH_ENDPOINT + "/{id}", expectedBatch.getId())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineBatchResponse actualWineBatch = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            // Should be filled
            softly.assertThat(actualWineBatch.id())
                    .isEqualTo(expectedBatch.getId());
            softly.assertThat(actualWineBatch.number())
                    .isEqualTo(expectedBatch.getNumber());
            softly.assertThat(actualWineBatch.wineType())
                    .isEqualTo(expectedBatch.getWineType());
            softly.assertThat(actualWineBatch.ingredients())
                    .containsExactlyInAnyOrderEntriesOf(expectedBatch.getIngredients());
            softly.assertThat(actualWineBatch.status())
                    .isEqualTo(expectedBatch.getStatus().getName());
            softly.assertThat(actualWineBatch.madeTime())
                    .isEqualTo(expectedBatch.getMadeTime());
            softly.assertThat(actualWineBatch.litersInBatch())
                    .isEqualTo(expectedBatch.getLitersInBatch());
            // Should not be filled
            softly.assertThat(actualWineBatch.openedTime()).isNull();
            softly.assertThat(actualWineBatch.bottledTime()).isNull();
            softly.assertThat(actualWineBatch.sugarLevel()).isNull();
            softly.assertThat(actualWineBatch.acidityLevel()).isNull();
            softly.assertThat(actualWineBatch.alcoholLevel()).isNull();
            softly.assertThat(actualWineBatch.dryness()).isNull();
        });
    }

    @Test
    void readOpenedWineBatch() {
        List<WineBatch> openedWineBatches = wineBatchRepository.getOpenedWineBatches();

        if (openedWineBatches.size() < 1) {
            fail("No opened batches are in test database");
            return;
        }

        WineBatch expectedBatch = openedWineBatches.get(0);

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(get(WINE_BATCH_ENDPOINT + "/{id}", expectedBatch.getId())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineBatchResponse actualWineBatch = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            // Should be filled
            softly.assertThat(actualWineBatch.id())
                    .isEqualTo(expectedBatch.getId());
            softly.assertThat(actualWineBatch.number())
                    .isEqualTo(expectedBatch.getNumber());
            softly.assertThat(actualWineBatch.wineType())
                    .isEqualTo(expectedBatch.getWineType());
            softly.assertThat(actualWineBatch.ingredients())
                    .containsExactlyInAnyOrderEntriesOf(expectedBatch.getIngredients());
            softly.assertThat(actualWineBatch.status())
                    .isEqualTo(expectedBatch.getStatus().getName());
            softly.assertThat(actualWineBatch.madeTime())
                    .isEqualTo(expectedBatch.getMadeTime());
            softly.assertThat(actualWineBatch.litersInBatch())
                    .isEqualTo(expectedBatch.getLitersInBatch());
            softly.assertThat(actualWineBatch.openedTime())
                    .isEqualTo(expectedBatch.getOpenedTime());
            softly.assertThat(actualWineBatch.sugarLevel())
                    .isEqualTo(expectedBatch.getSugarLevel());
            softly.assertThat(actualWineBatch.acidityLevel())
                    .isEqualTo(expectedBatch.getAcidityLevel());
            softly.assertThat(actualWineBatch.alcoholLevel())
                    .isEqualTo(expectedBatch.getAlcoholLevel());
            softly.assertThat(actualWineBatch.dryness())
                    .isEqualTo(expectedBatch.getDryness().getName());
            // Should not be filled
            softly.assertThat(actualWineBatch.bottledTime()).isNull();
        });
    }

    @Test
    void readBottledWineBatch() {
        List<WineBatch> bottledWineBatches = wineBatchRepository.getBottledWineBatches();

        if (bottledWineBatches.size() < 1) {
            fail("No bottled batches are in test database");
            return;
        }

        WineBatch expectedBatch = bottledWineBatches.get(0);

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(get(WINE_BATCH_ENDPOINT + "/{id}", expectedBatch.getId())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineBatchResponse actualWineBatch = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            // Should be filled
            softly.assertThat(actualWineBatch.id())
                    .isEqualTo(expectedBatch.getId());
            softly.assertThat(actualWineBatch.number())
                    .isEqualTo(expectedBatch.getNumber());
            softly.assertThat(actualWineBatch.wineType())
                    .isEqualTo(expectedBatch.getWineType());
            softly.assertThat(actualWineBatch.ingredients())
                    .containsExactlyInAnyOrderEntriesOf(expectedBatch.getIngredients());
            softly.assertThat(actualWineBatch.status())
                    .isEqualTo(expectedBatch.getStatus().getName());
            softly.assertThat(actualWineBatch.madeTime())
                    .isEqualTo(expectedBatch.getMadeTime());
            softly.assertThat(actualWineBatch.litersInBatch())
                    .isEqualTo(expectedBatch.getLitersInBatch());
            softly.assertThat(actualWineBatch.openedTime())
                    .isEqualTo(expectedBatch.getOpenedTime());
            softly.assertThat(actualWineBatch.sugarLevel())
                    .isEqualTo(expectedBatch.getSugarLevel());
            softly.assertThat(actualWineBatch.acidityLevel())
                    .isEqualTo(expectedBatch.getAcidityLevel());
            softly.assertThat(actualWineBatch.alcoholLevel())
                    .isEqualTo(expectedBatch.getAlcoholLevel());
            softly.assertThat(actualWineBatch.dryness())
                    .isEqualTo(expectedBatch.getDryness().getName());
            softly.assertThat(actualWineBatch.bottledTime())
                    .isEqualTo(expectedBatch.getBottledTime());
        });
    }

    @Test
    void updateWineBatch() {
        WineBatchUpdateRequest request = WineBatchUpdateRequest.builder()
                .wineType(Optional.of("Updated wine type"))
                .build();

        List<WineBatch> wineBatches = wineBatchRepository.findAll();

        if (wineBatches.size() < 1) {
            fail("No wine batches are in test database");
            return;
        }

        WineBatch originalWineBatch = wineBatches.get(0);

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(put(WINE_BATCH_ENDPOINT + "/{id}", originalWineBatch.getId())
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineBatchResponse actualWineBatch = convertServletResponseToDto(response);
        WineBatch persistedWineBatch = wineBatchRepository.findById(actualWineBatch.id()).orElseThrow();

        assertSoftly(softly ->
                softly.assertThat(actualWineBatch.wineType())
                .isEqualTo(request.wineType().orElse(""))
        );

        assertSoftly(softly ->
                softly.assertThat(persistedWineBatch.getWineType())
                .isEqualTo(request.wineType().orElse(""))
        );
    }

    @Test
    void deleteWineBatch() {
        List<WineBatch> preparedWineBatches = wineBatchRepository.getPreparedWineBatches();

        if (preparedWineBatches.size() < 1) {
            fail("No prepared batches are in test database");
            return;
        }

        WineBatch expectedBatch = preparedWineBatches.get(0);

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(delete(WINE_BATCH_ENDPOINT + "/{id}", expectedBatch.getId())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(response).isNotNull();

        WineBatchResponse actualWineBatch = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            // Should be filled
            softly.assertThat(actualWineBatch.id())
                    .isEqualTo(expectedBatch.getId());
            softly.assertThat(actualWineBatch.number())
                    .isEqualTo(expectedBatch.getNumber());
            softly.assertThat(actualWineBatch.wineType())
                    .isEqualTo(expectedBatch.getWineType());
            softly.assertThat(actualWineBatch.ingredients())
                    .containsExactlyInAnyOrderEntriesOf(expectedBatch.getIngredients());
            softly.assertThat(actualWineBatch.status())
                    .isEqualTo(expectedBatch.getStatus().getName());
            softly.assertThat(actualWineBatch.madeTime())
                    .isEqualTo(expectedBatch.getMadeTime());
            softly.assertThat(actualWineBatch.litersInBatch())
                    .isEqualTo(expectedBatch.getLitersInBatch());
            // Should not be filled
            softly.assertThat(actualWineBatch.openedTime()).isNull();
            softly.assertThat(actualWineBatch.bottledTime()).isNull();
            softly.assertThat(actualWineBatch.sugarLevel()).isNull();
            softly.assertThat(actualWineBatch.acidityLevel()).isNull();
            softly.assertThat(actualWineBatch.alcoholLevel()).isNull();
            softly.assertThat(actualWineBatch.dryness()).isNull();
        });

        assertThat(wineBatchRepository.findById(expectedBatch.getId())).isEmpty();
    }

    @Test
    void deleteBottleWineBatch() {
        List<WineBatch> bottledWineBatches = wineBatchRepository.getBottledWineBatches();

        if (bottledWineBatches.size() < 1) {
            fail("No bottled batches are in test database");
            return;
        }

        WineBatch expectedBatch = bottledWineBatches.get(0);

        try {
            mockMvc.perform(delete(WINE_BATCH_ENDPOINT + "/{id}", expectedBatch.getId())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isMethodNotAllowed())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            fail(e.getMessage(), e);
        }

        assertThat(wineBatchRepository.findById(expectedBatch.getId())).isNotEmpty();
    }

    private String convertRequestToJson(Object request) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.writeValueAsString(request);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return "";
        }
    }

    private WineBatchResponse convertServletResponseToDto(MockHttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), WineBatchResponse.class);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return WineBatchResponse.builder().build();
        }
    }
}
