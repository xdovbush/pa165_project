package edu.pa165.productionservice.persistence.winerecipe;

import edu.pa165.productionservice.model.RecordBehavior;
import edu.pa165.productionservice.model.RecordValidator;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import edu.pa165.productionservice.model.winerecipe.WineRecipeBehavior;
import edu.pa165.productionservice.model.winerecipe.WineRecipeValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class WineRecipeOrchestratorUnitTest {
    private static final Long RECIPE_ID = 1L;

    private WineRecipeRepository repository;
    private RecordValidator<WineRecipe> validator;
    private RecordBehavior<WineRecipe> behavior;
    private WineRecipeOrchestrator orchestrator;
    private WineRecipe oldRecipe;
    private WineRecipe newRecipe;
    private WineRecipe savedRecipe;

    @BeforeEach
    void setUp() {
        oldRecipe = new WineRecipe();
        newRecipe = new WineRecipe();
        savedRecipe = new WineRecipe();

        oldRecipe.setId(RECIPE_ID);
        newRecipe.setId(RECIPE_ID);
        savedRecipe.setId(RECIPE_ID);

        validator = mock(WineRecipeValidator.class);
        behavior = mock(WineRecipeBehavior.class);
        repository = mock(WineRecipeRepository.class);

        when(repository.findById(RECIPE_ID)).thenReturn(Optional.of(oldRecipe));
        when(repository.findAll()).thenReturn(List.of(oldRecipe, newRecipe, savedRecipe));
        when(repository.save(any(WineRecipe.class))).thenReturn(savedRecipe);

        orchestrator = new WineRecipeOrchestrator(repository, validator, behavior);
    }

    @Test
    void create() {
        WineRecipe actualRecipe = orchestrator.create(newRecipe);

        assertThat(actualRecipe).isSameAs(savedRecipe);

        verify(validator).validateCreation(newRecipe);
        verify(behavior).sourceValuesForCreation(newRecipe);
        verify(repository).save(newRecipe);
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void update() {
        WineRecipe actualRecipe = orchestrator.update(newRecipe);

        assertThat(actualRecipe).isSameAs(savedRecipe);

        verify(repository).findById(RECIPE_ID);
        verify(validator).validateUpdate(newRecipe);
        verify(behavior).sourceValuesForUpdate(oldRecipe, newRecipe);
        verify(repository).save(newRecipe);
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void delete() {
        WineRecipe actualRecipe = orchestrator.delete(newRecipe);

        assertThat(actualRecipe).isSameAs(newRecipe);

        verify(validator).validateDelete(newRecipe);
        verify(repository).delete(newRecipe);
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void deleteById() {
        WineRecipe actualRecipe = orchestrator.deleteById(RECIPE_ID);

        assertThat(actualRecipe).isSameAs(oldRecipe);

        verify(repository).findById(RECIPE_ID);
        verify(validator).validateDelete(oldRecipe);
        verify(repository).delete(oldRecipe);
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void getAll() {
        List<WineRecipe> wineRecipes = orchestrator.getAll();

        assertThat(wineRecipes).containsExactlyInAnyOrder(oldRecipe, newRecipe, savedRecipe);

        verify(repository).findAll();
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void getById() {
        Optional<WineRecipe> actualRecipe = orchestrator.getById(RECIPE_ID);

        assertThat(actualRecipe).contains(oldRecipe);

        verify(repository).findById(RECIPE_ID);
        verifyNoMoreInteractions(validator, behavior, repository);
    }
}