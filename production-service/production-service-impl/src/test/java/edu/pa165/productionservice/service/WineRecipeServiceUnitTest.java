package edu.pa165.productionservice.service;

import edu.pa165.productionservice.dto.WineRecipeCreateRequest;
import edu.pa165.productionservice.dto.WineRecipeResponse;
import edu.pa165.productionservice.dto.WineRecipeUpdateRequest;
import edu.pa165.productionservice.mapping.winerecipe.WineRecipeDtoMapper;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import edu.pa165.productionservice.persistence.winerecipe.WineRecipeOrchestrator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class WineRecipeServiceUnitTest {
    private static final Long RECIPE_ID = 1L;

    private WineRecipeOrchestrator orchestrator;
    private WineRecipeDtoMapper mapper;

    private WineRecipeService service;

    private WineRecipe oldRecipe;
    private WineRecipe newRecipe;
    private WineRecipeResponse oldResponse;
    private WineRecipeResponse newResponse;

    @BeforeEach
    void setUp() {
        oldRecipe = new WineRecipe();
        newRecipe = new WineRecipe();
        oldRecipe.setId(RECIPE_ID);
        newRecipe.setId(RECIPE_ID);

        oldResponse = WineRecipeResponse.builder().build();
        newResponse = WineRecipeResponse.builder().build();

        orchestrator = mock(WineRecipeOrchestrator.class);
        mapper = mock(WineRecipeDtoMapper.class);

        when(orchestrator.getById(RECIPE_ID)).thenReturn(Optional.of(oldRecipe));
        when(orchestrator.getAll()).thenReturn(List.of(oldRecipe, newRecipe));
        when(orchestrator.create(any())).thenReturn(newRecipe);
        when(orchestrator.update(any(), any())).thenReturn(newRecipe);
        when(orchestrator.deleteById(RECIPE_ID)).thenReturn(oldRecipe);

        when(mapper.convertFromRequestForCreate(any())).thenReturn(newRecipe);
        when(mapper.convertFromRequestForUpdate(any(),any())).thenReturn(newRecipe);
        when(mapper.convertToResponse(oldRecipe)).thenReturn(oldResponse);
        when(mapper.convertToResponse(newRecipe)).thenReturn(newResponse);

        service = new WineRecipeService(orchestrator, mapper);
    }

    @Test
    void getRecipe() {
        WineRecipeResponse actualResponse = service.getRecipe(RECIPE_ID);

        assertThat(actualResponse).isSameAs(oldResponse);

        verify(orchestrator).getById(RECIPE_ID);
        verify(mapper).convertToResponse(oldRecipe);
        verifyNoMoreInteractions(orchestrator, mapper);
    }

    @Test
    void getAllRecipes() {
        List<WineRecipeResponse> actualResponse = service.getAllRecipes();

        assertThat(actualResponse).containsExactlyInAnyOrder(oldResponse, newResponse);

        verify(orchestrator).getAll();
        verify(mapper).convertToResponse(oldRecipe);
        verify(mapper).convertToResponse(newRecipe);
        verifyNoMoreInteractions(orchestrator, mapper);
    }

    @Test
    void createRecipe() {
        WineRecipeCreateRequest request = WineRecipeCreateRequest.builder().build();

        WineRecipeResponse actualResponse = service.createRecipe(request);

        assertThat(actualResponse).isSameAs(newResponse);

        verify(mapper).convertFromRequestForCreate(request);
        verify(orchestrator).create(newRecipe);
        verify(mapper).convertToResponse(newRecipe);
        verifyNoMoreInteractions(orchestrator, mapper);
    }

    @Test
    void updateRecipe() {
        WineRecipeUpdateRequest request = WineRecipeUpdateRequest.builder().build();

        WineRecipeResponse actualResponse = service.updateRecipe(RECIPE_ID, request);

        assertThat(actualResponse).isSameAs(newResponse);

        verify(orchestrator).getById(RECIPE_ID);
        verify(mapper).convertFromRequestForUpdate(request, oldRecipe);
        verify(orchestrator).update(oldRecipe, newRecipe);
        verify(mapper).convertToResponse(newRecipe);
        verifyNoMoreInteractions(orchestrator, mapper);
    }

    @Test
    void deleteRecipe() {
        WineRecipeResponse actualResponse = service.deleteRecipe(RECIPE_ID);

        assertThat(actualResponse).isSameAs(oldResponse);

        verify(orchestrator).deleteById(RECIPE_ID);
        verify(mapper).convertToResponse(oldRecipe);
        verifyNoMoreInteractions(orchestrator, mapper);
    }
}