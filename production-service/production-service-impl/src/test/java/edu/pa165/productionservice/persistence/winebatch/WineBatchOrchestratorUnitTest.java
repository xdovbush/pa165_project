package edu.pa165.productionservice.persistence.winebatch;

import edu.pa165.productionservice.model.RecordBehavior;
import edu.pa165.productionservice.model.RecordValidator;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.model.winebatch.WineBatchBehavior;
import edu.pa165.productionservice.model.winebatch.WineBatchValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class WineBatchOrchestratorUnitTest {
    private static final Long BATCH_ID = 1L;

    private WineBatchRepository repository;
    private RecordValidator<WineBatch> validator;
    private RecordBehavior<WineBatch> behavior;
    private WineBatchOrchestrator orchestrator;
    private WineBatch oldBatch;
    private WineBatch newBatch;
    private WineBatch savedBatch;

    @BeforeEach
    void setUp() {
        oldBatch = new WineBatch();
        newBatch = new WineBatch();
        savedBatch = new WineBatch();

        oldBatch.setId(BATCH_ID);
        newBatch.setId(BATCH_ID);
        savedBatch.setId(BATCH_ID);

        validator = mock(WineBatchValidator.class);
        behavior = mock(WineBatchBehavior.class);
        repository = mock(WineBatchRepository.class);

        when(repository.findById(BATCH_ID)).thenReturn(Optional.of(oldBatch));
        when(repository.findAll()).thenReturn(List.of(oldBatch, newBatch, savedBatch));
        when(repository.save(any(WineBatch.class))).thenReturn(savedBatch);

        orchestrator = new WineBatchOrchestrator(repository, validator, behavior);
    }

    @Test
    void create() {
        WineBatch actualBatch = orchestrator.create(newBatch);

        assertThat(actualBatch).isSameAs(savedBatch);

        verify(validator).validateCreation(newBatch);
        verify(behavior).sourceValuesForCreation(newBatch);
        verify(repository).save(newBatch);
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void update() {
        WineBatch actualBatch = orchestrator.update(newBatch);

        assertThat(actualBatch).isSameAs(savedBatch);

        verify(repository).findById(BATCH_ID);
        verify(validator).validateUpdate(newBatch);
        verify(behavior).sourceValuesForUpdate(oldBatch, newBatch);
        verify(repository).save(newBatch);
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void delete() {
        WineBatch actualBatch = orchestrator.delete(newBatch);

        assertThat(actualBatch).isSameAs(newBatch);

        verify(validator).validateDelete(newBatch);
        verify(repository).delete(newBatch);
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void deleteById() {
        WineBatch actualBatch = orchestrator.deleteById(BATCH_ID);

        assertThat(actualBatch).isSameAs(oldBatch);

        verify(repository).findById(BATCH_ID);
        verify(validator).validateDelete(oldBatch);
        verify(repository).delete(oldBatch);
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void getAll() {
        List<WineBatch> wineBatches = orchestrator.getAll();

        assertThat(wineBatches).containsExactlyInAnyOrder(oldBatch, newBatch, savedBatch);

        verify(repository).findAll();
        verifyNoMoreInteractions(validator, behavior, repository);
    }

    @Test
    void getById() {
        Optional<WineBatch> actualBatch = orchestrator.getById(BATCH_ID);

        assertThat(actualBatch).contains(oldBatch);

        verify(repository).findById(BATCH_ID);
        verifyNoMoreInteractions(validator, behavior, repository);
    }
}