package edu.pa165.productionservice.model.winerecipe;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

import static edu.pa165.productionservice.TestUtils.createDummyWineRecipe;

class WineRecipeBehaviorUnitTest {

    private final WineRecipeBehavior behavior = new WineRecipeBehavior();

    @Test
    void sourceValuesForCreation() {
        WineRecipe recipe = createDummyWineRecipe();

        behavior.sourceValuesForCreation(recipe);

        // Assert that audit fields are set correctly
        SoftAssertions.assertSoftly(
                softly -> softly.assertThat(recipe.getCreatedDate()).isEqualTo(recipe.getLastModifiedDate())
        );
    }

    @Test
    void sourceValuesForUpdate() {
        WineRecipe originalRecipe = createDummyWineRecipe();
        WineRecipe updatedRecipe = createDummyWineRecipe();

        behavior.sourceValuesForUpdate(originalRecipe, updatedRecipe);

        // Assert that audit fields are set correctly
        SoftAssertions.assertSoftly(
                softly -> softly.assertThat(updatedRecipe.getCreatedDate()).isEqualTo(originalRecipe.getCreatedDate())
        );
    }
}