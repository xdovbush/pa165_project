package edu.pa165.productionservice.e2e.annotation;

import edu.pa165.productionservice.e2e.configuration.TestEnvironmentConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(TestEnvironmentConfiguration.class)
@DirtiesContext
public @interface UseTestData {
}
