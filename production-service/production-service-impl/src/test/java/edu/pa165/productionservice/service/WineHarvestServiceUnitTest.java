package edu.pa165.productionservice.service;

import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.persistence.webservice.harvest.HarvestInventoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static edu.pa165.productionservice.TestUtils.createDummyWineBatch;
import static org.mockito.Mockito.*;

class WineHarvestServiceUnitTest {
    private HarvestInventoryRepository harvestRepository;

    private WineHarvestService service;

    @BeforeEach
    void setUp() {
        harvestRepository = mock(HarvestInventoryRepository.class);
        when(harvestRepository.isGrapesAvailable(any(), any())).thenReturn(Boolean.TRUE);
        service = new WineHarvestService(harvestRepository);
    }
    @Test
    void grapesAvailableForCreate() {
        WineBatch batch = createDummyWineBatch();

        service.grapesAvailableForCreate(batch);

        for(var entry : batch.getIngredients().entrySet()) {
            verify(harvestRepository).isGrapesAvailable(entry.getKey(), entry.getValue());
        }
        verifyNoMoreInteractions(harvestRepository);
    }

    @Test
    void grapesAvailableForUpdate() {
        WineBatch oldBatch = createDummyWineBatch();
        WineBatch newBatch = createDummyWineBatch();

        oldBatch.setIngredients(Map.of());

        service.grapesAvailableForUpdate(oldBatch, newBatch);

        for(var entry : newBatch.getIngredients().entrySet()) {
            verify(harvestRepository).isGrapesAvailable(entry.getKey(), entry.getValue());
        }
        verifyNoMoreInteractions(harvestRepository);
    }

    @Test
    void onBatchCreate() {
        WineBatch batch = createDummyWineBatch();

        service.onBatchCreate(batch);

        for(var entry : batch.getIngredients().entrySet()) {
            verify(harvestRepository).removeGrapesFromHarvest(entry.getKey(), entry.getValue());
        }
        verifyNoMoreInteractions(harvestRepository);
    }

    @Test
    void onBatchUpdate_AddedQuantity() {
        WineBatch oldBatch = createDummyWineBatch();
        WineBatch newBatch = createDummyWineBatch();

        oldBatch.setIngredients(Map.of());

        service.onBatchUpdate(oldBatch, newBatch);

        for(var entry : newBatch.getIngredients().entrySet()) {
            verify(harvestRepository).removeGrapesFromHarvest(entry.getKey(), entry.getValue());
        }
        verifyNoMoreInteractions(harvestRepository);
    }

    @Test
    void onBatchUpdate_RemovedQuantity() {
        WineBatch oldBatch = createDummyWineBatch();
        WineBatch newBatch = createDummyWineBatch();

        newBatch.setIngredients(Map.of());

        service.onBatchUpdate(oldBatch, newBatch);

        for(var entry : newBatch.getIngredients().entrySet()) {
            verify(harvestRepository).addGrapesToHarvest(entry.getKey(), entry.getValue());
        }
        verifyNoMoreInteractions(harvestRepository);
    }

    @Test
    void onBatchDelete() {
        WineBatch batch = createDummyWineBatch();

        service.onBatchDelete(batch);

        for(var entry : batch.getIngredients().entrySet()) {
            verify(harvestRepository).addGrapesToHarvest(entry.getKey(), entry.getValue());
        }
        verifyNoMoreInteractions(harvestRepository);
    }
}