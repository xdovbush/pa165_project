package edu.pa165.productionservice;

import edu.pa165.productionservice.model.winebatch.BatchStatus;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.model.winebatch.WineDryness;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Map;

public class TestUtils {
    private static final LocalDate MADE_DATE = LocalDate.of(2024, 2, 29);
    private static final Instant CREATED_DATE = Instant.parse("2024-02-28T18:35:24.00Z");

    public static WineBatch createDummyWineBatch() {
        return createDummyWineBatch(BatchStatus.PREPARED);
    }

    public static WineBatch createDummyWineBatch(BatchStatus status) {
        return WineBatch.builder()
                .number(12345L)
                .wineType("Test wine type")
                .ingredients(
                        Map.of(
                                1L, 150.0,
                                2L, 250.0
                        )
                )
                .status(status)
                .madeTime(MADE_DATE)
                .openedTime(MADE_DATE.plusDays(1L))
                .bottledTime(MADE_DATE.plusDays(2L))
                .litersInBatch(1221.0)
                .sugarLevel(3.4)
                .acidityLevel(7.0)
                .alcoholLevel(14.0)
                .dryness(WineDryness.DRY)
                .createdDate(CREATED_DATE)
                .lastModifiedDate(Instant.now())
                .build();
    }

    public static WineRecipe createDummyWineRecipe() {
        return createDummyWineRecipe("Test recipe");
    }

    public static WineRecipe createDummyWineRecipe(String name) {
        return WineRecipe.builder()
                .name(name)
                .ingredients(
                        Map.of(
                                "Wine type 1", 30.0,
                                "Wine type 2", 70.0
                        )
                )
                .createdDate(CREATED_DATE)
                .lastModifiedDate(Instant.now())
                .build();
    }
}
