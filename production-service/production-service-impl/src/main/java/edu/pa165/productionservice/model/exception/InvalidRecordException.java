package edu.pa165.productionservice.model.exception;

import static edu.pa165.productionservice.translation.StringTemplateResolver.resolveTranslation;

public class InvalidRecordException extends RuntimeException {
    public InvalidRecordException(String message, Object... args) {
        super(resolveTranslation(message, args));
    }
}
