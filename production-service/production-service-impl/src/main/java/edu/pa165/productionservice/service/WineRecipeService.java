package edu.pa165.productionservice.service;

import edu.pa165.productionservice.dto.WineRecipeCreateRequest;
import edu.pa165.productionservice.dto.WineRecipeResponse;
import edu.pa165.productionservice.dto.WineRecipeUpdateRequest;
import edu.pa165.productionservice.mapping.winerecipe.WineRecipeDtoMapper;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import edu.pa165.productionservice.persistence.winerecipe.WineRecipeOrchestrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class WineRecipeService {
    private final WineRecipeOrchestrator orchestrator;
    private final WineRecipeDtoMapper mapper;

    @Autowired
    WineRecipeService(
            WineRecipeOrchestrator orchestrator,
            WineRecipeDtoMapper mapper
    )
    {
        this.orchestrator = orchestrator;
        this.mapper = mapper;
    }

    @Transactional(readOnly = true)
    public WineRecipeResponse getRecipe(Long id) {
        Optional<WineRecipe> recipe = orchestrator.getById(id);
        return recipe.map(mapper::convertToResponse).orElse(null);
    }

    @Transactional(readOnly = true)
    public List<WineRecipeResponse> getAllRecipes() {
        List<WineRecipe> recipes = orchestrator.getAll();
        return recipes.stream().map(mapper::convertToResponse).toList();
    }

    public WineRecipeResponse createRecipe(WineRecipeCreateRequest request) {
        WineRecipe recipe = mapper.convertFromRequestForCreate(request);
        return mapper.convertToResponse(orchestrator.create(recipe));
    }

    public WineRecipeResponse updateRecipe(Long id, WineRecipeUpdateRequest request) {
        WineRecipe oldRecipe = orchestrator.getById(id).orElseThrow();
        WineRecipe newRecipe = mapper.convertFromRequestForUpdate(request, oldRecipe);
        return mapper.convertToResponse(orchestrator.update(oldRecipe, newRecipe));
    }

    public WineRecipeResponse deleteRecipe(Long id) {
        return mapper.convertToResponse(orchestrator.deleteById(id));
    }
}
