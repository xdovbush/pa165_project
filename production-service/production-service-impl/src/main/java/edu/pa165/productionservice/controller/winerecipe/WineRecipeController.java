package edu.pa165.productionservice.controller.winerecipe;

import edu.pa165.productionservice.dto.*;
import edu.pa165.productionservice.service.WineRecipeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static edu.pa165.productionservice.api.ProductionServiceApi.WINE_RECIPE_ENDPOINT;

@RestController
@RequestMapping(WINE_RECIPE_ENDPOINT)
public class WineRecipeController {

    private final WineRecipeService service;

    @Autowired
    WineRecipeController(WineRecipeService service) {
        this.service = service;
    }

    @GetMapping
    @Operation(
            summary = "Gets all wine recipes",
            description = """
                    Returns a list of existing wine recipes.
                    """
    )
    @ResponseStatus(HttpStatus.OK)
    public List<WineRecipeResponse> getAllWineRecipes() {
        return service.getAllRecipes();
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Gets wine recipe info by recipe ID",
            description = """
                    Returns wine recipe by given as URL param recipe ID.
                    If such wine recipe does not exist, returns empty response.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine recipe was found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineRecipeResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public WineRecipeResponse getWineRecipe(@PathVariable("id") Long id) {
        return service.getRecipe(id);
    }

    @PostMapping
    @Operation(
            summary = "Create new wine recipe",
            description = """
                    Receives WineRecipeCreateRequest DTO that will be converted to entity and saved.
                    All fields are mandatory. Ingredients is a map ingredient-percentage, so sum of all
                    ingredients should give 100.0 percent.
                    Returns WineRecipeResponse DTO that contains saved entity data.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Wine recipe was created",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineRecipeResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public WineRecipeResponse createWineRecipe(@RequestBody WineRecipeCreateRequest request) {
        return service.createRecipe(request);
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Updates existing wine recipe",
            description = """
                    Receives recipe ID as URL parameter and WineRecipeUpdateRequest DTO as requested data update.
                    It will be enough just to have fields that needs to be updated in JSON, others can be omitted.
                    Returns WineRecipeResponse DTO that contains updated entity data.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine recipe was updated",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineRecipeResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public WineRecipeResponse updateWineRecipe(@PathVariable("id") Long id, @RequestBody WineRecipeUpdateRequest request) {
        return service.updateRecipe(id, request);
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Deletes existing wine recipe by ID",
            description = """
                    Deletes wine recipe by given as URL param recipe ID.
                    This action is idempotent.
                    Returns deleted wine recipe, if there was any.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine recipe was deleted/does not exist anymore",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineRecipeResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public WineRecipeResponse deleteWineRecipe(@PathVariable("id") Long id) {
        return service.deleteRecipe(id);
    }
}
