package edu.pa165.productionservice.translation;

public class StringTemplateResolver {
    public static String resolveTranslation(String template, Object... args) {
        return String.format(template, args);
    }

    // Only static class -> no constructor
    private StringTemplateResolver() {}
}
