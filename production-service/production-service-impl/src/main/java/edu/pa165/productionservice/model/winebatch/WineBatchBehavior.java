package edu.pa165.productionservice.model.winebatch;

import edu.pa165.productionservice.model.RecordBehavior;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class WineBatchBehavior implements RecordBehavior<WineBatch> {
    @Override
    public void sourceValuesForCreation(WineBatch batch) {
        nullifyNotRelatedFields(batch);
        // Audit fields
        Instant createdDate = Instant.now();
        batch.setCreatedDate(createdDate);
        batch.setLastModifiedDate(createdDate);
    }

    @Override
    public void sourceValuesForUpdate(WineBatch oldBatch, WineBatch newBatch) {
        nullifyNotRelatedFields(newBatch);
        // Audit fields
        newBatch.setCreatedDate(oldBatch.getCreatedDate());
        newBatch.setLastModifiedDate(Instant.now());
    }

    private void nullifyNotRelatedFields(WineBatch batch) {
        if (batch.getStatus() == BatchStatus.OPENED) {
            batch.setBottledTime(null);
        }
        if (batch.getStatus() == BatchStatus.PREPARED) {
            batch.setBottledTime(null);
            batch.setOpenedTime(null);
            batch.setSugarLevel(null);
            batch.setAcidityLevel(null);
            batch.setAlcoholLevel(null);
            batch.setDryness(null);
        }
    }
}
