package edu.pa165.productionservice.model.winebatch;

import edu.pa165.productionservice.model.RecordValidator;
import edu.pa165.productionservice.model.exception.InvalidRecordException;
import edu.pa165.productionservice.service.exception.CouldNotPerformOperationException;
import edu.pa165.productionservice.translation.ErrorStringTemplates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class WineBatchValidator implements RecordValidator<WineBatch> {

    private final ErrorStringTemplates errors;

    @Autowired
    WineBatchValidator(ErrorStringTemplates errors) {
        this.errors = errors;
    }

    @Override
    public void validateCreation(WineBatch batch) {
        assertMandatoryField(batch.getStatus() == BatchStatus.PREPARED);
        validateMandatoryFieldsForPreparedStatus(batch);
    }

    @Override
    public void validateUpdate(WineBatch batch) {
        switch (batch.getStatus()) {
            case PREPARED -> validateMandatoryFieldsForPreparedStatus(batch);
            case OPENED -> validateMandatoryFieldsForOpenedStatus(batch);
            case BOTTLED -> validateMandatoryFieldsForBrewedStatus(batch);
            default -> throw new InvalidRecordException(
                    errors.BREW_STATUS_SHOULD_BE_ONE_OF_1,
                    Arrays.toString(BatchStatus.values())
            );
        }
    }

    @Override
    public void validateDelete(WineBatch batch) {
        if(batch.getStatus() == BatchStatus.BOTTLED) {
            throw new CouldNotPerformOperationException(errors.BOTTLED_BATCH_COULD_NOT_BE_DELETED);
        }
    }

    private void validateMandatoryFieldsForPreparedStatus(WineBatch batch) {}

    private void validateMandatoryFieldsForOpenedStatus(WineBatch batch) {
        // Each next status depends on previous statuses' validation.
        validateMandatoryFieldsForPreparedStatus(batch);

        assertMandatoryField(batch.getNumber() != null);
        assertMandatoryField(batch.getWineType() != null && batch.getWineType().length() > 0);
        assertMandatoryField(batch.getIngredients().size() > 0);
        assertMandatoryField(batch.getMadeTime() != null);
    }
    private void validateMandatoryFieldsForBrewedStatus(WineBatch batch) {
        // Each next status depends on previous statuses' validation.
        validateMandatoryFieldsForOpenedStatus(batch);

        assertMandatoryField(batch.getLitersInBatch() != null && batch.getLitersInBatch() >= 0.0);
        assertMandatoryField(batch.getSugarLevel() != null && batch.getSugarLevel() >= 0.0);
        assertMandatoryField(batch.getAcidityLevel() != null && batch.getAcidityLevel() >= 0.0);
        assertMandatoryField(batch.getAlcoholLevel() != null && batch.getAlcoholLevel() >= 0.0);
        assertMandatoryField(batch.getDryness() != null);
        assertMandatoryField(batch.getOpenedTime() != null);
    }

    private void assertMandatoryField(boolean condition) {
        if(!condition) {
            throw new InvalidRecordException(errors.RECORD_DOES_NOT_HAVE_MANDATORY_FIELDS_SET_PROPERLY);
        }
    }
}
