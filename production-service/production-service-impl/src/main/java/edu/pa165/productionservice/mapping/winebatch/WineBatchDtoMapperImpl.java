package edu.pa165.productionservice.mapping.winebatch;

import edu.pa165.productionservice.dto.WineBatchCreateRequest;
import edu.pa165.productionservice.dto.WineBatchUpdateRequest;
import edu.pa165.productionservice.dto.WineBatchResponse;
import edu.pa165.productionservice.model.winebatch.BatchStatus;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.model.winebatch.WineDryness;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class WineBatchDtoMapperImpl implements WineBatchDtoMapper {

    @Override
    public WineBatchResponse convertToResponse(WineBatch batch) {
        WineBatchResponse.WineBatchResponseBuilder responseBuilder = WineBatchResponse.builder()
                .id(batch.getId())
                .number(batch.getNumber())
                .wineType(batch.getWineType())
                .ingredients(batch.getIngredients())
                .madeTime(batch.getMadeTime())
                .litersInBatch(batch.getLitersInBatch())
                .status((batch.getStatus() != null)? batch.getStatus().getName() : null);

        if (batch.getStatus() != BatchStatus.PREPARED) {
            responseBuilder = responseBuilder.openedTime(batch.getOpenedTime())
                    .sugarLevel(batch.getSugarLevel())
                    .acidityLevel(batch.getAcidityLevel())
                    .alcoholLevel(batch.getAlcoholLevel())
                    .dryness((batch.getDryness() != null)? batch.getDryness().getName() : null);
            if (batch.getStatus() == BatchStatus.BOTTLED) {
                responseBuilder = responseBuilder.bottledTime(batch.getBottledTime());
            }
        }
        return responseBuilder.build();
    }

    @Override
    public WineBatch convertFromRequestForCreate(WineBatchCreateRequest request) {
        return WineBatch.builder()
                .number(request.number())
                .wineType(request.wineType().orElse(null))
                .ingredients(request.ingredients().orElse(Map.of()))
                .madeTime(request.madeTime().orElse(null))
                .litersInBatch(request.litersInBatch().orElse(null))
                .build();
    }

    @Override
    public WineBatch convertFromRequestForUpdate(WineBatchUpdateRequest request, WineBatch originalBatch) {
        return WineBatch.builder()
                .id(originalBatch.getId())
                .number(request.number()
                        .orElse(originalBatch.getNumber()))
                .wineType(request.wineType()
                        .orElse(originalBatch.getWineType()))
                .ingredients(request.ingredients()
                        .orElse(originalBatch.getIngredients()))
                .status(originalBatch.getStatus())
                .madeTime(request.madeTime()
                        .orElse(originalBatch.getMadeTime()))
                .openedTime(request.openedTime()
                        .orElse(originalBatch.getOpenedTime()))
                .bottledTime(request.bottledTime()
                        .orElse(originalBatch.getBottledTime()))
                .litersInBatch(request.litersInBatch()
                        .orElse(originalBatch.getLitersInBatch()))
                .sugarLevel(request.sugarLevel()
                        .orElse(originalBatch.getSugarLevel()))
                .acidityLevel(request.acidityLevel()
                        .orElse(originalBatch.getAcidityLevel()))
                .alcoholLevel(request.alcoholLevel()
                        .orElse(originalBatch.getAlcoholLevel()))
                .dryness(request.dryness().isPresent() ?
                        WineDryness.of(request.dryness().get())
                        :
                        originalBatch.getDryness())
                .build();
    }
}
