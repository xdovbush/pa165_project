package edu.pa165.productionservice.persistence;

import java.util.List;
import java.util.Optional;

public interface RecordOrchestrator<T> {
    T create(T object);
    T update(T newObject);
    T update(T oldObject, T newObject);
    T delete(T object);
    T deleteById(Long id);
    List<T> getAll();
    Optional<T> getById(Long id);
}
