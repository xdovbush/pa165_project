package edu.pa165.productionservice.model.winerecipe;

import edu.pa165.productionservice.model.RecordValidator;
import edu.pa165.productionservice.model.exception.InvalidRecordException;
import edu.pa165.productionservice.translation.ErrorStringTemplates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class WineRecipeValidator implements RecordValidator<WineRecipe> {

    private final ErrorStringTemplates errors;

    @Autowired
    WineRecipeValidator(ErrorStringTemplates errors) {
        this.errors = errors;
    }

    @Override
    public void validateCreation(WineRecipe recipe) {
        assertIngredientsAddUpTo100(recipe.getIngredients());
    }

    @Override
    public void validateUpdate(WineRecipe recipe) {
        assertIngredientsAddUpTo100(recipe.getIngredients());
    }

    @Override
    public void validateDelete(WineRecipe recipe) {}

    private void assertIngredientsAddUpTo100(Map<String, Double> ingredients) {
        double sum = ingredients.values().stream().mapToDouble(d -> d).sum();
        assertMandatoryField(sum == 100.0);
    }

    private void assertMandatoryField(boolean condition) {
        if(!condition) {
            throw new InvalidRecordException(errors.RECORD_DOES_NOT_HAVE_MANDATORY_FIELDS_SET_PROPERLY);
        }
    }
}
