package edu.pa165.productionservice.service;

import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.persistence.webservice.harvest.HarvestInventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class WineHarvestService {
    private final HarvestInventoryRepository harvestRepository;

    @Autowired
    WineHarvestService(HarvestInventoryRepository harvestRepository) {
        this.harvestRepository = harvestRepository;
    }

    public boolean grapesAvailableForCreate(WineBatch batch) {
        // Check grapes per harvest
        for (Map.Entry<Long, Double> ingredient : batch.getIngredients().entrySet()) {
            if (!harvestRepository.isGrapesAvailable(ingredient.getKey(), ingredient.getValue())) {
                return false;
            }
        }
        return true;
    }

    public boolean grapesAvailableForUpdate(WineBatch oldBatch, WineBatch newBatch) {
        // Check grapes per harvest
        for (Map.Entry<Long, Double> ingredient : newBatch.getIngredients().entrySet()) {
            Long harvestId = ingredient.getKey();
            double neededQuantity = ingredient.getValue();
            if(oldBatch.getIngredients().containsKey(harvestId)) {
                neededQuantity -= oldBatch.getIngredients().get(harvestId);
            }
            if (neededQuantity > 0.0 && !harvestRepository.isGrapesAvailable(harvestId, neededQuantity)) {
                return false;
            }
        }
        return true;
    }

    public void onBatchCreate(WineBatch batch) {
        // Adjust grape type per harvest
        for (Map.Entry<Long, Double> ingredient : batch.getIngredients().entrySet()) {
            harvestRepository.removeGrapesFromHarvest(ingredient.getKey(), ingredient.getValue());
        }
    }

    public void onBatchUpdate(WineBatch oldBatch, WineBatch newBatch) {
        // Adjust grape type per harvest
        for (Map.Entry<Long, Double> ingredient : newBatch.getIngredients().entrySet()) {
            Long harvestId = ingredient.getKey();
            double neededQuantity = ingredient.getValue();
            if(oldBatch.getIngredients().containsKey(harvestId)) {
                neededQuantity -= oldBatch.getIngredients().get(harvestId);
            }
            if (neededQuantity > 0.0) {
                harvestRepository.removeGrapesFromHarvest(harvestId, neededQuantity);
            }
            else if (neededQuantity < 0.0) {
                harvestRepository.addGrapesToHarvest(harvestId, -neededQuantity);
            }
        }
    }

    public void onBatchDelete(WineBatch batch) {
        // Adjust grape type per harvest
        for (Map.Entry<Long, Double> ingredient : batch.getIngredients().entrySet()) {
            harvestRepository.addGrapesToHarvest(ingredient.getKey(), ingredient.getValue());
        }
    }
}
