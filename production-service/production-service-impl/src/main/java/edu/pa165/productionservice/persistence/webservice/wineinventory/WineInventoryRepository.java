package edu.pa165.productionservice.persistence.webservice.wineinventory;

import edu.pa165.wineinventoryservice.dto.WineCreateRequest;

public interface WineInventoryRepository {
    void saveWineBottles(WineCreateRequest request);
}
