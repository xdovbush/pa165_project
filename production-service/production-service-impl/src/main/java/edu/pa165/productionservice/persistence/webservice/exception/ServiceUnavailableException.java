package edu.pa165.productionservice.persistence.webservice.exception;

import static edu.pa165.productionservice.translation.StringTemplateResolver.resolveTranslation;

public class ServiceUnavailableException extends RuntimeException {
    public ServiceUnavailableException(String message, Object... args) {
        super(resolveTranslation(message, args));
    }
}

