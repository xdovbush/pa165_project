package edu.pa165.productionservice.persistence.webservice.harvest;

public interface HarvestInventoryRepository {
    // TODO: How to specify harvests in a better way?
    boolean isGrapesAvailable(Long harvestId, Double quantity);
    void removeGrapesFromHarvest(Long harvestId, Double quantity);
    void addGrapesToHarvest(Long harvestId, Double quantity);
}
