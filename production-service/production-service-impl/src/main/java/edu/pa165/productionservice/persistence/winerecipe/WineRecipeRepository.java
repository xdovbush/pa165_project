package edu.pa165.productionservice.persistence.winerecipe;

import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WineRecipeRepository extends JpaRepository<WineRecipe, Long> {
    /**
     * Only custom read methods, because any creation/modification/deletion
     * should go through orchestrators to be validated and sourced properly.
     */

    @Query("SELECT r FROM WineRecipe r WHERE indices(r.ingredients) = ?1")
    List<WineRecipe> getWineRecipesWithIngredient(String ingredientName);

    @Query("SELECT r FROM WineRecipe r WHERE r.ingredients IS EMPTY")
    List<WineRecipe> getWineRecipesWithoutIngredients();
}
