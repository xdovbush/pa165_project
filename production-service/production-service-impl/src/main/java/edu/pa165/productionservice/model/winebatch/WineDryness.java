package edu.pa165.productionservice.model.winebatch;

public enum WineDryness {
    BONE_DRY("Bone Dry", 1.0),
    DRY("Dry", 10.0),
    OFF_DRY("Off-Dry", 35.0),
    SWEET("Sweet", 120.0),
    VERY_SWEET("Very Sweet", Double.MAX_VALUE);

    private final String name;
    private final Double maxSugarPerLiter;

    WineDryness(String name, Double maxSugarPerLiter) {
        this.name = name;
        this.maxSugarPerLiter = maxSugarPerLiter;
    }

    public String getName() {
        return this.name;
    }

    public Double getMaxSugarPerLiter() {
        return this.maxSugarPerLiter;
    }

    public static WineDryness of(String name) {
        for (WineDryness wineDryness : values()) {
            if (wineDryness.name.equals(name)) return wineDryness;
        }
        throw new IllegalArgumentException("Such wine dryness does not exist: " + name);
    }
}
