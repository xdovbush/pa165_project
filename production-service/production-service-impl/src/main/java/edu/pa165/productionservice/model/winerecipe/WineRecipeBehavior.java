package edu.pa165.productionservice.model.winerecipe;

import edu.pa165.productionservice.model.RecordBehavior;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class WineRecipeBehavior implements RecordBehavior<WineRecipe> {
    @Override
    public void sourceValuesForCreation(WineRecipe recipe) {
        // Audit fields
        Instant createdDate = Instant.now();
        recipe.setCreatedDate(createdDate);
        recipe.setLastModifiedDate(createdDate);
    }

    @Override
    public void sourceValuesForUpdate(WineRecipe oldRecipe, WineRecipe newRecipe) {
        // Audit fields
        newRecipe.setCreatedDate(oldRecipe.getCreatedDate());
        newRecipe.setLastModifiedDate(Instant.now());
    }
}
