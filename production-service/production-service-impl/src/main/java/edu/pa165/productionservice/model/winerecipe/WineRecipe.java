package edu.pa165.productionservice.model.winerecipe;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;
import java.util.Map;

@Entity
@Table(name = "AllWineRecipes") // Keep table naming consistent as: All + {Record type in singular form}
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WineRecipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "recipe_ingredient_mapping",
            joinColumns = {@JoinColumn(name = "recipe_id", referencedColumnName = "id")})
    @MapKeyColumn(name = "ingredient_name")
    @Column(name = "ingredients")
    private Map<String, Double> ingredients;

    private Instant createdDate;
    private Instant lastModifiedDate;
}
