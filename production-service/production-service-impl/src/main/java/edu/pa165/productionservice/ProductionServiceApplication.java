package edu.pa165.productionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static edu.pa165.productionservice.api.ProductionServiceApi.NAME;

/**
 * Production service runner
 *
 * @author andrii.dovbush
 * @since 2024.03.10
 */
@SpringBootApplication
public class ProductionServiceApplication {

    public static void main(String[] args) {
        setStaticCommonProperties();

        SpringApplication.run(ProductionServiceApplication.class, args);
    }

    private static void setStaticCommonProperties() {
        System.setProperty("spring.application.name", NAME);
    }

}
