package edu.pa165.productionservice.model;

public interface RecordBehavior<T> {
    void sourceValuesForCreation(T record);

    void sourceValuesForUpdate(T oldRecord, T newRecord);

}
