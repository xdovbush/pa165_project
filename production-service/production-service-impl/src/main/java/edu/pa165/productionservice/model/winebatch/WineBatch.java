package edu.pa165.productionservice.model.winebatch;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Map;

@Entity
@Table(name = "AllWineBatch") // Keep table naming consistent as: All + {Record type in singular form}
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WineBatch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long number;

    private String wineType;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "batch_ingredient_mapping",
            joinColumns = {@JoinColumn(name = "recipe_id", referencedColumnName = "id")})
    @MapKeyColumn(name = "ingredient_name")
    @Column(name = "ingredients")
    // Map of harvestId to quantity used in batch
    private Map<Long, Double> ingredients;

    private BatchStatus status;

    private LocalDate madeTime;
    private LocalDate openedTime;
    private LocalDate bottledTime;

    private Double litersInBatch;

    private Double sugarLevel;
    private Double acidityLevel;
    private Double alcoholLevel;

    private WineDryness dryness;

    private Instant createdDate;
    private Instant lastModifiedDate;
}
