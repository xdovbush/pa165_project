package edu.pa165.productionservice.translation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:production-service.translations")
public class ErrorStringTemplates {

    @Value("${errors.wine-batch-with-id-1-is-not-found}")
    public String WINE_BATCH_WITH_ID_1_IS_NOT_FOUND;

    @Value("${errors.no-grapes-available-in-harvest-inventory}")
    public String NO_GRAPES_AVAILABLE_IN_HARVEST_INVENTORY;

    @Value("${errors.open-action-is-not-possible-to-perform}")
    public String OPEN_ACTION_IS_NOT_POSSIBLE;

    @Value("${errors.bottle-action-is-not-possible-to-perform}")
    public String BOTTLE_ACTION_IS_NOT_POSSIBLE;

    @Value("${errors.bottled-batch-could-not-be-deleted}")
    public String BOTTLED_BATCH_COULD_NOT_BE_DELETED;

    @Value("${errors.brew-status-should-be-one-of-1}")
    public String BREW_STATUS_SHOULD_BE_ONE_OF_1;

    @Value("${errors.record-does-not-have-mandatory-fields-set-properly}")
    public String RECORD_DOES_NOT_HAVE_MANDATORY_FIELDS_SET_PROPERLY;

    @Value("${errors.harvest-with-id-1-cannot-be-updated}")
    public String HARVEST_WITH_ID_1_CANNOT_BE_UPDATED;

    @Value("${errors.harvest-with-id-1-does-not-exist}")
    public String HARVEST_WITH_ID_1_DOES_NOT_EXIST;

    @Value("${errors.wine-bottles-cannot-be-created}")
    public String WINE_BOTTLES_CANNOT_BE_CREATED;
}
