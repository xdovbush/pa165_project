package edu.pa165.productionservice.persistence.winerecipe;

import edu.pa165.productionservice.model.RecordBehavior;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import edu.pa165.productionservice.persistence.RecordOrchestrator;
import edu.pa165.productionservice.model.RecordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Orchestrator class exists to perform validations and sourcing of audit info on persisted object.
 */
@Component
@Transactional
public class WineRecipeOrchestrator implements RecordOrchestrator<WineRecipe> {

    private final WineRecipeRepository repository;
    private final RecordValidator<WineRecipe> validator;
    private final RecordBehavior<WineRecipe> behavior;

    @Autowired
    WineRecipeOrchestrator(
            WineRecipeRepository repository,
            RecordValidator<WineRecipe> validator,
            RecordBehavior<WineRecipe> behavior
    ) {
        this.repository = repository;
        this.validator = validator;
        this.behavior = behavior;
    }

    @Override
    public WineRecipe create(WineRecipe recipe) {
        validator.validateCreation(recipe);
        behavior.sourceValuesForCreation(recipe);
        return repository.save(recipe);
    }

    @Override
    public WineRecipe update(WineRecipe newRecipe) {
        Optional<WineRecipe> optionalRecipe = this.getById(newRecipe.getId());
        if (optionalRecipe.isEmpty()) {
            return null;
        }
        WineRecipe oldRecipe = optionalRecipe.get();
        return this.update(oldRecipe, newRecipe);
    }

    @Override
    public WineRecipe update(WineRecipe oldRecipe, WineRecipe newRecipe) {
        validator.validateUpdate(newRecipe);
        behavior.sourceValuesForUpdate(oldRecipe, newRecipe);
        return repository.save(newRecipe);
    }

    @Override
    public WineRecipe delete(WineRecipe recipe) {
        validator.validateDelete(recipe);
        repository.delete(recipe);
        return recipe;
    }

    @Override
    public WineRecipe deleteById(Long id) {
        Optional<WineRecipe> recipe = this.getById(id);
        if (recipe.isEmpty()) {
            return null;
        }
        return delete(recipe.get());
    }

    @Override
    public List<WineRecipe> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<WineRecipe> getById(Long id) {
        return repository.findById(id);
    }
}
