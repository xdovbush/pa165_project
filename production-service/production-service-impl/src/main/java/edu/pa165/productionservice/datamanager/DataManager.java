package edu.pa165.productionservice.datamanager;

import edu.pa165.productionservice.model.winebatch.BatchStatus;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.model.winebatch.WineDryness;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import edu.pa165.productionservice.persistence.winebatch.WineBatchRepository;
import edu.pa165.productionservice.persistence.winerecipe.WineRecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Map;

@Component
@Profile({"dev", "docker"})
@Transactional
public class DataManager implements ApplicationRunner {

    private final WineBatchRepository wineBatchRepository;
    private final WineRecipeRepository wineRecipeRepository;

    @Autowired
    public DataManager(
            WineBatchRepository wineBatchRepository,
            WineRecipeRepository wineRecipeRepository
    ) {
        this.wineBatchRepository = wineBatchRepository;
        this.wineRecipeRepository = wineRecipeRepository;
    }

    public void cleanDb() {
        wineBatchRepository.deleteAll();
        wineRecipeRepository.deleteAll();
    }

    public void fillDb() {
        if (wineBatchRepository.count() == 0L && wineRecipeRepository.count() == 0L)
        {
            wineBatchRepository.save(
                    WineBatch.builder()
                            .number(1L)
                            .wineType("Chardonnay")
                            .ingredients(
                                    Map.of(1L, 1000.0)
                            )
                            .status(BatchStatus.PREPARED)
                            .madeTime(LocalDate.of(2023, 9, 30))
                            .createdDate(Instant.now())
                            .lastModifiedDate(Instant.now())
                            .build()
            );
            wineBatchRepository.save(
                    WineBatch.builder()
                            .number(2L)
                            .wineType("Merlot")
                            .ingredients(
                                    Map.of(2L, 800.0)
                            )
                            .status(BatchStatus.OPENED)
                            .madeTime(LocalDate.of(2023, 10, 15))
                            .openedTime(LocalDate.of(2023, 10, 20))
                            .litersInBatch(800.0)
                            .sugarLevel(2.0)
                            .acidityLevel(6.0)
                            .alcoholLevel(14.0)
                            .dryness(WineDryness.BONE_DRY)
                            .createdDate(Instant.now())
                            .lastModifiedDate(Instant.now())
                            .build()
            );
            wineBatchRepository.save(
                    WineBatch.builder()
                            .number(1003L)
                            .wineType("Cabernet Sauvignon")
                            .ingredients(
                                    Map.of(2L, 1200.0)
                            )
                            .status(BatchStatus.BOTTLED)
                            .madeTime(LocalDate.of(2023, 10, 25))
                            .openedTime(LocalDate.of(2023, 11, 20))
                            .bottledTime(LocalDate.of(2024, 1, 10))
                            .litersInBatch(1200.0)
                            .sugarLevel(4.5)
                            .acidityLevel(6.8)
                            .alcoholLevel(14.5)
                            .dryness(WineDryness.DRY)
                            .createdDate(Instant.now())
                            .lastModifiedDate(Instant.now())
                            .build()
            );
            wineBatchRepository.save(
                    WineBatch.builder()
                            .number(2L)
                            .wineType("Pinot Noir")
                            .ingredients(
                                    Map.of(4L, 900.0)
                            )
                            .status(BatchStatus.OPENED)
                            .madeTime(LocalDate.of(2023, 10, 18))
                            .openedTime(LocalDate.of(2024, 1, 13))
                            .litersInBatch(900.0)
                            .sugarLevel(20.0)
                            .acidityLevel(6.0)
                            .alcoholLevel(14.0)
                            .dryness(WineDryness.SWEET)
                            .createdDate(Instant.now())
                            .lastModifiedDate(Instant.now())
                            .build()
            );
            wineRecipeRepository.save(
                    WineRecipe.builder()
                            .name("Chardonnay Wine Recipe")
                            .ingredients(
                                    Map.of(
                                            "Chardonnay grapes", 92.0,
                                            "Yeast", 2.0,
                                            "Oak chips",  6.0
                                    )
                            )
                            .createdDate(Instant.now())
                            .lastModifiedDate(Instant.now())
                            .build()
            );
            wineRecipeRepository.save(
                    WineRecipe.builder()
                            .name("Merlot Wine Recipe")
                            .ingredients(
                                    Map.of(
                                            "Merlot grapes", 91.1,
                                            "Yeast", 2.0,
                                            "Oak cubes",  6.9
                                    )
                            )
                            .createdDate(Instant.now())
                            .lastModifiedDate(Instant.now())
                            .build()
            );
            wineRecipeRepository.save(
                    WineRecipe.builder()
                            .name("Cabernet Sauvignon Wine Recipe")
                            .ingredients(
                                    Map.of(
                                            "Cabernet Sauvignon grapes", 91.3,
                                            "Yeast", 2.3,
                                            "Oak cubes",  6.4
                                    )
                            )
                            .createdDate(Instant.now())
                            .lastModifiedDate(Instant.now())
                            .build()
            );
            wineRecipeRepository.save(
                    WineRecipe.builder()
                            .name("Pinot Noir Wine Recipe")
                            .ingredients(
                                    Map.of(
                                            "Pinot Noir grapes", 90.5,
                                            "Yeast", 2.3,
                                            "American oak chips",  7.2
                                    )
                            )
                            .createdDate(Instant.now())
                            .lastModifiedDate(Instant.now())
                            .build()
            );
        }
    }

    @Autowired
    public void run(ApplicationArguments args) {
        fillDb();
    }
}
