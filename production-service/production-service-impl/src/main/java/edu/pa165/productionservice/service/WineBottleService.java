package edu.pa165.productionservice.service;

import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.persistence.webservice.wineinventory.WineInventoryRepository;
import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class WineBottleService {
    private final WineInventoryRepository wineInventory;

    @Autowired
    WineBottleService(WineInventoryRepository wineInventory)
    {
        this.wineInventory = wineInventory;
    }

    public void bottleWineBatch(WineBatch batch, Double litersInBottle, BigDecimal price) {
        WineCreateRequest request = createWineBottles(batch, litersInBottle, price);
        wineInventory.saveWineBottles(request);
    }

    private WineCreateRequest createWineBottles(WineBatch batch, Double litersInBottle, BigDecimal price) {
        int quantity = (int) Math.floor(batch.getLitersInBatch() / litersInBottle);
        return WineCreateRequest.builder()
                .name(batch.getWineType())
                .dryness(batch.getDryness().getName())
                .year(batch.getMadeTime().getYear())
                .sugarLevel(batch.getSugarLevel())
                .acidity(batch.getAcidityLevel())
                .alcoholLevel(batch.getAlcoholLevel())
                .batchNumber(batch.getNumber())
                .quantityLeft(quantity)
                .pricePerBottle(price.doubleValue())
                .build();
    }
}
