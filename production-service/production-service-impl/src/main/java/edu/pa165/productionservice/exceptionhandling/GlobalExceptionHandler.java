package edu.pa165.productionservice.exceptionhandling;

import edu.pa165.productionservice.model.exception.InvalidRecordException;
import edu.pa165.productionservice.persistence.webservice.exception.ServiceUnavailableException;
import edu.pa165.productionservice.service.exception.CouldNotPerformOperationException;
import edu.pa165.productionservice.service.exception.ResourceNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.util.UrlPathHelper;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();

    @ExceptionHandler({ResourceNotFoundException.class})
    public ResponseEntity<ApiError> handleResourceNotFound(
            final ResourceNotFoundException ex,
            final HttpServletRequest request
    ) {
        final ApiError apiError = new ApiError(
                HttpStatus.NOT_FOUND,
                ex.getLocalizedMessage(),
                URL_PATH_HELPER.getRequestUri(request));
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({CouldNotPerformOperationException.class})
    public ResponseEntity<ApiError> handleCouldNotPerformOperation(
            final CouldNotPerformOperationException ex,
            final HttpServletRequest request
    ) {
        final ApiError apiError = new ApiError(
                HttpStatus.METHOD_NOT_ALLOWED,
                ex.getLocalizedMessage(),
                URL_PATH_HELPER.getRequestUri(request));
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ServiceUnavailableException.class})
    public ResponseEntity<ApiError> handleServiceUnavailable(
            final ServiceUnavailableException ex,
            final HttpServletRequest request
    ) {
        final ApiError apiError = new ApiError(
                HttpStatus.SERVICE_UNAVAILABLE,
                ex.getLocalizedMessage(),
                URL_PATH_HELPER.getRequestUri(request));
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({InvalidRecordException.class})
    public ResponseEntity<ApiError> handleInvalidRecord(
            final InvalidRecordException ex,
            final HttpServletRequest request
    ) {
        final ApiError apiError = new ApiError(
                HttpStatus.UNPROCESSABLE_ENTITY,
                ex.getLocalizedMessage(),
                URL_PATH_HELPER.getRequestUri(request));
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ApiError> handleAll(final Exception ex, HttpServletRequest request) {
        final ApiError apiError = new ApiError(
                HttpStatus.INTERNAL_SERVER_ERROR,
                getInitialException(ex).getLocalizedMessage(),
                URL_PATH_HELPER.getRequestUri(request));
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    private Exception getInitialException(Exception ex) {
        while (ex.getCause() != null) {
            ex = (Exception) ex.getCause();
        }
        return ex;
    }

}
