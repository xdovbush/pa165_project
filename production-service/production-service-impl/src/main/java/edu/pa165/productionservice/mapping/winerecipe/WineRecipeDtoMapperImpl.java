package edu.pa165.productionservice.mapping.winerecipe;

import edu.pa165.productionservice.dto.WineRecipeCreateRequest;
import edu.pa165.productionservice.dto.WineRecipeResponse;
import edu.pa165.productionservice.dto.WineRecipeUpdateRequest;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;
import org.springframework.stereotype.Component;

@Component
public class WineRecipeDtoMapperImpl implements WineRecipeDtoMapper {
    @Override
    public WineRecipeResponse convertToResponse(WineRecipe recipe) {
        return WineRecipeResponse.builder()
                .id(recipe.getId())
                .name(recipe.getName())
                .ingredients(recipe.getIngredients())
                .build();
    }

    @Override
    public WineRecipe convertFromRequestForCreate(WineRecipeCreateRequest request) {
        return WineRecipe.builder()
                .name(request.name())
                .ingredients(request.ingredients())
                .build();
    }

    @Override
    public WineRecipe convertFromRequestForUpdate(WineRecipeUpdateRequest request, WineRecipe originalRecipe) {
        return WineRecipe.builder()
                .id(originalRecipe.getId())
                .name(request.name().orElse(originalRecipe.getName()))
                .ingredients(request.ingredients().orElse(originalRecipe.getIngredients()))
                .build();
    }
}
