package edu.pa165.productionservice.service.exception;

import static edu.pa165.productionservice.translation.StringTemplateResolver.resolveTranslation;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message, Object... args) {
        super(resolveTranslation(message, args));
    }
}
