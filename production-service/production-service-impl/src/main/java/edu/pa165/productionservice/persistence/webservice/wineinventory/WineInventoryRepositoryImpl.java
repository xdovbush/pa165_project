package edu.pa165.productionservice.persistence.webservice.wineinventory;

import edu.pa165.productionservice.service.exception.CouldNotPerformOperationException;
import edu.pa165.productionservice.translation.ErrorStringTemplates;
import edu.pa165.wineinventoryservice.api.WineInventoryServiceApi;
import edu.pa165.wineinventoryservice.dto.WineCreateRequest;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Repository
public class WineInventoryRepositoryImpl implements WineInventoryRepository {
    private static final String HTTP = "http://";
    private static final String WINE_SERVICE_ENDPOINT = HTTP
            + WineInventoryServiceApi.NAME
            + WineInventoryServiceApi.WINE_INTERNAL_ENDPOINT;

    private final WebClient.Builder webClientBuilder;
    private final ErrorStringTemplates errors;


    @Autowired
    WineInventoryRepositoryImpl(
            WebClient.Builder webClientBuilder,
            ErrorStringTemplates errors
    ) {
        this.webClientBuilder = webClientBuilder;
        this.errors = errors;
    }
    @Override
    public void saveWineBottles(WineCreateRequest request) {
        sendWineCreationRequest(request);
    }

    private void sendWineCreationRequest(WineCreateRequest request) {
        try{
            webClientBuilder.build()
                    .post()
                    .uri(WINE_SERVICE_ENDPOINT)
                    .body(Mono.just(request), WineCreateRequest.class)
                    .retrieve()
                    .bodyToMono(WineResponse.class)
                    .block();
        } catch (Exception e) {
            throw new CouldNotPerformOperationException(errors.WINE_BOTTLES_CANNOT_BE_CREATED);
        }
    }
}
