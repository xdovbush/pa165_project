package edu.pa165.productionservice.persistence.winebatch;

import edu.pa165.productionservice.model.RecordBehavior;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.persistence.RecordOrchestrator;
import edu.pa165.productionservice.model.RecordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Orchestrator class exists to perform validations and sourcing of audit info on persisted object.
 */
@Component
@Transactional
public class WineBatchOrchestrator implements RecordOrchestrator<WineBatch> {
    private final WineBatchRepository repository;
    private final RecordValidator<WineBatch> validator;
    private final RecordBehavior<WineBatch> behavior;

    @Autowired
    WineBatchOrchestrator(
            WineBatchRepository repository,
            RecordValidator<WineBatch> validator,
            RecordBehavior<WineBatch> behavior
    ) {
        this.repository = repository;
        this.validator = validator;
        this.behavior = behavior;
    }

    @Override
    public WineBatch create(WineBatch batch) {
        validator.validateCreation(batch);
        behavior.sourceValuesForCreation(batch);
        return repository.save(batch);
    }

    @Override
    public WineBatch update(WineBatch newBatch) {
        Optional<WineBatch> optionalBatch = this.getById(newBatch.getId());
        if (optionalBatch.isEmpty()) {
            return null;
        }
        WineBatch oldBatch = optionalBatch.get();
        return this.update(oldBatch, newBatch);
    }

    @Override
    public WineBatch update(WineBatch oldBatch, WineBatch newBatch) {
        validator.validateUpdate(newBatch);
        behavior.sourceValuesForUpdate(oldBatch, newBatch);
        return repository.save(newBatch);
    }

    @Override
    public WineBatch delete(WineBatch batch) {
        validator.validateDelete(batch);
        repository.delete(batch);
        return batch;
    }

    @Override
    public WineBatch deleteById(Long id) {
        Optional<WineBatch> batch = this.getById(id);
        if (batch.isEmpty()) {
            return null;
        }
        return delete(batch.get());
    }

    @Override
    public List<WineBatch> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<WineBatch> getById(Long id) {
        return repository.findById(id);
    }
}
