package edu.pa165.productionservice.model;

public interface RecordValidator<T> {
    void validateCreation(T record);

    void validateUpdate(T record);

    void validateDelete(T record);
}
