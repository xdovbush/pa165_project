package edu.pa165.productionservice.mapping.winerecipe;

import edu.pa165.productionservice.dto.WineRecipeCreateRequest;
import edu.pa165.productionservice.dto.WineRecipeResponse;
import edu.pa165.productionservice.dto.WineRecipeUpdateRequest;
import edu.pa165.productionservice.model.winerecipe.WineRecipe;

public interface WineRecipeDtoMapper {
    WineRecipeResponse convertToResponse(WineRecipe recipe);
    WineRecipe convertFromRequestForCreate(WineRecipeCreateRequest request);
    WineRecipe convertFromRequestForUpdate(WineRecipeUpdateRequest request, WineRecipe originalRecipe);
}
