package edu.pa165.productionservice.model.winebatch;

public enum BatchStatus {
    PREPARED("Prepared"),
    OPENED("Opened"),
    BOTTLED("Bottled");

    private final String name;

    BatchStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public static BatchStatus of(String name) {
        for(BatchStatus status : values()) {
            if(status.name.equals(name)) return status;
        }
        throw new IllegalArgumentException("Such batch status does not exist: " + name);
    }
}
