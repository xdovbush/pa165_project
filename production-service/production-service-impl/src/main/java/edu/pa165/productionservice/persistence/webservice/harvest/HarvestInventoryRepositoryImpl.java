package edu.pa165.productionservice.persistence.webservice.harvest;

import edu.pa165.harvestservice.api.HarvestServiceApi;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import edu.pa165.productionservice.service.exception.CouldNotPerformOperationException;
import edu.pa165.productionservice.service.exception.ResourceNotFoundException;
import edu.pa165.productionservice.translation.ErrorStringTemplates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Repository
public class HarvestInventoryRepositoryImpl implements HarvestInventoryRepository {

    private static final String HTTP = "http://";
    private static final String HARVEST_SERVICE_ENDPOINT = HTTP + HarvestServiceApi.NAME + HarvestServiceApi.HARVEST_ENDPOINT;

    private final WebClient.Builder webClientBuilder;
    private final ErrorStringTemplates errors;


    @Autowired
    HarvestInventoryRepositoryImpl(
            WebClient.Builder webClientBuilder,
            ErrorStringTemplates errors
    ) {
        this.webClientBuilder = webClientBuilder;
        this.errors = errors;
    }

    @Override
    public boolean isGrapesAvailable(Long harvestId, Double quantity) {
        HarvestResponse response = getHarvestFromService(harvestId);
        if(response == null) return false;

        return response.quantity() >= quantity;
    }

    @Override
    public void removeGrapesFromHarvest(Long harvestId, Double quantity) {
        HarvestResponse response = getHarvestFromService(harvestId);
        if(response == null){
            throw new ResourceNotFoundException(errors.HARVEST_WITH_ID_1_DOES_NOT_EXIST, harvestId);
        }

        Double newQuantity = response.quantity() - quantity;
        HarvestUpdateRequest request = mapToUpdateRequestBuilder(response)
                .quantity(newQuantity)
                .build();

        sendHarvestUpdateRequest(request, response.id());
    }

    @Override
    public void addGrapesToHarvest(Long harvestId, Double quantity) {
        HarvestResponse response = getHarvestFromService(harvestId);
        if(response == null){
            throw new ResourceNotFoundException(errors.HARVEST_WITH_ID_1_DOES_NOT_EXIST, harvestId);
        }

        Double newQuantity = response.quantity() + quantity;
        HarvestUpdateRequest request = mapToUpdateRequestBuilder(response)
                .quantity(newQuantity)
                .build();

        sendHarvestUpdateRequest(request, response.id());
    }

    private HarvestResponse getHarvestFromService(Long harvestId) {
        try {
            return webClientBuilder.build()
                    .get()
                    .uri(HARVEST_SERVICE_ENDPOINT+ "/{id}", harvestId)
                    .retrieve()
                    .bodyToMono(HarvestResponse.class)
                    .block();
        } catch (Exception e) {
            return null;
        }
    }

    private void sendHarvestUpdateRequest(HarvestUpdateRequest request, Long harvestId) {
        try{
            webClientBuilder.build()
                    .put()
                    .uri(HARVEST_SERVICE_ENDPOINT + "/{id}", harvestId)
                    .body(Mono.just(request), HarvestUpdateRequest.class)
                    .retrieve()
                    .bodyToMono(HarvestResponse.class)
                    .block();
        } catch (Exception e) {
            throw new CouldNotPerformOperationException(errors.HARVEST_WITH_ID_1_CANNOT_BE_UPDATED, harvestId);
        }
    }

    private HarvestUpdateRequest.HarvestUpdateRequestBuilder mapToUpdateRequestBuilder(HarvestResponse response) {
        return HarvestUpdateRequest.builder()
                .typeOfGrape(response.typeOfGrape())
                .year(response.year())
                .harvestDate(response.harvestDate())
                .quantity(response.quantity())
                .location(response.location())
                .quality(response.quality())
                .additionalNotes(response.additionalNotes())
                .ripenessLevels(response.ripenessLevels())
                .skinAndSeedMaturity(response.skinAndSeedMaturity())
                .healthOfGrapes(response.healthOfGrapes());
    }
}
