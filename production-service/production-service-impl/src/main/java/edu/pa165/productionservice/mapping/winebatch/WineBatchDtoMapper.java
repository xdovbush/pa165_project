package edu.pa165.productionservice.mapping.winebatch;

import edu.pa165.productionservice.dto.WineBatchCreateRequest;
import edu.pa165.productionservice.dto.WineBatchUpdateRequest;
import edu.pa165.productionservice.dto.WineBatchResponse;
import edu.pa165.productionservice.model.winebatch.WineBatch;

public interface WineBatchDtoMapper {
    WineBatchResponse convertToResponse(WineBatch batch);
    WineBatch convertFromRequestForCreate(WineBatchCreateRequest request);
    WineBatch convertFromRequestForUpdate(WineBatchUpdateRequest request, WineBatch originalBatch);
}
