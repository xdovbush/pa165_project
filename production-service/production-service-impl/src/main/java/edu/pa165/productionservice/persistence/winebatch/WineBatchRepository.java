package edu.pa165.productionservice.persistence.winebatch;

import edu.pa165.productionservice.model.winebatch.WineBatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WineBatchRepository extends JpaRepository<WineBatch, Long> {
    /**
     * Only custom read methods, because any creation/modification/deletion
     * should go through orchestrators to be validated and sourced properly.
     */
    @Query("SELECT b FROM WineBatch b WHERE b.status = edu.pa165.productionservice.model.winebatch.BatchStatus.PREPARED")
    List<WineBatch> getPreparedWineBatches();

    @Query("SELECT b FROM WineBatch b WHERE b.status = edu.pa165.productionservice.model.winebatch.BatchStatus.OPENED")
    List<WineBatch> getOpenedWineBatches();

    @Query("SELECT b FROM WineBatch b WHERE b.status = edu.pa165.productionservice.model.winebatch.BatchStatus.BOTTLED")
    List<WineBatch> getBottledWineBatches();
}
