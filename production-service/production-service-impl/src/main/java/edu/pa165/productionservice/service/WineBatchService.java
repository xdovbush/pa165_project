package edu.pa165.productionservice.service;

import edu.pa165.productionservice.dto.*;
import edu.pa165.productionservice.mapping.winebatch.WineBatchDtoMapper;
import edu.pa165.productionservice.model.winebatch.BatchStatus;
import edu.pa165.productionservice.model.winebatch.WineBatch;
import edu.pa165.productionservice.model.winebatch.WineDryness;
import edu.pa165.productionservice.persistence.RecordOrchestrator;
import edu.pa165.productionservice.service.exception.CouldNotPerformOperationException;
import edu.pa165.productionservice.service.exception.ResourceNotFoundException;
import edu.pa165.productionservice.translation.ErrorStringTemplates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class WineBatchService {
    private static final Double LITERS_IN_BOTTLE = 0.75;

    private final WineBatchDtoMapper mapper;
    private final WineBottleService bottleService;
    private final WineHarvestService harvestService;
    private final RecordOrchestrator<WineBatch> orchestrator;
    private final ErrorStringTemplates errors;

    @Autowired
    WineBatchService(
            WineBatchDtoMapper mapper,
            WineBottleService bottleService,
            WineHarvestService harvestService,
            RecordOrchestrator<WineBatch> orchestrator,
            ErrorStringTemplates errors
    ) {
        this.mapper = mapper;
        this.bottleService = bottleService;
        this.harvestService = harvestService;
        this.orchestrator = orchestrator;
        this.errors = errors;
    }

    @Transactional(readOnly = true)
    public WineBatchResponse getBatch(Long id) {
        Optional<WineBatch> batch = orchestrator.getById(id);
        if (batch.isEmpty()) {
            throw new ResourceNotFoundException(errors.WINE_BATCH_WITH_ID_1_IS_NOT_FOUND, id);
        }
        return mapper.convertToResponse(batch.get());
    }

    @Transactional(readOnly = true)
    public List<WineBatchResponse> getAllBatches() {
        List<WineBatch> batches = orchestrator.getAll();
        return batches.stream().map(mapper::convertToResponse).toList();
    }

    public WineBatchResponse createBatch(WineBatchCreateRequest request) {
        WineBatch batch = prepareBatch(request);

        if (harvestService.grapesAvailableForCreate(batch)) {
            harvestService.onBatchCreate(batch);
            batch = orchestrator.create(batch);

            return mapper.convertToResponse(batch);
        }
        throw new CouldNotPerformOperationException(errors.NO_GRAPES_AVAILABLE_IN_HARVEST_INVENTORY);
    }

    public WineBatchResponse updateBatch(Long id, WineBatchUpdateRequest request) {
        WineBatch oldBatch = orchestrator.getById(id)
                .orElseThrow(() -> new ResourceNotFoundException(errors.WINE_BATCH_WITH_ID_1_IS_NOT_FOUND, id));
        WineBatch newBatch = mapper.convertFromRequestForUpdate(request, oldBatch);

        if (harvestService.grapesAvailableForUpdate(oldBatch, newBatch)) {
            harvestService.onBatchUpdate(oldBatch, newBatch);
            newBatch = orchestrator.update(oldBatch, newBatch);

            return mapper.convertToResponse(newBatch);
        }
        throw new CouldNotPerformOperationException(errors.NO_GRAPES_AVAILABLE_IN_HARVEST_INVENTORY);
    }

    public WineBatchResponse deleteBatch(Long id) {
        WineBatch batch = orchestrator.deleteById(id);

        if (batch != null) {
            harvestService.onBatchDelete(batch);
            return mapper.convertToResponse(batch);
        }
        return null;
    }

    public WineBatchResponse openBatch(Long id, WineBatchOpenRequest openRequest) {
        Optional<WineBatch> batchOptional = orchestrator.getById(id);

        if (batchOptional.isPresent() && batchOptional.get().getStatus() == BatchStatus.PREPARED) {
            WineBatch batch = batchOptional.get();
            // Update status
            openBatch(batch, openRequest);
            // Update batch
            return mapper.convertToResponse(orchestrator.update(batch));
        }
        throw new CouldNotPerformOperationException(errors.OPEN_ACTION_IS_NOT_POSSIBLE);
    }

    public WineBatchResponse bottleBatch(Long id, WineBatchBottleRequest request) {
        Optional<WineBatch> batchOptional = orchestrator.getById(id);

        if (batchOptional.isPresent() && batchOptional.get().getStatus() == BatchStatus.OPENED) {
            WineBatch batch = batchOptional.get();
            bottleBatch(batch, request);
            // Updating before calling wine inventory so in case of error no changes will be done other service.
            batch = orchestrator.update(batch);
            // Calling API of other service to create bottles
            bottleService.bottleWineBatch(
                    batch,
                    request.litersInBottle().orElse(LITERS_IN_BOTTLE),
                    request.price().orElse(BigDecimal.ZERO)
            );
            return mapper.convertToResponse(batch);
        }
        throw new CouldNotPerformOperationException(errors.BOTTLE_ACTION_IS_NOT_POSSIBLE);
    }

    private WineBatch prepareBatch(WineBatchCreateRequest request) {
        WineBatch batch = mapper.convertFromRequestForCreate(request);
        batch.setStatus(BatchStatus.PREPARED);
        return batch;
    }

    private void openBatch(WineBatch batch, WineBatchOpenRequest request) {
        batch.setOpenedTime(request.openedTime().orElse(null));
        batch.setSugarLevel(request.sugarLevel().orElse(null));
        batch.setAcidityLevel(request.acidityLevel().orElse(null));
        batch.setAlcoholLevel(request.alcoholLevel().orElse(null));
        batch.setDryness(request.dryness().isPresent() ?
                WineDryness.of(request.dryness().get())
                :
                null
        );
        batch.setStatus(BatchStatus.OPENED);
    }

    private void bottleBatch(WineBatch batch, WineBatchBottleRequest request) {
        batch.setBottledTime(request.bottledTime().orElse(null));
        batch.setStatus(BatchStatus.BOTTLED);
    }
}
