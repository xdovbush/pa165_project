package edu.pa165.productionservice.controller.winebatch;

import edu.pa165.productionservice.dto.*;
import edu.pa165.productionservice.service.WineBatchService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static edu.pa165.productionservice.api.ProductionServiceApi.WINE_BATCH_ENDPOINT;

@RestController
@OpenAPIDefinition(
        info = @Info(title = "Production Service",
                version = "1.0",
                description = """
                        Service for wine batch and recipe management. The API has operations for:
                        - Create wine batch
                        - Read wine batch: all/by ID
                        - Update wine batch: by ID
                        - Open wine batch: by ID
                        - Bottle wine batch: by ID
                        - Delete wine batch: by ID
                        - Create wine recipe
                        - Read wine recipe: all/by ID
                        - Update wine recipe: by ID
                        - Delete wine recipe: by ID
                        - Seed dummy DB data
                        - Clean DB data
                        """,
                contact = @Contact(name = "Andrii Dovbush", email = "555097@mail.muni.cz")
        ),
        security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"}),
        servers = @Server(description = "Localhost", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "9083"),
        })
)
@Tag(name = "Wine production management", description = "Microservice for wine production management")
@RequestMapping(WINE_BATCH_ENDPOINT)
public class WineBatchController {
    private final WineBatchService service;

    @Autowired
    WineBatchController(WineBatchService service) {
        this.service = service;
    }

    @GetMapping
    @Operation(
            summary = "Gets all wine batches",
            description = """
                    Returns a list of existing wine batches.
                    """
    )
    @ResponseStatus(HttpStatus.OK)
    public List<WineBatchResponse> getAllWineBatches() {
        return service.getAllBatches();
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Gets wine batch info by batch ID",
            description = """
                    Returns wine batch by given as URL param batch ID.
                    If such wine batch does not exist, returns empty response.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine batch was found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineBatchResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public WineBatchResponse getWineBatch(@PathVariable("id") Long id) {
        return service.getBatch(id);
    }

    @PostMapping
    @Operation(
            summary = "Create new wine batch",
            description = """
                    Receives WineBatchCreateRequest DTO that will be converted to entity and saved.
                    Only wine batch number must be filled, other fields - optional.
                    Returns WineBatchResponse DTO that contains saved entity data.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Wine batch was created",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineBatchResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public WineBatchResponse createWineBatch(@RequestBody WineBatchCreateRequest request) {
        return service.createBatch(request);
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Updates existing wine batch",
            description = """
                    Receives batch ID as URL parameter and WineBatchUpdateRequest DTO as requested data update.
                    It will be enough just to have fields that needs to be updated in JSON, others can be omitted.
                    Returns WineBatchResponse DTO that contains updated entity data.
                    """
    )
    @ApiResponses(
            value = { 
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine batch was updated",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineBatchResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public WineBatchResponse updateWineBatch(@PathVariable("id") Long id, @RequestBody WineBatchUpdateRequest request) {
        return service.updateBatch(id, request);
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Deletes existing wine batch by ID",
            description = """
                    Deletes wine batch by given as URL param batch ID.
                    This action is idempotent.
                    Returns deleted wine batch, if there was any.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine batch was deleted/does not exist anymore",
                            content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = WineBatchResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public WineBatchResponse deleteWineBatch(@PathVariable("id") Long id) {
        return service.deleteBatch(id);
    }

    @PostMapping("/{id}/open")
    @Operation(
            summary = "Opens existing wine batch is prepared status",
            description = """
                    Requires batch ID as URL parameter. Optionally, additional data can
                    be provided in WineBatchOpenRequest DTO. No need to add/fill all fields.
                    Returns WineBatchResponse DTO that contains updated entity data.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine batch was opened successfully",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineBatchResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public WineBatchResponse openWineBatch(@PathVariable("id") Long id, @RequestBody(required = false) WineBatchOpenRequest request) {
        return service.openBatch(id, request);
    }

    @PostMapping("/{id}/bottle")
    @Operation(
            summary = "Bottles existing wine batch is opened status",
            description = """
                    Requires batch ID as URL parameter. Optionally, additional data can
                    be provided in WineBatchBottleRequest DTO. No need to add/fill all fields.
                    Returns WineBatchResponse DTO that contains updated entity data.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Wine batch was opened successfully",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = WineBatchResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public WineBatchResponse bottleWineBatch(@PathVariable("id") Long id, @RequestBody(required = false) WineBatchBottleRequest request) {
        return service.bottleBatch(id, request);
    }
}
