package edu.pa165.productionservice.api;

/**
 * This is exposed class that can be used in other modules.
 * Please do not put in API module internal classes that should not be used in other services.
 * Such exposed classes should be used only to keep things consistent throughout application.
 *
 * So under this category are: names, endpoints, request/response DTOs.
 */
public interface ProductionServiceApi {
    String NAME = "production-service";
    String WINE_BATCH_ENDPOINT = "/api/batches";
    String WINE_RECIPE_ENDPOINT = "/api/recipes";
    String DATA_ENDPOINT = "/internal/data";
}
