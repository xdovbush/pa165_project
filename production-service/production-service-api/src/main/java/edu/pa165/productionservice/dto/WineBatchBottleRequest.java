package edu.pa165.productionservice.dto;

import lombok.Builder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
@Builder
public record WineBatchBottleRequest(
        Optional<LocalDate> bottledTime,
        Optional<Double> litersInBottle,
        Optional<BigDecimal> price
) {
}
