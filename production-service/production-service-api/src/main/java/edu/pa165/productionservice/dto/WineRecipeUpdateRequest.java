package edu.pa165.productionservice.dto;

import lombok.Builder;

import java.util.Map;
import java.util.Optional;
@Builder
public record WineRecipeUpdateRequest(
        Optional<String> name,
        Optional<Map<String, Double>> ingredients
) {
}
