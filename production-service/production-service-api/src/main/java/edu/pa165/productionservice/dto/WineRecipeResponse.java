package edu.pa165.productionservice.dto;

import lombok.Builder;

import java.util.Map;

@Builder
public record WineRecipeResponse (
        Long id,
        String name,
        Map<String, Double> ingredients
) {
}
