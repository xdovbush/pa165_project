package edu.pa165.productionservice.dto;

import lombok.Builder;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

@Builder
public record WineBatchUpdateRequest(
        Optional<Long> number,
        Optional<String> wineType,
        Optional<Map<Long, Double>> ingredients,
        Optional<LocalDate> madeTime,
        Optional<LocalDate> openedTime,
        Optional<LocalDate> bottledTime,
        Optional<Double> litersInBatch,
        Optional<Double> sugarLevel,
        Optional<Double> acidityLevel,
        Optional<Double> alcoholLevel,
        Optional<String> dryness
) {
}
