package edu.pa165.productionservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.Map;

@Builder
public record WineRecipeCreateRequest(
        @JsonProperty(required = true) String name,
        @JsonProperty(required = true) Map<String, Double> ingredients
) {
}
