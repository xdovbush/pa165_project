package edu.pa165.productionservice.dto;

import lombok.Builder;

import java.time.LocalDate;
import java.util.Optional;
@Builder
public record WineBatchOpenRequest(
        Optional<LocalDate> openedTime,
        Optional<Double> sugarLevel,
        Optional<Double> acidityLevel,
        Optional<Double> alcoholLevel,
        Optional<String> dryness
) {
}
