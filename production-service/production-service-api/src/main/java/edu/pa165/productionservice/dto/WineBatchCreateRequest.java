package edu.pa165.productionservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

@Builder
public record WineBatchCreateRequest(
        @JsonProperty(required = true) Long number,
        Optional<String> wineType,
        Optional<Map<Long, Double>> ingredients,
        Optional<LocalDate> madeTime,
        Optional<Double> litersInBatch
) {
}
