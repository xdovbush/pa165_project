package edu.pa165.productionservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;

import java.time.LocalDate;
import java.util.Map;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public record WineBatchResponse(
        Long id,
        Long number,
        String wineType,
        Map<Long, Double> ingredients,
        String status,
        LocalDate madeTime,
        LocalDate openedTime,
        LocalDate bottledTime,
        Double litersInBatch,
        Double sugarLevel,
        Double acidityLevel,
        Double alcoholLevel,
        String dryness
) {
}
