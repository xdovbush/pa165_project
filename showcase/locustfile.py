import random

from locust import HttpUser, task, between

# insert your token with both test_1 and test_2 scopes here
TOKEN = ""
API_GATEWAY_PORT = 8080

HARVEST_URL = f"http://localhost:{API_GATEWAY_PORT}/harvest-management-service"
ORDER_URL = f"http://localhost:{API_GATEWAY_PORT}/order-service"
PRODUCTION_URL = f"http://localhost:{API_GATEWAY_PORT}/production-service"
INVENTORY_URL = f"http://localhost:{API_GATEWAY_PORT}/wine-inventory-service"

auth = { "Authorization": f"Bearer {TOKEN}" }

headers = {
    "Authorization": f"Bearer {TOKEN}",
    "accept": "*/*", 
    "Content-Type": "application/json"
}

class WineConnoisseur(HttpUser):
    wait_time = between(1, 1)
    
    @task
    def get_all_available_wines_and_add_review(self):
        """
        This task is modeling a customer that wants to see all
        available wine, get specific wine and add a review on it.
        """
        
        # Customer wants to see all available wine
        wines = self.client.get(url = INVENTORY_URL + "/wines", headers=auth)
        
        # Customer sees wine he/she bought recently
        locations_json = wines.json()
        rand_choose = random.randint(1, len(locations_json)) - 1

        wine_id = locations_json[rand_choose]["id"]

        # Customer wants to get info on that wine specifically
        wine = self.client.get(url = INVENTORY_URL + f"/wines/{wine_id}", headers=auth)

        # Customer wants to add a review on this wine
        json_data_new_review = {
            "rating": 4,
            "comment": "I've seen better wine, but the price on it...",
            "wineId": f"{wine_id}"
        }
        self.client.post(url = INVENTORY_URL + "/reviews", headers=headers, json=json_data_new_review)

        # Customer wants to see all reviews
        self.client.get(url = INVENTORY_URL + "/reviews", headers=auth)

    @task
    def create_new_harvest_and_batch_from_it(self):
        """
        This task is modeling an owner of the wine shop
        that wants to create new harvest and wine batch from it.
        """
        # Owner wants to enter info about new harvest for this year
        json_data_new_harvest = {
            "typeOfGrape": "Chardonnay",
            "year": 2023,
            "harvestDate": "2023-09-15",
            "quantity": random.randint(1000, 2000),
            "location": "Napa Valley",
            "quality": "High Quality",
            "additionalNotes": "Sunny weather during harvest",
            "ripenessLevels": "Perfectly Ripe",
            "skinAndSeedMaturity": "Fully Mature",
            "healthOfGrapes": "Excellent"
        }
        new_harvest = self.client.post(url = HARVEST_URL + "/harvests", headers=headers, json=json_data_new_harvest)

        # Owner can immediately get id of created harvest
        new_harvest_id = new_harvest["id"]

        # Owner wants to see all harvests
        self.client.get(url = HARVEST_URL + "/harvests", headers=headers)

        # Owner wants to create new batch from created harvest
        json_data_new_batch = {
            "number": random.randint(0, 999999),
            "wineType": "Chardonnay Sauvignon",
            "ingredients": {
                f"{new_harvest_id}": 300
            },
            "madeTime": "2023-12-01",
            "litersInBatch": 300.0
        }
        self.client.post(url = PRODUCTION_URL + "/batches", headers=headers, json=json_data_new_batch)

        # Owner wants to see all batches
        self.client.get(url = PRODUCTION_URL + "/batches", headers=headers)

