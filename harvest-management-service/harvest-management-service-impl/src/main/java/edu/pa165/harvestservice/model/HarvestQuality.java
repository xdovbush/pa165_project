package edu.pa165.harvestservice.model;

public enum HarvestQuality {
    LOW_QUALITY("Low Quality", 0.0, 10.0),
    MEDIUM_QUALITY("Medium Quality", 10.1, 20.0),
    HIGH_QUALITY("High Quality", 20.1, 30.0),
    PREMIUM_QUALITY("Premium Quality", 30.1, Double.MAX_VALUE);

    private final String name;
    private final Double minBrixLevel;
    private final Double maxBrixLevel;

    HarvestQuality(String name, Double minBrixLevel, Double maxBrixLevel) {
        this.name = name;
        this.minBrixLevel = minBrixLevel;
        this.maxBrixLevel = maxBrixLevel;
    }

    public String getName() {
        return name;
    }

    public Double getMinBrixLevel() {
        return minBrixLevel;
    }

    public Double getMaxBrixLevel() {
        return maxBrixLevel;
    }

    public static HarvestQuality getCategoryByBrixLevel(Double brixLevel) {
        for (HarvestQuality category : values()) {
            if (brixLevel >= category.minBrixLevel && brixLevel <= category.maxBrixLevel) {
                return category;
            }
        }
        throw new IllegalArgumentException("Invalid Brix Level: " + brixLevel);
    }

    public static HarvestQuality of(String name) {
        for (HarvestQuality harvestQuality : values()) {
            if (harvestQuality.name.equals(name)) return harvestQuality;
        }
        throw new IllegalArgumentException("Such harvest does not exist: " + name);
    }
}
