package edu.pa165.harvestservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static edu.pa165.harvestservice.api.HarvestServiceApi.NAME;

/**
 * Harvest management service runner
 *
 * @author andrii.dovbush
 * @since 2024.03.10
 */
@SpringBootApplication
public class HarvestServiceApplication {

    public static void main(String[] args) {
        setStaticCommonProperties();

        SpringApplication.run(HarvestServiceApplication.class, args);
    }

    private static void setStaticCommonProperties() {
        System.setProperty("spring.application.name", NAME);
    }

}
