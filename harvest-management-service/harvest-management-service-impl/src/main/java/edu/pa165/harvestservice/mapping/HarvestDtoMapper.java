package edu.pa165.harvestservice.mapping;

import edu.pa165.harvestservice.dto.HarvestCreateRequest;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import edu.pa165.harvestservice.model.Harvest;

public interface HarvestDtoMapper {
    HarvestResponse convertToResponse(Harvest harvest);
    Harvest convertFromRequestForCreate(HarvestCreateRequest request);
    Harvest convertFromRequestForUpdate(HarvestUpdateRequest request, Harvest originalHarvest);
}
