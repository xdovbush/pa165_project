package edu.pa165.harvestservice.datamanager;

import edu.pa165.harvestservice.model.Harvest;
import edu.pa165.harvestservice.model.HarvestQuality;
import edu.pa165.harvestservice.model.RipenessLevels;
import edu.pa165.harvestservice.model.SkinAndSeedMaturity;
import edu.pa165.harvestservice.model.TypeOfGrape;
import edu.pa165.harvestservice.repository.HarvestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Component
@Profile({"dev", "docker"})
@Transactional
public class DataManager implements ApplicationRunner {

    private final HarvestRepository harvestRepository;

    @Autowired
    public DataManager(HarvestRepository harvestRepository) {
        this.harvestRepository = harvestRepository;
    }

    public void cleanDb() {
        harvestRepository.deleteAll();
    }

    public void fillDb() {
        if (harvestRepository.count() == 0L)
        {
            harvestRepository.save(new Harvest(
                    null,
                    TypeOfGrape.CABERNET_SAUVIGNON,
                    2023,
                    LocalDate.of(2023, 9, 15),
                    1000.0,
                    "Napa Valley",
                    HarvestQuality.PREMIUM_QUALITY,
                    "Notes about the Chardonnay harvest",
                    RipenessLevels.SLIGHTLY_OVER_RIPE,
                    SkinAndSeedMaturity.FULLY_MATURE,
                    "Healthy"
            ));
            harvestRepository.save(new Harvest(
                    null,
                    TypeOfGrape.MERLOT,
                    2023,
                    LocalDate.of(2023, 10, 10),
                    800.0,
                    "Sonoma County",
                    HarvestQuality.MEDIUM_QUALITY,
                    "Notes about the Merlot harvest",
                    RipenessLevels.PERFECTLY_RIPE,
                    SkinAndSeedMaturity.PARTIALLY_MATURE,
                    "Good condition"
            ));
            harvestRepository.save(new Harvest(
                    null,
                    TypeOfGrape.CHARDONNAY,
                    2023,
                    LocalDate.of(2023, 10, 25),
                    1200.0,
                    "Paso Robles",
                    HarvestQuality.LOW_QUALITY,
                    "Notes about the Cabernet Sauvignon harvest",
                    RipenessLevels.UNDER_RIPE,
                    SkinAndSeedMaturity.IMMATURE,
                    "Some issues with pests"
            ));
            harvestRepository.save(new Harvest(
                    null,
                    TypeOfGrape.PINOT_NOIR,
                    2023,
                    LocalDate.of(2023, 9, 30),
                    600.0,
                    "Czech River Valley",
                    HarvestQuality.HIGH_QUALITY,
                    "Notes about the Pinot Noir harvest",
                    RipenessLevels.OVER_RIPE,
                    SkinAndSeedMaturity.OVER_MATURE,
                    "Healthy"
            ));
        }
    }

    @Autowired
    public void run(ApplicationArguments args) {
        fillDb();
    }
}
