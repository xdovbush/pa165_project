package edu.pa165.harvestservice.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "AllHarvest")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Harvest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_of_grape")
    private TypeOfGrape typeOfGrape;

    @Column(name = "harvest_year")
    private Integer year;

    @Column(name = "harvest_date")
    private LocalDate harvestDate;

    @Column(name = "quantity")
    private Double quantity;

    @Column(name = "location")
    private String location;

    @Enumerated(EnumType.STRING)
    @Column(name = "quality")
    private HarvestQuality quality;

    @Lob
    @Column(name = "additional_notes", columnDefinition = "TEXT")
    private String additionalNotes;

    @Enumerated(EnumType.STRING)
    @Column(name = "ripeness_levels")
    private RipenessLevels ripenessLevels;

    @Enumerated(EnumType.STRING)
    @Column(name = "skin_and_seed_maturity")
    private SkinAndSeedMaturity skinAndSeedMaturity;

    @Column(name = "health_of_grapes")
    private String healthOfGrapes;

}
