package edu.pa165.harvestservice.controller;

import edu.pa165.harvestservice.dto.HarvestCreateRequest;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import edu.pa165.harvestservice.service.HarvestService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static edu.pa165.harvestservice.api.HarvestServiceApi.HARVEST_ENDPOINT;

@RestController
@OpenAPIDefinition(
        info = @Info(title = "Harvest Management Service",
                version = "1.0",
                description = """
                        Service for harvest management. The API has operations for:
                        - Create
                        - Read: all/by ID
                        - Update: by ID
                        - Delete: by ID
                        - Seed dummy DB data
                        - Clean DB data
                        """,
                contact = @Contact(name = "Lukas Havlicek", email = "553637@mail.muni.cz")
        ),
        security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"}),
        servers = @Server(description = "Localhost", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "9081"),
        })
)
@Tag(name = "Harvest Management", description = "Microservice for harvest management")
@RequestMapping(HARVEST_ENDPOINT)
public class HarvestController {

    private final HarvestService service;

    @Autowired
    public HarvestController(HarvestService harvestService) {
        this.service = harvestService;
    }

    @PostMapping
    @Operation(
            summary = "Create new harvest",
            description = """
                    Receives HarvestCreateRequest DTO that will be converted to entity and saved.
                    All fields in DTO should be filled.
                    Returns HarvestResponse DTO that contains saved entity data.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Harvest was created",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = HarvestResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public HarvestResponse createHarvest(@RequestBody HarvestCreateRequest request) {
        return this.service.createHarvest(request);
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Gets harvest info by harvest ID",
            description = """
                    Returns harvest by given as URL param harvest ID.
                    If such harvest does not exist, returns empty response.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Harvest was found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = HarvestResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public HarvestResponse getHarvest(@PathVariable("id") Long id) {
        return this.service.getHarvest(id);
    }

    @GetMapping
    @Operation(
            summary = "Gets all harvests",
            description = """
                    Returns a list of existing harvests.
                    """
    )
    @ResponseStatus(HttpStatus.OK)
    public List<HarvestResponse> getAllHarvests() {
        return this.service.getAllHarvests();
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Updates existing harvest",
            description = """
                    Receives harvest ID as URL parameter and HarvestUpdateRequest DTO as requested data update.
                    All fields in DTO should be filled.
                    Returns HarvestResponse DTO that contains updated entity data.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Harvest was updated",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = HarvestResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public HarvestResponse updateTypeOfGrape(@PathVariable Long id, @RequestBody HarvestUpdateRequest request) {
        return this.service.updateHarvest(id, request);
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Deletes existing harvest by ID",
            description = """
                    Deletes harvest by given as URL param harvest ID.
                    This action is idempotent.
                    """
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Harvest was deleted/does not exist anymore"
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public void deleteHarvest(@PathVariable Long id) {
        this.service.deleteHarvest(id);
    }

}
