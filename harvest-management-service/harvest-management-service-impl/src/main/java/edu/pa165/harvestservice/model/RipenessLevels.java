package edu.pa165.harvestservice.model;

public enum RipenessLevels {
    UNDER_RIPE("Under Ripe", 0.0, 5.0),
    SLIGHTLY_UNDER_RIPE("Slightly Under Ripe", 5.1, 10.0),
    PERFECTLY_RIPE("Perfectly Ripe", 10.1, 15.0),
    SLIGHTLY_OVER_RIPE("Slightly Over Ripe", 15.1, 20.0),
    OVER_RIPE("Over Ripe", 20.1, Double.MAX_VALUE);

    private final String name;
    private final Double minRipenessIndex;
    private final Double maxRipenessIndex;

    RipenessLevels(String name, Double minRipenessIndex, Double maxRipenessIndex) {
        this.name = name;
        this.minRipenessIndex = minRipenessIndex;
        this.maxRipenessIndex = maxRipenessIndex;
    }

    public String getName() {
        return name;
    }

    public Double getMinRipenessIndex() {
        return minRipenessIndex;
    }

    public Double getMaxRipenessIndex() {
        return maxRipenessIndex;
    }

    public static RipenessLevels getCategoryByRipenessIndex(Double ripenessIndex) {
        for (RipenessLevels category : values()) {
            if (ripenessIndex >= category.minRipenessIndex && ripenessIndex <= category.maxRipenessIndex) {
                return category;
            }
        }
        throw new IllegalArgumentException("Invalid Ripeness Index: " + ripenessIndex);
    }

    public static RipenessLevels of(String name) {
        for (RipenessLevels ripenessLevels : values()) {
            if (ripenessLevels.name.equals(name)) return ripenessLevels;
        }
        throw new IllegalArgumentException("Such ripeness level does not exist: " + name);
    }
}
