package edu.pa165.harvestservice.controller;

import edu.pa165.harvestservice.datamanager.DataManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static edu.pa165.harvestservice.api.HarvestServiceApi.DATA_ENDPOINT;

@RestController
@Profile({"dev", "docker"})
@RequestMapping(DATA_ENDPOINT)
public class DataManagerController {

    private final DataManager dataManager;

    @Autowired
    DataManagerController(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @PostMapping("/clean")
    @Operation(
            summary = "Cleans service DB",
            description = """
            Deletes all data on related to this microservice DB.
            """
    )
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Harvest management service DB was cleaned"
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public String cleanDb(){
        dataManager.cleanDb();
        return "DB was cleaned";
    }

    @PostMapping("/seed")
    @Operation(
            summary = "Seed service DB",
            description = """
            Populates with new data on related to this microservice DB.
            Attention: it will populate data only if DB is empty. Otherwise it keeps DB as it is.
            """
    )
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Harvest management service DB was filled with dummy data"
            )
    })
    @ResponseStatus(HttpStatus.OK)
    public String seedDb(){
        dataManager.fillDb();
        return "DB was filled";
    }
}
