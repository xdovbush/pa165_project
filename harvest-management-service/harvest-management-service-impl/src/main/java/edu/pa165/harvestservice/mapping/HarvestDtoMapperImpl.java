package edu.pa165.harvestservice.mapping;

import edu.pa165.harvestservice.dto.HarvestCreateRequest;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import edu.pa165.harvestservice.model.Harvest;
import edu.pa165.harvestservice.model.HarvestQuality;
import edu.pa165.harvestservice.model.RipenessLevels;
import edu.pa165.harvestservice.model.SkinAndSeedMaturity;
import edu.pa165.harvestservice.model.TypeOfGrape;

public class HarvestDtoMapperImpl implements HarvestDtoMapper {
    @Override
    public HarvestResponse convertToResponse(Harvest harvest) {
        return new HarvestResponse(
                harvest.getId(),
                harvest.getTypeOfGrape().getName(),
                harvest.getYear(),
                harvest.getHarvestDate(),
                harvest.getQuantity(),
                harvest.getLocation(),
                harvest.getQuality().getName(),
                harvest.getAdditionalNotes(),
                harvest.getRipenessLevels().getName(),
                harvest.getSkinAndSeedMaturity().getName(),
                harvest.getHealthOfGrapes()
        );
    }

    @Override
    public Harvest convertFromRequestForCreate(HarvestCreateRequest request) {
        Harvest harvest = new Harvest();
        harvest.setTypeOfGrape(TypeOfGrape.of(request.typeOfGrape()));
        harvest.setYear(request.year());
        harvest.setHarvestDate(request.harvestDate());
        harvest.setQuantity(request.quantity());
        harvest.setLocation(request.location());
        harvest.setQuality(HarvestQuality.of(request.quality()));
        harvest.setAdditionalNotes(request.additionalNotes());
        harvest.setRipenessLevels(RipenessLevels.of(request.ripenessLevels()));
        harvest.setSkinAndSeedMaturity(SkinAndSeedMaturity.of(request.skinAndSeedMaturity()));
        harvest.setHealthOfGrapes(request.healthOfGrapes());
        return harvest;
    }

    @Override
    public Harvest convertFromRequestForUpdate(HarvestUpdateRequest request, Harvest originalHarvest) {
        originalHarvest.setTypeOfGrape(TypeOfGrape.of(request.typeOfGrape()));
        originalHarvest.setYear(request.year());
        originalHarvest.setHarvestDate(request.harvestDate());
        originalHarvest.setQuantity(request.quantity());
        originalHarvest.setLocation(request.location());
        originalHarvest.setQuality(HarvestQuality.of(request.quality())); // Convert string to enum
        originalHarvest.setAdditionalNotes(request.additionalNotes());
        originalHarvest.setRipenessLevels(RipenessLevels.of(request.ripenessLevels()));
        originalHarvest.setSkinAndSeedMaturity(SkinAndSeedMaturity.of(request.skinAndSeedMaturity()));
        originalHarvest.setHealthOfGrapes(request.healthOfGrapes());
        return originalHarvest;
    }
}
