package edu.pa165.harvestservice.model;

public enum TypeOfGrape {
    CABERNET_SAUVIGNON("Cabernet Sauvignon"),
    MERLOT("Merlot"),
    PINOT_NOIR("Pinot Noir"),
    SYRAH("Syrah"),
    ZINFANDEL("Zinfandel"),
    CHARDONNAY("Chardonnay"),
    SAUVIGNON_BLANC("Sauvignon Blanc"),
    RIESLING("Riesling"),
    PINOT_GRIGIO("Pinot Grigio"),
    MOSCATO("Moscato");

    private final String name;

    TypeOfGrape(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static TypeOfGrape of(String name) {
        for (TypeOfGrape type : values()) {
            if (type.name.equals(name)) return type;
        }
        throw new IllegalArgumentException("Such grape type does not exist: " + name);
    }
}
