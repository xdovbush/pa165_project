package edu.pa165.harvestservice.service;

import edu.pa165.harvestservice.dto.HarvestCreateRequest;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import edu.pa165.harvestservice.mapping.HarvestDtoMapper;
import edu.pa165.harvestservice.mapping.HarvestDtoMapperImpl;
import edu.pa165.harvestservice.model.Harvest;
import edu.pa165.harvestservice.model.HarvestQuality;
import edu.pa165.harvestservice.model.RipenessLevels;
import edu.pa165.harvestservice.model.SkinAndSeedMaturity;
import edu.pa165.harvestservice.model.TypeOfGrape;
import edu.pa165.harvestservice.monitoring.HarvestMetricsEndpoint;
import edu.pa165.harvestservice.repository.HarvestRepository;
import edu.pa165.harvestservice.service.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class HarvestServiceImpl implements HarvestService{

    HarvestRepository harvestRepository;

    HarvestMetricsEndpoint harvestMetricsEndpoint;

    HarvestDtoMapper mapper;

    @Autowired
    HarvestServiceImpl(HarvestRepository harvestRepository, HarvestMetricsEndpoint harvestMetricsEndpoint){
        this.harvestRepository = harvestRepository;
        this.harvestMetricsEndpoint = harvestMetricsEndpoint;
        this.mapper = new HarvestDtoMapperImpl();
    }

    @Override
    public HarvestResponse createHarvest(HarvestCreateRequest request) {
        Harvest newHarvest = mapper.convertFromRequestForCreate(request);

        Harvest savedHarvest = harvestRepository.save(newHarvest);
        harvestMetricsEndpoint.recordHarvestCreation(savedHarvest.getQuality().getName());
        return mapper.convertToResponse(savedHarvest);
    }

    @Override
    @Transactional(readOnly = true)
    public HarvestResponse getHarvest(Long id) {
        Harvest harvest = harvestRepository.findById(id).orElse(null);
        if (harvest == null) {
            return null;
        }
        return mapper.convertToResponse(harvest);
    }

    @Override
    @Transactional(readOnly = true)
    public List<HarvestResponse> getAllHarvests() {
        List<Harvest> allHarvests = harvestRepository.findAll();
        return allHarvests.stream()
                .map(h -> mapper.convertToResponse(h))
                .toList();
    }

    @Override
    public HarvestResponse updateHarvest(Long id, HarvestUpdateRequest request) {
        Harvest originalHarvest = harvestRepository.findById(id).orElseThrow(() -> new
                ResourceNotFoundException("Harvest not found"));

        Harvest updatedHarvest = mapper.convertFromRequestForUpdate(request, originalHarvest);

        Harvest savedHarvest = harvestRepository.save(updatedHarvest);
        return mapper.convertToResponse(savedHarvest);
    }


    @Override
    public void deleteHarvest(Long id) {
        harvestRepository.deleteById(id);
    }
}
