package edu.pa165.harvestservice.model;

public enum SkinAndSeedMaturity {
    IMMATURE("Immature", "Green, not fully developed"),
    PARTIALLY_MATURE("Partially Mature", "Some coloration, still firm"),
    FULLY_MATURE("Fully Mature", "Deep coloration, soft texture"),
    OVER_MATURE("Over Mature", "Brownish, very soft"),
    DRY_AND_BRITTLE("Dry and Brittle", "Dry, cracking easily");

    private final String name;
    private final String description;

    SkinAndSeedMaturity(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public static SkinAndSeedMaturity of(String name) {
        for (SkinAndSeedMaturity maturity : values()) {
            if (maturity.name.equals(name)) return maturity;
        }
        throw new IllegalArgumentException("Such skin and seed maturity does not exist: " + name);
    }
}
