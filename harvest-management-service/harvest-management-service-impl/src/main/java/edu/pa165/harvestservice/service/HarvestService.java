package edu.pa165.harvestservice.service;

import edu.pa165.harvestservice.dto.HarvestCreateRequest;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface HarvestService {

    HarvestResponse createHarvest(HarvestCreateRequest request);

    HarvestResponse getHarvest(Long id);

    List<HarvestResponse> getAllHarvests();

    HarvestResponse updateHarvest(Long id, HarvestUpdateRequest request);

    void deleteHarvest(Long id);
}
