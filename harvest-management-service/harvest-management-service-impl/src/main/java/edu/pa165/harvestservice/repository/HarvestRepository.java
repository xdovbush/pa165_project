package edu.pa165.harvestservice.repository;

import edu.pa165.harvestservice.model.Harvest;
import edu.pa165.harvestservice.model.HarvestQuality;
import edu.pa165.harvestservice.model.TypeOfGrape;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HarvestRepository extends JpaRepository<Harvest, Long> {

    @Query("SELECT h FROM Harvest h WHERE h.year = :year")
    List<Harvest> findByYear(@Param("year") int year);

    @Query("SELECT h FROM Harvest h WHERE h.typeOfGrape = :typeOfGrape AND h.quality = :quality")
    List<Harvest> findByTypeOfGrapeAndQuality(@Param("typeOfGrape") TypeOfGrape typeOfGrape,
                                              @Param("quality") HarvestQuality quality);
}
