package edu.pa165.harvestservice.monitoring;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Endpoint(id = "harvestMetrics")
public class HarvestMetricsEndpoint {
    private final MeterRegistry meterRegistry;
    private final Map<String, Counter> creationCountersByQuality;

    public HarvestMetricsEndpoint(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
        this.creationCountersByQuality = new HashMap<>();
    }

    public void recordHarvestCreation(String quality) {
        creationCountersByQuality.computeIfAbsent(quality, this::createCounter).increment();
    }

    private Counter createCounter(String quality) {
        return Counter.builder("harvest.creations")
                .tag("quality", quality)
                .description("Number of harvests created by quality")
                .register(meterRegistry);
    }

    @ReadOperation
    public Map<String, Integer> currentHarvestCreationCounts() {
        Map<String, Integer> counts = new HashMap<>();
        creationCountersByQuality.forEach((quality, counter) -> counts.put(quality, (int)counter.count()));
        return counts;
    }
}
