package edu.pa165.harvestservice.mapping;

import edu.pa165.harvestservice.dto.HarvestCreateRequest;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import edu.pa165.harvestservice.model.Harvest;
import edu.pa165.harvestservice.model.HarvestQuality;
import edu.pa165.harvestservice.model.RipenessLevels;
import edu.pa165.harvestservice.model.SkinAndSeedMaturity;
import edu.pa165.harvestservice.model.TypeOfGrape;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class HarvestDtoMapperImplUnitTest {
    private HarvestDtoMapperImpl mapper;

    @BeforeEach
    public void setUp() {
        mapper = new HarvestDtoMapperImpl();
    }

    @Test
    public void testConvertToResponse() {
        Harvest harvest = new Harvest();
        harvest.setId(1L);
        harvest.setTypeOfGrape(TypeOfGrape.MERLOT);
        harvest.setYear(2020);
        harvest.setHarvestDate(LocalDate.of(2020, 9, 15));
        harvest.setQuantity(1500.0);
        harvest.setLocation("Vineyard A");
        harvest.setQuality(HarvestQuality.PREMIUM_QUALITY);
        harvest.setAdditionalNotes("Excellent condition");
        harvest.setRipenessLevels(RipenessLevels.PERFECTLY_RIPE);
        harvest.setSkinAndSeedMaturity(SkinAndSeedMaturity.FULLY_MATURE);
        harvest.setHealthOfGrapes("Healthy");

        HarvestResponse response = mapper.convertToResponse(harvest);

        assertNotNull(response);
        assertEquals(harvest.getId(), response.id());
        assertEquals(harvest.getTypeOfGrape().getName(), response.typeOfGrape());
        assertEquals(harvest.getYear(), response.year());
        assertEquals(harvest.getHarvestDate(), response.harvestDate());
        assertEquals(harvest.getQuantity(), response.quantity());
        assertEquals(harvest.getLocation(), response.location());
        assertEquals(harvest.getQuality().getName(), response.quality());
        assertEquals(harvest.getAdditionalNotes(), response.additionalNotes());
        assertEquals(harvest.getRipenessLevels().getName(), response.ripenessLevels());
        assertEquals(harvest.getSkinAndSeedMaturity().getName(), response.skinAndSeedMaturity());
        assertEquals(harvest.getHealthOfGrapes(), response.healthOfGrapes());
    }

    @Test
    public void testConvertFromRequestForCreate() {
        HarvestCreateRequest request = new HarvestCreateRequest(
                "Merlot",
                2020,
                LocalDate.of(2020, 10, 10),
                1200.0,
                "Vineyard B",
                "High Quality",
                "Some notes",
                "Slightly Under Ripe",
                "Dry and Brittle",
                "Very healthy");

        Harvest harvest = mapper.convertFromRequestForCreate(request);

        assertNotNull(harvest);
        assertEquals(TypeOfGrape.of(request.typeOfGrape()), harvest.getTypeOfGrape());
        assertEquals(request.year(), harvest.getYear());
        assertEquals(request.harvestDate(), harvest.getHarvestDate());
        assertEquals(request.quantity(), harvest.getQuantity());
        assertEquals(request.location(), harvest.getLocation());
        assertEquals(HarvestQuality.of(request.quality()), harvest.getQuality());
        assertEquals(request.additionalNotes(), harvest.getAdditionalNotes());
        assertEquals(RipenessLevels.of(request.ripenessLevels()), harvest.getRipenessLevels());
        assertEquals(SkinAndSeedMaturity.of(request.skinAndSeedMaturity()), harvest.getSkinAndSeedMaturity());
        assertEquals(request.healthOfGrapes(), harvest.getHealthOfGrapes());
    }

    @Test
    public void testConvertFromRequestForUpdate() {
        Harvest originalHarvest = new Harvest();
        HarvestUpdateRequest request = new HarvestUpdateRequest(
                "Chardonnay",
                2021,
                LocalDate.of(2021, 11, 20),
                1300.0,
                "Vineyard C",
                "High Quality",
                "Updated notes",
                "Over Ripe",
                "Partially Mature",
                "Satisfactory");

        Harvest updatedHarvest = mapper.convertFromRequestForUpdate(request, originalHarvest);

        assertNotNull(updatedHarvest);
        assertEquals(TypeOfGrape.of(request.typeOfGrape()), updatedHarvest.getTypeOfGrape());
        assertEquals(request.year(), updatedHarvest.getYear());
        assertEquals(request.harvestDate(), updatedHarvest.getHarvestDate());
        assertEquals(request.quantity(), updatedHarvest.getQuantity());
        assertEquals(request.location(), updatedHarvest.getLocation());
        assertEquals(HarvestQuality.of(request.quality()), updatedHarvest.getQuality());
        assertEquals(request.additionalNotes(), updatedHarvest.getAdditionalNotes());
        assertEquals(RipenessLevels.of(request.ripenessLevels()), updatedHarvest.getRipenessLevels());
        assertEquals(SkinAndSeedMaturity.of(request.skinAndSeedMaturity()), updatedHarvest.getSkinAndSeedMaturity());
        assertEquals(request.healthOfGrapes(), updatedHarvest.getHealthOfGrapes());
    }
}