package edu.pa165.harvestservice.e2e;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pa165.harvestservice.dto.HarvestCreateRequest;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import edu.pa165.harvestservice.model.Harvest;
import edu.pa165.harvestservice.model.HarvestQuality;
import edu.pa165.harvestservice.model.RipenessLevels;
import edu.pa165.harvestservice.model.SkinAndSeedMaturity;
import edu.pa165.harvestservice.model.TypeOfGrape;
import edu.pa165.harvestservice.repository.HarvestRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static edu.pa165.harvestservice.api.HarvestServiceApi.HARVEST_ENDPOINT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class HarvestCrudFullStackTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HarvestRepository harvestRepository;

    @BeforeAll
    void initialize() {
        Harvest newHarvest = new Harvest();
        newHarvest.setTypeOfGrape(TypeOfGrape.CABERNET_SAUVIGNON);
        newHarvest.setYear(2021);
        newHarvest.setHarvestDate(LocalDate.now());
        newHarvest.setQuantity(1500.0);
        newHarvest.setLocation("Napa Valley");
        newHarvest.setQuality(HarvestQuality.HIGH_QUALITY);
        newHarvest.setAdditionalNotes("Excellent conditions");
        newHarvest.setRipenessLevels(RipenessLevels.PERFECTLY_RIPE);
        newHarvest.setSkinAndSeedMaturity(SkinAndSeedMaturity.FULLY_MATURE);
        newHarvest.setHealthOfGrapes("Healthy");
        newHarvest.setId(1L);

        harvestRepository.save(newHarvest);
    }

    @AfterAll
    void cleanUp() {
        harvestRepository.deleteAll();
    }

    @Test
    public void testCreateHarvest() throws Exception {
        HarvestCreateRequest request = new HarvestCreateRequest(
                TypeOfGrape.MERLOT.getName(),
                2022, LocalDate.now(),
                1200.0,
                "Sonoma",
                HarvestQuality.MEDIUM_QUALITY.getName(),
                "Notes on diverse climates",
                RipenessLevels.PERFECTLY_RIPE.getName(),
                SkinAndSeedMaturity.IMMATURE.getName(),
                "Good");

        MockHttpServletResponse response = mockMvc.perform(post(HARVEST_ENDPOINT)
                        .content(convertRequestToJson(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE )
                        .accept(MediaType.APPLICATION_JSON_VALUE ))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse();

        assertThat(response).isNotNull();

        HarvestResponse harvestResponse = convertServletResponseToDto(response, HarvestResponse.class);
        assertThat(harvestResponse).isNotNull();
        assertThat(harvestResponse.typeOfGrape()).isEqualTo(request.typeOfGrape());
        assertThat(harvestResponse.quantity()).isEqualTo(request.quantity());
    }

    @Test
    public void testUpdateHarvest() throws Exception {
        Long harvestId = harvestRepository.findAll().iterator().next().getId();
        HarvestUpdateRequest request = new HarvestUpdateRequest(
                TypeOfGrape.PINOT_NOIR.getName(),
                2023, LocalDate.now(),
                1100.0,
                "Central Valley",
                HarvestQuality.LOW_QUALITY.getName(),
                "Difficult year",
                RipenessLevels.SLIGHTLY_UNDER_RIPE.getName(),
                SkinAndSeedMaturity.IMMATURE.getName(),
                "Poor");

        MockHttpServletResponse response = mockMvc.perform(put(HARVEST_ENDPOINT+"/{id}", harvestId)
                        .content(convertRequestToJson(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE )
                        .accept(MediaType.APPLICATION_JSON_VALUE ))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertThat(response).isNotNull();

        HarvestResponse harvestResponse = convertServletResponseToDto(response, HarvestResponse.class);
        assertThat(harvestResponse).isNotNull();
        assertThat(harvestResponse.typeOfGrape()).isEqualTo(request.typeOfGrape());
        assertThat(harvestResponse.quantity()).isEqualTo(request.quantity());
    }

    private String convertRequestToJson(Object request) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsString(request);
    }

    private <T> T convertServletResponseToDto(MockHttpServletResponse response, Class<T> responseType) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.readValue(response.getContentAsString(), responseType);
    }
}
