package edu.pa165.harvestservice.controller;

import edu.pa165.harvestservice.dto.HarvestCreateRequest;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import edu.pa165.harvestservice.service.HarvestService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HarvestControllerTest {
    @Mock
    private HarvestService harvestService;
    private HarvestController harvestController;

    AutoCloseable openMocks;

    @BeforeEach
    void setup() {
        openMocks = MockitoAnnotations.openMocks(this);
        harvestController = new HarvestController(harvestService);
    }

    @AfterEach
    public void tearDown() throws Exception {
        openMocks.close();
    }

    @Test
    void getHarvestTest() {
        Long harvestId = 1L;

        HarvestResponse mockedHarvestResponse = new HarvestResponse(
                harvestId,
                "Merlot",
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                "High Quality",
                "AOC (certification of authenticity)",
                "ripe",
                "ripe",
                "healthy");

        when(harvestService.getHarvest(harvestId)).thenReturn(mockedHarvestResponse);
        HarvestResponse harvestResponse = harvestController.getHarvest(harvestId);

        Mockito.verify(harvestService).getHarvest(harvestId);
        assertEquals(mockedHarvestResponse, harvestResponse);
    }

    @Test
    void getAllHarvestsTest() {
        Long harvestId = 1L;

        List<HarvestResponse> mockedHarvestResponses = List.of(new HarvestResponse(
                harvestId,
                "Merlot",
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                "High Quality",
                "AOC (certification of authenticity)",
                "ripe",
                "ripe",
                "healthy"));

        when(harvestService.getAllHarvests()).thenReturn(mockedHarvestResponses);

        List<HarvestResponse> harvestResponses = harvestController.getAllHarvests();

        Mockito.verify(harvestService).getAllHarvests();
        assertEquals(mockedHarvestResponses, harvestResponses);
    }

    @Test
    void createHarvestTest() {
        HarvestCreateRequest harvestCreateRequest = new HarvestCreateRequest(
                "Merlot",
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                "High Quality",
                "AOC (certification of authenticity)",
                "ripe",
                "ripe",
                "healthy");

        HarvestResponse mockedHarvestResponse = new HarvestResponse(
                1L,
                "Merlot",
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                "High Quality",
                "AOC (certification of authenticity)",
                "ripe",
                "ripe",
                "healthy");

        when(harvestService.createHarvest(harvestCreateRequest)).thenReturn(mockedHarvestResponse);

        HarvestResponse harvestResponse = harvestController.createHarvest(harvestCreateRequest);

        Mockito.verify(harvestService).createHarvest(harvestCreateRequest);

        assertEquals(mockedHarvestResponse, harvestResponse);
    }

    @Test
    void updateHarvestTest() {
        Long harvestId = 1L;

        HarvestUpdateRequest harvestUpdateRequest = new HarvestUpdateRequest(
                "Merlot",
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                "High Quality",
                "AOC (certification of authenticity)",
                "ripe",
                "ripe",
                "healthy");

        HarvestResponse mockedHarvestResponse = new HarvestResponse(
                harvestId,
                "Merlot",
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                "High Quality",
                "AOC (certification of authenticity)",
                "ripe",
                "ripe",
                "healthy");

        when(harvestService.updateHarvest(harvestId, harvestUpdateRequest)).thenReturn(mockedHarvestResponse);

        HarvestResponse harvestResponse = harvestController.updateTypeOfGrape(harvestId, harvestUpdateRequest);

        Mockito.verify(harvestService).updateHarvest(harvestId, harvestUpdateRequest);
        assertEquals(mockedHarvestResponse, harvestResponse);
    }

    @Test
    void deleteHarvestTest() {
        Long harvestId = 1L;
        harvestController.deleteHarvest(harvestId);
        verify(harvestService).deleteHarvest(harvestId);
    }


}
