package edu.pa165.harvestservice.repository;

import edu.pa165.harvestservice.model.Harvest;
import edu.pa165.harvestservice.model.HarvestQuality;
import edu.pa165.harvestservice.model.TypeOfGrape;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;


import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("test")
public class HarvestRepositoryTest {

    @Autowired
    private HarvestRepository harvestRepository;

    @BeforeEach
    void setUp() {
        Harvest harvest1 = new Harvest();
        harvest1.setTypeOfGrape(TypeOfGrape.MERLOT);
        harvest1.setYear(2020);
        harvest1.setHarvestDate(LocalDate.of(2020, 9, 15));
        harvest1.setQuality(HarvestQuality.PREMIUM_QUALITY);
        harvestRepository.save(harvest1);

        Harvest harvest2 = new Harvest();
        harvest2.setTypeOfGrape(TypeOfGrape.MERLOT);
        harvest2.setYear(2020);
        harvest2.setHarvestDate(LocalDate.of(2020, 10, 10));
        harvest2.setQuality(HarvestQuality.HIGH_QUALITY);
        harvestRepository.save(harvest2);

        harvestRepository.flush();
    }

    @AfterEach
    void tearDown() {
        harvestRepository.deleteAll();
    }

    @Test
    void findByYear() {
        List<Harvest> results = harvestRepository.findByYear(2020);
        assertThat(results).hasSize(2);
        assertThat(results.get(0).getTypeOfGrape()).isEqualTo(TypeOfGrape.MERLOT);
    }

    @Test
    void findByTypeOfGrapeAndQuality() {
        List<Harvest> results = harvestRepository.findByTypeOfGrapeAndQuality(TypeOfGrape.MERLOT, HarvestQuality.PREMIUM_QUALITY);
        assertThat(results).isNotEmpty();
        assertThat(results.get(0).getYear()).isEqualTo(2020);
    }
}