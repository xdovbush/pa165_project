package edu.pa165.harvestservice.service;

import edu.pa165.harvestservice.dto.HarvestCreateRequest;
import edu.pa165.harvestservice.dto.HarvestResponse;
import edu.pa165.harvestservice.dto.HarvestUpdateRequest;
import edu.pa165.harvestservice.model.Harvest;
import edu.pa165.harvestservice.model.HarvestQuality;
import edu.pa165.harvestservice.model.RipenessLevels;
import edu.pa165.harvestservice.model.SkinAndSeedMaturity;
import edu.pa165.harvestservice.model.TypeOfGrape;
import edu.pa165.harvestservice.monitoring.HarvestMetricsEndpoint;
import edu.pa165.harvestservice.repository.HarvestRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HarvestServiceImplTest {
    @Mock
    private HarvestRepository harvestRepository;

    @Mock
    private HarvestMetricsEndpoint harvestMetricsEndpoint;

    @InjectMocks
    private HarvestServiceImpl harvestService;

    AutoCloseable openMocks;

    @BeforeEach
    void setUp() {openMocks = MockitoAnnotations.openMocks(this);}

    @AfterEach
    public void tearDown() throws Exception {
        openMocks.close();
    }

    @Test
    void createHarvestTest() {
        HarvestCreateRequest request = HarvestCreateRequest.builder()
                .typeOfGrape("Merlot")
                .year(2015)
                .harvestDate(LocalDate.of(2015, 10, 10))
                .quantity(100.0)
                .location("Petrus")
                .quality("High Quality")
                .additionalNotes("AOC (certification of authenticity)")
                .ripenessLevels("Perfectly Ripe")
                .skinAndSeedMaturity("Dry and Brittle")
                .healthOfGrapes("healthy")
                .build();

        Harvest harvest = new Harvest(
                1L,
                TypeOfGrape.MERLOT,
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                HarvestQuality.HIGH_QUALITY,
                "AOC (certification of authenticity)",
                RipenessLevels.PERFECTLY_RIPE,
                SkinAndSeedMaturity.DRY_AND_BRITTLE,
                "healthy"
        );

        when(harvestRepository.save(any(Harvest.class))).thenReturn(harvest);

        HarvestResponse harvestResponse = harvestService.createHarvest(request);

        assertNotNull(harvestResponse);
        assertEquals("High Quality", harvestResponse.quality());
        assertEquals(1L, harvestResponse.id());
        assertEquals("AOC (certification of authenticity)", harvestResponse.additionalNotes());

        verify(harvestMetricsEndpoint).recordHarvestCreation(harvest.getQuality().getName());
    }

    @Test
    void getHarvestTest() {
        Long harvestId = 1L;

        Harvest harvest = new Harvest(
                harvestId,
                TypeOfGrape.MERLOT,
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                HarvestQuality.HIGH_QUALITY,
                "AOC (certification of authenticity)",
                RipenessLevels.SLIGHTLY_OVER_RIPE,
                SkinAndSeedMaturity.OVER_MATURE,
                "healthy"
        );

        when(harvestRepository.findById(harvestId)).thenReturn(java.util.Optional.of(harvest));

        HarvestResponse harvestResponse = harvestService.getHarvest(harvestId);

        assertNotNull(harvestResponse);
        assertEquals("High Quality", harvestResponse.quality());
        assertEquals(1L, harvestResponse.id());
        assertEquals("AOC (certification of authenticity)", harvestResponse.additionalNotes());
    }

    @Test
    void getAllHarvestsTest() {
        Harvest harvest = new Harvest(
                1L,
                TypeOfGrape.SYRAH,
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                HarvestQuality.HIGH_QUALITY,
                "AOC (certification of authenticity)",
                RipenessLevels.UNDER_RIPE,
                SkinAndSeedMaturity.OVER_MATURE,
                "healthy"
        );

        when(harvestRepository.findAll()).thenReturn(java.util.List.of(harvest));

        HarvestResponse harvestResponse = harvestService.getAllHarvests().get(0);

        assertNotNull(harvestResponse);
        assertEquals("High Quality", harvestResponse.quality());
        assertEquals(1L, harvestResponse.id());
        assertEquals("AOC (certification of authenticity)", harvestResponse.additionalNotes());
    }

    @Test
    void updateHarvestTest() {
        Long harvestId = 1L;

        HarvestUpdateRequest request = HarvestUpdateRequest.builder()
                .typeOfGrape("Moscato")
                .year(2015)
                .harvestDate(LocalDate.of(2015, 10, 10))
                .quantity(100.0)
                .location("Petrus")
                .quality("High Quality")
                .additionalNotes("AOC (certification of authenticity)")
                .ripenessLevels("Perfectly Ripe")
                .skinAndSeedMaturity("Over Mature")
                .healthOfGrapes("healthy")
                .build();

        Harvest harvest = new Harvest(
                harvestId,
                TypeOfGrape.MOSCATO,
                2015,
                LocalDate.of(2015, 10, 10),
                100.0,
                "Petrus",
                HarvestQuality.HIGH_QUALITY,
                "AOC (certification of authenticity)",
                RipenessLevels.PERFECTLY_RIPE,
                SkinAndSeedMaturity.OVER_MATURE,
                "healthy"
        );

        when(harvestRepository.findById(harvestId)).thenReturn(java.util.Optional.of(harvest));
        when(harvestRepository.save(any(Harvest.class))).thenReturn(harvest);

        HarvestResponse harvestResponse = harvestService.updateHarvest(harvestId, request);

        assertNotNull(harvestResponse);
        assertEquals("High Quality", harvestResponse.quality());
        assertEquals(1L, harvestResponse.id());
        assertEquals("AOC (certification of authenticity)", harvestResponse.additionalNotes());
    }

    @Test
    void deleteHarvestTest() {
        Long harvestId = 1L;

        harvestService.deleteHarvest(harvestId);
        verify(harvestRepository).deleteById(harvestId);
    }
}
