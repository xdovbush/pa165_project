package edu.pa165.harvestservice.dto;

import lombok.Builder;

import java.time.LocalDate;

@Builder
public record HarvestCreateRequest(
        String typeOfGrape,
        Integer year,
        LocalDate harvestDate,
        Double quantity,
        String location,
        String quality,
        String additionalNotes,
        String ripenessLevels,
        String skinAndSeedMaturity,
        String healthOfGrapes
) {
}