package edu.pa165.orderservice.datamanager;

import edu.pa165.orderservice.enums.OrderStatus;
import edu.pa165.orderservice.model.OrderEntity;
import edu.pa165.orderservice.model.OrderItemLine;
import edu.pa165.orderservice.repository.OrderEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Component
@Profile({"dev", "docker"})
@Transactional
public class DataManager implements ApplicationRunner {

    private final OrderEntityRepository orderEntityRepository;

    @Autowired
    public DataManager(OrderEntityRepository orderEntityRepository) {
        this.orderEntityRepository = orderEntityRepository;
    }

    public void cleanDb() {
        orderEntityRepository.deleteAll();
    }

    public void fillDb() {
        if (orderEntityRepository.count() == 0L)
        {
            // Order 1
            OrderItemLine order1Item1 = OrderItemLine.builder()
                    .wineId(1L)
                    .quantity(2)
                    .unitPrice(50)
                    .description("Chardonnay 2023")
                    .build();

            OrderItemLine order1Item2 = OrderItemLine.builder()
                    .wineId(2L)
                    .quantity(1)
                    .unitPrice(40)
                    .description("Merlot 2023")
                    .build();

            List<OrderItemLine> order1Items = new ArrayList<>();
            order1Items.add(order1Item1);
            order1Items.add(order1Item2);

            OrderEntity order1 = OrderEntity.builder()
                    .customerName("John Doe")
                    .customerPhone("123-456-7890")
                    .customerEmail("john.doe@example.com")
                    .customerAddress("123 Main St")
                    .billingAddress("123 Main St")
                    .shippingAddress("456 Elm St")
                    .total(140)
                    .status(OrderStatus.CLOSED)
                    .orderItemLines(order1Items)
                    .build();

            // Order 2
            OrderItemLine order2Item1 = OrderItemLine.builder()
                    .wineId(3L)
                    .quantity(3)
                    .unitPrice(60)
                    .description("Cabernet Sauvignon 2023")
                    .build();

            List<OrderItemLine> order2Items = new ArrayList<>();
            order2Items.add(order2Item1);

            OrderEntity order2 = OrderEntity.builder()
                    .customerName("Jane Smith")
                    .customerPhone("987-654-3210")
                    .customerEmail("jane.smith@example.com")
                    .customerAddress("456 Elm St")
                    .billingAddress("456 Elm St")
                    .shippingAddress("789 Oak St")
                    .total(180)
                    .status(OrderStatus.OPENED)
                    .orderItemLines(order2Items)
                    .build();

            orderEntityRepository.save(order1);
            orderEntityRepository.save(order2);
        }
    }

    @Autowired
    public void run(ApplicationArguments args) {
        fillDb();
    }
}
