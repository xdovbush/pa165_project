package edu.pa165.orderservice.controller;

import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.enums.OrderStatus;
import edu.pa165.orderservice.service.OrderEntityService;
import edu.pa165.orderservice.service.OrderEntityStatusService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static edu.pa165.orderservice.api.OrderServiceApi.ORDER_STATUS_ENDPOINT;

@RestController
@RequestMapping(ORDER_STATUS_ENDPOINT)
public class OrderEntityStatusController {

    private final OrderEntityService service;
    private final OrderEntityStatusService statusService;

    @Autowired
    OrderEntityStatusController(OrderEntityService service, OrderEntityStatusService statusService) {
        this.service = service;
        this.statusService = statusService;
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Updates status of the existing order",
            description = """
                    By given order ID, as URL parameter, and order status name,
                    updates existing order to new status.
                    Returns DTO with updated entity info.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Order was updated",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = OrderEntityResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public OrderEntityResponse updateOrderEntity(@PathVariable("id") Long id, @RequestBody String orderStatus) {
        OrderStatus status = OrderStatus.of(orderStatus);
        OrderEntityResponse orderEntityResponse = this.service.changeStatusOrderEntity(id, status);

        statusService.updateWineStockIfNeeded(status, orderEntityResponse);

        return orderEntityResponse;
    }


}
