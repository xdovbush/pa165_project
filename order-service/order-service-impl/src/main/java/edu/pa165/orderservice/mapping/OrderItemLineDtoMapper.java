package edu.pa165.orderservice.mapping;

import edu.pa165.orderservice.dto.OrderItemLineDto;
import edu.pa165.orderservice.model.OrderItemLine;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderItemLineDtoMapper {
    public OrderItemLineDto convertToDto(OrderItemLine orderItemLine) {
        return new OrderItemLineDto(orderItemLine.getWineId(), orderItemLine.getQuantity(), orderItemLine.getUnitPrice(), orderItemLine.getDescription());
    }
    public List<OrderItemLineDto> convertToDto(List<OrderItemLine> orderItemEntities) {
        return orderItemEntities.stream().map(this::convertToDto).toList();
    }
    public OrderItemLine convertToEntity(OrderItemLineDto orderItemLineDto) {
        return OrderItemLine.builder()
                .wineId(orderItemLineDto.wineId())
                .quantity(orderItemLineDto.quantity())
                .unitPrice(orderItemLineDto.price())
                .description(orderItemLineDto.description())
                .build();
    }

    public List<OrderItemLine> convertToEntity(List<OrderItemLineDto> orderItemLineDtos) {
        return orderItemLineDtos.stream().map(this::convertToEntity).toList();
    }
}
