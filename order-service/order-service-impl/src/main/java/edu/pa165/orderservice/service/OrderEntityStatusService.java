package edu.pa165.orderservice.service;

import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.enums.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderEntityStatusService {

    private final WineStockService wineStockService;

    @Autowired
    public OrderEntityStatusService(WineStockService wineStockService) {
        this.wineStockService = wineStockService;
    }


    public void updateWineStockIfNeeded(OrderStatus newOrderStatus, OrderEntityResponse orderEntityResponse) {
        if (!newOrderStatus.name().equals(orderEntityResponse.status())) {
            if (orderEntityResponse.status().equals(OrderStatus.OPENED.name()) && newOrderStatus == OrderStatus.CANCELED) {
                // change wine stock numbers by calling API of wine inventory service
                orderEntityResponse.orderItemLineDto().forEach(orderItemLine -> wineStockService.updateWineStock(orderItemLine.wineId(), -orderItemLine.quantity()));
            }
        }
    }
}
