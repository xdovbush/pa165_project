package edu.pa165.orderservice.service;

import edu.pa165.orderservice.dto.OrderEntityRequest;
import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.dto.OrderEntityUpdateRequest;
import edu.pa165.orderservice.dto.OrderEntityUpdateResult;
import edu.pa165.orderservice.enums.OrderStatus;
import edu.pa165.orderservice.mapping.OrderEntityDtoMapper;
import edu.pa165.orderservice.mapping.OrderItemLineDtoMapper;
import edu.pa165.orderservice.model.OrderEntity;
import edu.pa165.orderservice.model.OrderItemLine;
import edu.pa165.orderservice.monitoring.OrderMetricsEndpoint;
import edu.pa165.orderservice.repository.OrderEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OrderEntityServiceImpl implements OrderEntityService {
    private final OrderEntityRepository repository;
    private final OrderItemLineDtoMapper orderItemLineMapper;
    private final OrderEntityDtoMapper orderEntityMapper;
    private final OrderMetricsEndpoint orderMetricsEndpoint;

    @Autowired
    OrderEntityServiceImpl(OrderEntityRepository repository, OrderItemLineDtoMapper orderItemLineMapper, OrderEntityDtoMapper orderEntityMapper, OrderMetricsEndpoint orderMetricsEndpoint) {
        this.repository = repository;
        this.orderItemLineMapper = orderItemLineMapper;
        this.orderEntityMapper = orderEntityMapper;
        this.orderMetricsEndpoint = orderMetricsEndpoint;
    }

    @Override
    public OrderEntityResponse createOrderEntity(OrderEntityRequest request) {
        OrderEntity newOrderEntity = new OrderEntity();

        newOrderEntity.setCustomerName(request.name());
        newOrderEntity.setCustomerEmail(request.email());
        newOrderEntity.setCustomerPhone(request.phone());
        newOrderEntity.setCustomerAddress(request.customerAddress());
        newOrderEntity.setBillingAddress(request.billingAddress());
        newOrderEntity.setShippingAddress(request.shippingAddress());
        newOrderEntity.setTotal(request.total());
        newOrderEntity.setStatus(OrderStatus.OPENED);
        newOrderEntity.setOrderItemLines(orderItemLineMapper.convertToEntity(request.orderItemLineDto()));

        newOrderEntity = repository.save(newOrderEntity);

        orderMetricsEndpoint.incrementOrderCount();

        return orderEntityMapper.convertToEntityResponse(newOrderEntity);
    }

    @Override
    public void deleteOrderEntity(Long id) {
        repository.deleteById(id);
    }

    @Override
    public OrderEntityUpdateResult updateOrderEntity(Long id, OrderEntityUpdateRequest request) {
        OrderEntity orderEntity = repository.findById(id).orElse(null);
        if (orderEntity == null) {
            return null;
        }

        orderEntity.setCustomerName(request.name());
        orderEntity.setCustomerEmail(request.email());
        orderEntity.setCustomerPhone(request.phone());
        orderEntity.setCustomerAddress(request.customerAddress());
        orderEntity.setBillingAddress(request.billingAddress());
        orderEntity.setShippingAddress(request.shippingAddress());
        orderEntity.setTotal(request.total());

        Map<Long, Integer> wineIdQuantityOriginalMap = new HashMap<>();
        Map<Long, Integer> wineIdQuantityDifferenceMap = new HashMap<>();

        // Clear the existing OrderItemLine entities
        if(orderEntity.getOrderItemLines() != null) {
            for (OrderItemLine orderItemLine : orderEntity.getOrderItemLines()) {
                // add the quantity to the map, sum if existing
                wineIdQuantityOriginalMap.put(orderItemLine.getWineId(), wineIdQuantityOriginalMap.getOrDefault(orderItemLine.getWineId(), 0) + orderItemLine.getQuantity());
            }

            orderEntity.getOrderItemLines().clear();
        }
        else {
            orderEntity.setOrderItemLines(List.of());
        }

        // Convert the OrderItemLineDto objects to OrderItemLine entities and add them to the OrderEntity
        List<OrderItemLine> newOrderItemLines = orderItemLineMapper.convertToEntity(request.orderItemLineDto());
        if(!newOrderItemLines.isEmpty()) {
            orderEntity.getOrderItemLines().addAll(newOrderItemLines);

            for (OrderItemLine orderItemLine : orderEntity.getOrderItemLines()) {
                // add the difference to the map
                Long wineId = orderItemLine.getWineId();
                Integer originalWineQty = wineIdQuantityOriginalMap.getOrDefault(wineId, 0);
                wineIdQuantityDifferenceMap.put(wineId, wineIdQuantityDifferenceMap.getOrDefault(wineId, -originalWineQty) + orderItemLine.getQuantity());
            }
        }

        orderEntity = repository.save(orderEntity);

        return OrderEntityUpdateResult.builder().orderEntityResponse(orderEntityMapper.convertToEntityResponse(orderEntity)).wineIdQuantityDifferenceMap(wineIdQuantityDifferenceMap).build();

//        return orderEntityMapper.convertToEntityResponse(orderEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public OrderEntityResponse getOrderEntity(Long id) {
        OrderEntity orderEntity = repository.findById(id).orElse(null);
        if (orderEntity == null) {
            return null;
        }
        return orderEntityMapper.convertToEntityResponse(orderEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderEntityResponse> getAllOrderEntities() {
        List<OrderEntity> allTestEntities = repository.findAll();
        return allTestEntities.stream()
                .map(orderEntityMapper::convertToEntityResponse
                )
                .toList();
    }

    @Override
    public OrderEntityResponse changeStatusOrderEntity(Long id, OrderStatus orderStatus) {
        OrderEntity orderEntity = repository.findById(id).orElse(null);
        if (orderEntity == null) {
            return null;
        }

        orderEntity.setStatus(orderStatus);
        repository.save(orderEntity);

        return orderEntityMapper.convertToEntityResponse(orderEntity);

    }

    @Override
    public List<OrderEntityResponse> getOrderEntitiesShippedTo(String address) {
        List<OrderEntity> orderEntities = repository.getOrdersShippedTo(address);
        if (orderEntities.isEmpty()) {
            return null;
        }

        List<OrderEntityResponse> orderEntityResponses = new ArrayList<>();
        for (OrderEntity orderEntity : orderEntities) {
            orderEntityResponses.add(orderEntityMapper.convertToEntityResponse(orderEntity));
        }

        return orderEntityResponses;
    }

    @Override
    public List<OrderEntityResponse> getOrderEntitiesWithWine(Long wineId) {
        List<OrderEntity> orderEntities = repository.getOrdersWithWine(wineId);
        if (orderEntities.isEmpty()) {
            return null;
        }

        List<OrderEntityResponse> orderEntityResponses = new ArrayList<>();
        for (OrderEntity orderEntity : orderEntities) {
            orderEntityResponses.add(orderEntityMapper.convertToEntityResponse(orderEntity));
        }

        return orderEntityResponses;
    }

}
