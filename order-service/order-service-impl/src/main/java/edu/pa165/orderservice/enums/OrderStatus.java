package edu.pa165.orderservice.enums;

public enum OrderStatus {
    OPENED("Opened"),
    PAID("Paid"),
    FULFILLED("Fulfilled"),
    CLOSED("Closed"),
    CANCELED("Canceled");

    private final String name;

    OrderStatus(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public static OrderStatus of(String name) {
        for (OrderStatus orderStatus : values()) {
            if (orderStatus.name.equals(name)) return orderStatus;
        }
        throw new IllegalArgumentException("Such order status does not exist: " + name);
    }
}
