package edu.pa165.orderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static edu.pa165.orderservice.api.OrderServiceApi.NAME;

/**
 * Order service runner
 *
 * @author andrii.dovbush
 * @since 2024.03.10
 */
@SpringBootApplication
public class OrderServiceApplication {

    public static void main(String[] args) {
        setStaticCommonProperties();

        SpringApplication.run(OrderServiceApplication.class, args);
    }

    private static void setStaticCommonProperties() {
        System.setProperty("spring.application.name", NAME);
    }

}
