package edu.pa165.orderservice.service;

import edu.pa165.orderservice.dto.OrderEntityRequest;
import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.dto.OrderEntityUpdateRequest;
import edu.pa165.orderservice.dto.OrderEntityUpdateResult;
import edu.pa165.orderservice.enums.OrderStatus;
import jakarta.annotation.Nullable;

import java.util.List;

public interface OrderEntityService {
    OrderEntityResponse createOrderEntity(OrderEntityRequest request);
    void deleteOrderEntity(Long id);
    OrderEntityUpdateResult updateOrderEntity(Long id, OrderEntityUpdateRequest request);

    @Nullable
    OrderEntityResponse getOrderEntity(Long id);

    List<OrderEntityResponse> getAllOrderEntities();

    OrderEntityResponse changeStatusOrderEntity(Long id, OrderStatus orderStatus);

    List<OrderEntityResponse> getOrderEntitiesShippedTo(String address);

    List<OrderEntityResponse> getOrderEntitiesWithWine(Long wineId);
}
