package edu.pa165.orderservice.facade;

import edu.pa165.orderservice.dto.*;
import edu.pa165.orderservice.service.OrderEntityService;
import edu.pa165.orderservice.service.WineStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class OrderEntityFacade {
    private final OrderEntityService orderEntityService;
    private final WineStockService wineStockService;

    @Autowired
    OrderEntityFacade(OrderEntityService orderEntityService, WineStockService wineStockService) {
        this.orderEntityService = orderEntityService;
        this.wineStockService = wineStockService;
    }

    public OrderEntityResponse createOrderEntity(OrderEntityRequest request) {
        OrderEntityResponse response = this.orderEntityService.createOrderEntity(request);

        Map<Long, Integer> wineIdQuantityMap = request.orderItemLineDto().stream()
                .collect(Collectors.toMap(OrderItemLineDto::wineId, OrderItemLineDto::quantity));

        wineStockService.updateWineStock(wineIdQuantityMap);

        return response;
    }

    public OrderEntityResponse updateOrderEntity(Long id, OrderEntityUpdateRequest request) {
        OrderEntityUpdateResult response = this.orderEntityService.updateOrderEntity(id, request);

        wineStockService.updateWineStock(response.wineIdQuantityDifferenceMap());

        return response.orderEntityResponse();
    }

    public void deleteOrderEntity(Long id) {
        OrderEntityResponse response = Objects.requireNonNull(this.orderEntityService.getOrderEntity(id));

        Map<Long, Integer> wineIdQuantityMap = new HashMap<>();

        response.orderItemLineDto().forEach(orderItemLine -> wineIdQuantityMap.put(orderItemLine.wineId(), -orderItemLine.quantity()));

        wineStockService.updateWineStock(wineIdQuantityMap);

        this.orderEntityService.deleteOrderEntity(id);
    }

    public OrderEntityResponse getOrderEntity(Long id) {
        return this.orderEntityService.getOrderEntity(id);
    }

    public List<OrderEntityResponse> getAllOrderEntities() {
        return this.orderEntityService.getAllOrderEntities();
    }

    public List<OrderEntityResponse> getOrderEntitiesShippedTo(String address) {
        return this.orderEntityService.getOrderEntitiesShippedTo(address);
    }

    public List<OrderEntityResponse> getOrderEntitiesWithWine(Long wineId) {
        return this.orderEntityService.getOrderEntitiesWithWine(wineId);
    }
}
