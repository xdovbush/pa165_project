package edu.pa165.orderservice.controller;

import edu.pa165.orderservice.dto.OrderEntityRequest;
import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.dto.OrderEntityUpdateRequest;
import edu.pa165.orderservice.facade.OrderEntityFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static edu.pa165.orderservice.api.OrderServiceApi.ORDER_ENDPOINT;

@RestController
@OpenAPIDefinition(
        info = @Info(title = "Order Service",
                version = "1.0",
                description = """
                        Service for order management. The API has operations for:
                        - Create
                        - Read: all/by ID
                        - Update: by ID
                        - Update order status: by ID
                        - Delete: by ID
                        - Seed dummy DB data
                        - Clean DB data
                        """,
                contact = @Contact(name = "Ivann Vyslanko", email = "556729@mail.muni.cz")
        ),
        servers = @Server(description = "Localhost", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "9082"),
        })
)
@Tag(name = "Order management", description = "Microservice for order management")
@RequestMapping(ORDER_ENDPOINT)
public class OrderEntityController {

    private final OrderEntityFacade facade;


    @Autowired
    OrderEntityController(OrderEntityFacade facade) {
        this.facade = facade;
    }

    @PostMapping
    @Operation(
            summary = "Create new order",
            description = """
                    Receives OrderEntityRequest DTO that will be converted to entity and saved.
                    All fields in DTO should be filled.
                    Returns OrderEntityResponse DTO that contains saved entity data.
                    """,
            security = @SecurityRequirement(name = "customer", scopes = {"SCOPE_test_2"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Order was created",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = OrderEntityResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    public OrderEntityResponse createOrderEntity(@RequestBody OrderEntityRequest request) {
        return facade.createOrderEntity(request);
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Updates existing order",
            description = """
                    Receives order ID as URL parameter and OrderEntityUpdateRequest DTO as requested data update.
                    All fields in DTO should be filled.
                    Returns OrderEntityResponse DTO that contains updated entity data.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Order was updated",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = OrderEntityResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public OrderEntityResponse updateOrderEntity(@PathVariable("id") Long id, @RequestBody OrderEntityUpdateRequest request) {
        return facade.updateOrderEntity(id, request);
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Deletes existing order by ID",
            description = """
                    Deletes order by given as URL param order ID.
                    This action is idempotent.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Order was deleted/does not exist anymore"
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public void deleteOrderEntity(@PathVariable("id") Long id) {
        facade.deleteOrderEntity(id);
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Gets order info by order ID",
            description = """
                    Returns oder by given as URL param harvest ID.
                    If such order does not exist, returns empty response.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Order was found",
                            content = {
                                    @Content(mediaType = "application/json",
                                            schema = @Schema(implementation = OrderEntityResponse.class))
                            }
                    )
            }
    )
    @ResponseStatus(HttpStatus.OK)
    public OrderEntityResponse getOrderEntity(@PathVariable("id") Long id) {
        return facade.getOrderEntity(id);
    }

    @GetMapping("/shipped-to/{address}")
    @Operation(
            summary = "Gets orders with the specified address",
            description = """
                    Returns a list of existing orders.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ResponseStatus(HttpStatus.OK)
    public List<OrderEntityResponse> getOrderEntitiesShippedTo(@PathVariable("address") String address) {
        return facade.getOrderEntitiesShippedTo(address);
    }

    @GetMapping("/wine/{wineId}")
    @Operation(
            summary = "Gets orders with the specified wine",
            description = """
                    Returns a list of existing orders.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ResponseStatus(HttpStatus.OK)
    public List<OrderEntityResponse> getOrderEntitiesWithWine(@PathVariable("wineId") Long wineId) {
        return facade.getOrderEntitiesWithWine(wineId);
    }

    @GetMapping
    @Operation(
            summary = "Gets all orders",
            description = """
                    Returns a list of existing orders.
                    """,
            security = @SecurityRequirement(name = "owner", scopes = {"SCOPE_test_1"})
    )
    @ResponseStatus(HttpStatus.OK)
    public List<OrderEntityResponse> getAllOrderEntities() {
        return facade.getAllOrderEntities();
    }
}
