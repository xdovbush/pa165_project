package edu.pa165.orderservice.mapping;

import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.model.OrderEntity;

import org.springframework.stereotype.Component;

@Component
public class OrderEntityDtoMapper {
    public OrderEntityResponse convertToEntityResponse(OrderEntity orderEntity) {
        OrderItemLineDtoMapper mapper = new OrderItemLineDtoMapper();
        return OrderEntityResponse.builder()
                .id(orderEntity.getId())
                .name(orderEntity.getCustomerName())
                .phone(orderEntity.getCustomerPhone())
                .email(orderEntity.getCustomerEmail())
                .customerAddress(orderEntity.getCustomerAddress())
                .billingAddress(orderEntity.getBillingAddress())
                .shippingAddress(orderEntity.getShippingAddress())
                .total(orderEntity.getTotal())
                .status(orderEntity.getStatus().getName())
                .orderItemLineDto(mapper.convertToDto(orderEntity.getOrderItemLines()))
                .build();
    }
}
