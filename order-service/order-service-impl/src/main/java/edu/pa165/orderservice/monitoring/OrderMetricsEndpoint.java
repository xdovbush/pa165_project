package edu.pa165.orderservice.monitoring;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Endpoint(id = "orderMetrics")
public class OrderMetricsEndpoint {
    private final AtomicInteger totalOrders = new AtomicInteger(0);
    private final AtomicInteger ordersLastHour = new AtomicInteger(0);
    private LocalDateTime lastHour = LocalDateTime.now();

    public void incrementOrderCount() {
        totalOrders.incrementAndGet();
        ordersLastHour.incrementAndGet();
    }

    @ReadOperation
    public Map<String, Integer> getOrderMetrics() {
        LocalDateTime now = LocalDateTime.now();
        if (now.minusHours(1).isAfter(lastHour)) {
            // Reset count if more than an hour has passed
            ordersLastHour.set(0);
            lastHour = now;
        }

        Map<String, Integer> metrics = new HashMap<>();
        metrics.put("totalOrders", totalOrders.get());
        metrics.put("ordersLastHour", ordersLastHour.get());
        return metrics;
    }
}
