package edu.pa165.orderservice.repository;

import edu.pa165.orderservice.model.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderEntityRepository extends JpaRepository<OrderEntity, Long> {

    @Query("SELECT o FROM OrderEntity o WHERE o.shippingAddress = :address")
    List<OrderEntity> getOrdersShippedTo(String address);

    @Query("SELECT o FROM OrderEntity o JOIN o.orderItemLines item WHERE item.wineId = :wineId")
    List<OrderEntity> getOrdersWithWine(Long wineId);

}