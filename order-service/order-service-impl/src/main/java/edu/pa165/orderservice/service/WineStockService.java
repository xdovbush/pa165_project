package edu.pa165.orderservice.service;

import edu.pa165.wineinventoryservice.api.WineInventoryServiceApi;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import edu.pa165.wineinventoryservice.dto.WineUpdateRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Objects;


@Service
public class WineStockService {
    private static final String WINE_INVENTORY_SERVICE_ENDPOINT = "http://" + WineInventoryServiceApi.NAME + WineInventoryServiceApi.WINE_ENDPOINT;
    private static final String WINE_INVENTORY_SERVICE_INTERNAL_ENDPOINT = "http://" + WineInventoryServiceApi.NAME + WineInventoryServiceApi.WINE_INTERNAL_ENDPOINT;

    private final WebClient.Builder webClientBuilder;

    public WineStockService(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    public WineResponse updateWineStock(Long wineId, int addedQuantity) {
        WineResponse wineResponse = Objects.requireNonNull(
                webClientBuilder.build()
                .get()
                .uri(WINE_INVENTORY_SERVICE_ENDPOINT + "/{id}", wineId)
                .retrieve()
                .bodyToMono(WineResponse.class)
                .block()
        );

        int originalQuantity = wineResponse.quantityLeft();
        int difference = originalQuantity - addedQuantity;

        WineUpdateRequest request = WineUpdateRequest.builder()
                .quantityLeft(difference)
                .name(wineResponse.name())
                .dryness(wineResponse.dryness())
                .year(wineResponse.year())
                .sugarLevel(wineResponse.sugarLevel())
                .acidity(wineResponse.acidity())
                .alcoholLevel(wineResponse.alcoholLevel())
                .litersInBottle(wineResponse.litersInBottle())
                .batchNumber(wineResponse.batchNumber())
                .pricePerBottle(wineResponse.pricePerBottle())
                .build();

        return webClientBuilder.build()
                .put()
                .uri(WINE_INVENTORY_SERVICE_INTERNAL_ENDPOINT + "/{id}", wineId)
                .body(request, WineUpdateRequest.class)
                .retrieve()
                .bodyToMono(WineResponse.class)
                .block();
    }

    public void updateWineStock(Map<Long, Integer> qtyMap) {
        webClientBuilder.build()
                .put()
                .uri(WINE_INVENTORY_SERVICE_INTERNAL_ENDPOINT + "/quantity")
                .body(Mono.just(qtyMap), Map.class)
                .retrieve()
                .bodyToMono(Void.class)
                .block();
    }
}
