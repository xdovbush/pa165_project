package edu.pa165.orderservice.model;

import edu.pa165.orderservice.enums.OrderStatus;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String customerName;
    private String customerPhone;
    private String customerEmail;
    private String customerAddress;
    private String billingAddress;
    private String shippingAddress;
    private Integer total;
    private OrderStatus status;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<OrderItemLine> orderItemLines;
}
