package edu.pa165.orderservice.facade;

import edu.pa165.orderservice.dto.*;
import edu.pa165.orderservice.service.OrderEntityService;
import edu.pa165.orderservice.service.WineStockService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class OrderEntityFacadeUnitTest {
    private static final Long WINE_ID = 1L;
    private static final Integer ORIGINAL_QUANTITY = 20;
    private static final Integer NEW_QUANTITY = 40;

    private OrderEntityService orderEntityService;
    private WineStockService wineStockService;
    private OrderEntityResponse originalResponse;
    private OrderEntityResponse newResponse;

    private OrderEntityFacade facade;

    @BeforeEach
    void setUp() {
        orderEntityService = mock(OrderEntityService.class);
        wineStockService = mock(WineStockService.class);

        OrderItemLineDto originalLine = OrderItemLineDto.builder()
                .wineId(WINE_ID)
                .quantity(ORIGINAL_QUANTITY)
                .build();
        originalResponse = OrderEntityResponse.builder()
                .orderItemLineDto(List.of(originalLine))
                .build();

        OrderItemLineDto newLine = OrderItemLineDto.builder()
                .wineId(WINE_ID)
                .quantity(NEW_QUANTITY)
                .build();
        newResponse = OrderEntityResponse.builder()
                .orderItemLineDto(List.of(newLine))
                .build();
        OrderEntityUpdateResult newUpdateResult = OrderEntityUpdateResult.builder()
                .orderEntityResponse(newResponse)
                .wineIdQuantityDifferenceMap(Map.of(WINE_ID, NEW_QUANTITY - ORIGINAL_QUANTITY))
                .build();

        when(orderEntityService.createOrderEntity(any())).thenReturn(newResponse);
        when(orderEntityService.updateOrderEntity(any(), any())).thenReturn(newUpdateResult);
        when(orderEntityService.getOrderEntity(any())).thenReturn(originalResponse);
        when(orderEntityService.getAllOrderEntities()).thenReturn(List.of(originalResponse, newResponse));

        facade = new OrderEntityFacade(orderEntityService, wineStockService);
    }

    @Test
    void createOrderEntity() {
        OrderEntityRequest createRequest = OrderEntityRequest.builder()
                .orderItemLineDto(newResponse.orderItemLineDto())
                .build();
        OrderEntityResponse actualResponse = facade.createOrderEntity(createRequest);

        assertThat(actualResponse).isSameAs(newResponse);

        verify(orderEntityService).createOrderEntity(createRequest);
        verify(wineStockService).updateWineStock(Map.of(WINE_ID, NEW_QUANTITY));
        verifyNoMoreInteractions(orderEntityService, wineStockService);
    }

    @Test
    void updateOrderEntity() {
        OrderEntityUpdateRequest updateRequest = OrderEntityUpdateRequest.builder().build();
        OrderEntityResponse actualResponse = facade.updateOrderEntity(WINE_ID, updateRequest);

        assertThat(actualResponse).isSameAs(newResponse);

        verify(orderEntityService).updateOrderEntity(WINE_ID, updateRequest);
        verify(wineStockService).updateWineStock(Map.of(WINE_ID, NEW_QUANTITY - ORIGINAL_QUANTITY));
        verifyNoMoreInteractions(orderEntityService, wineStockService);
    }

    @Test
    void deleteOrderEntity() {
        facade.deleteOrderEntity(WINE_ID);

        verify(orderEntityService).getOrderEntity(WINE_ID);
        verify(wineStockService).updateWineStock(Map.of(WINE_ID, - ORIGINAL_QUANTITY));
        verify(orderEntityService).deleteOrderEntity(WINE_ID);
        verifyNoMoreInteractions(orderEntityService, wineStockService);
    }

    @Test
    void getOrderEntity() {
        OrderEntityResponse actualResponse = facade.getOrderEntity(WINE_ID);

        assertThat(actualResponse).isSameAs(originalResponse);

        verify(orderEntityService).getOrderEntity(WINE_ID);
        verifyNoMoreInteractions(orderEntityService, wineStockService);
    }

    @Test
    void getAllOrderEntities() {
        List<OrderEntityResponse> actualResponse = facade.getAllOrderEntities();

        assertThat(actualResponse).containsExactlyInAnyOrder(originalResponse, newResponse);

        verify(orderEntityService).getAllOrderEntities();
        verifyNoMoreInteractions(orderEntityService, wineStockService);
    }
}