package edu.pa165.orderservice.e2e;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.pa165.orderservice.TestUtils;
import edu.pa165.orderservice.dto.OrderEntityRequest;
import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.dto.OrderEntityUpdateRequest;
import edu.pa165.orderservice.dto.OrderItemLineDto;
import edu.pa165.orderservice.enums.OrderStatus;
import edu.pa165.orderservice.mapping.OrderItemLineDtoMapper;
import edu.pa165.orderservice.model.OrderEntity;
import edu.pa165.orderservice.model.OrderItemLine;
import edu.pa165.orderservice.repository.OrderEntityRepository;
import edu.pa165.orderservice.service.WineStockService;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static edu.pa165.orderservice.TestUtils.createDummyOrderItemLine;
import static edu.pa165.orderservice.api.OrderServiceApi.ORDER_ENDPOINT;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OrderEntityCrudFullStackTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private OrderEntityRepository orderEntityRepository;

    @MockBean
    private WineStockService wineStockService;

    @Autowired
    private OrderItemLineDtoMapper orderItemLineDtoMapper;

    @BeforeAll
    void initialize() {
        OrderEntity newOrderEntity = TestUtils.createDummyOrderEntity();
        orderEntityRepository.save(newOrderEntity);

        when(wineStockService.updateWineStock(anyLong(), anyInt())).thenReturn(WineResponse.builder().build());
    }

    @AfterAll
    void cleanUp() {
        orderEntityRepository.deleteAll();
    }

    @Test
    @Transactional
    public void testCreateOrderEntity() {
        OrderEntityRequest request = OrderEntityRequest.builder()
                .name("John Doe")
                .email("john@doe.fr")
                .phone("1234567890")
                .customerAddress("123 Main St")
                .billingAddress("123 Main St")
                .shippingAddress("123 Main St")
                .total(100)
                .orderItemLineDto(List.of())
                .build();

        MockHttpServletResponse response = null;
        try {
            response = mockMvc.perform(post(ORDER_ENDPOINT)
                            .content(convertRequestToJson(request))
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isCreated())
                    .andReturn()
                    .getResponse();
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
        }

        assertNotNull(response);

        OrderEntityResponse orderEntityResponse = convertServletResponseToDto(response);
        OrderEntity expectedOrderEntity = orderEntityRepository.findById(orderEntityResponse.id()).orElse(null);

        assertSoftly(softly -> {
            softly.assertThat(orderEntityResponse.name())
                    .isEqualTo(request.name());
            softly.assertThat(orderEntityResponse.orderItemLineDto())
                    .containsExactlyInAnyOrderElementsOf(request.orderItemLineDto());
        });

        assertNotNull(expectedOrderEntity);
        List<OrderItemLineDto> orderItemLineDto = orderItemLineDtoMapper.convertToDto(expectedOrderEntity.getOrderItemLines());

        assertSoftly(softly -> {
            softly.assertThat(expectedOrderEntity.getCustomerName())
                    .isEqualTo(request.name());
            softly.assertThat(orderItemLineDto)
                    .containsExactlyInAnyOrderElementsOf(request.orderItemLineDto());
        });

    }

    @Test
    public void testUpdateOrderEntity() throws Exception {
        Long orderId = 1L;

        OrderEntityUpdateRequest request = OrderEntityUpdateRequest.builder()
                .name("John Doe")
                .email("john@doe.com")
                .phone("1234567890")
                .customerAddress("123 Main St")
                .billingAddress("123 Main St")
                .shippingAddress("123 Main St")
                .total(100)
                .orderItemLineDto(Collections.singletonList(TestUtils.createDummyOrderItemLineDto()))
                .build();

        MockHttpServletResponse response = mockMvc.perform(put(ORDER_ENDPOINT + "/{id}", orderId)
                        .content(convertRequestToJson(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        OrderEntityResponse orderEntityResponse = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            softly.assertThat(orderEntityResponse.name())
                    .isEqualTo(request.name());
            softly.assertThat(orderEntityResponse.orderItemLineDto())
                    .containsExactlyInAnyOrderElementsOf(request.orderItemLineDto());
        });

    }

    @Test
    public void testGetOrderEntity() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get(ORDER_ENDPOINT + "/{id}", TestUtils.ORDER_ID)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        OrderEntityResponse orderEntityResponse = convertServletResponseToDto(response);

        assertSoftly(softly -> {
            softly.assertThat(orderEntityResponse.name())
                    .isEqualTo(TestUtils.CUSTOMER_NAME);
            softly.assertThat(orderEntityResponse.phone())
                    .isEqualTo(TestUtils.CUSTOMER_PHONE);
        });

    }

    @Test
    public void testGetAllOrderEntity() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get(ORDER_ENDPOINT)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        OrderEntityResponse[] orderEntityResponses = convertServletResponseToDtoArray(response);

        OrderEntityResponse orderEntityResponse = orderEntityResponses[0];

        assertSoftly(softly -> {
            softly.assertThat(orderEntityResponse.name())
                    .isEqualTo(TestUtils.CUSTOMER_NAME);
            softly.assertThat(orderEntityResponse.phone())
                    .isEqualTo(TestUtils.CUSTOMER_PHONE);
        });

    }

    @Test
    public void testDeleteOrderEntity() throws Exception {
        Long orderId = 2L;
        OrderItemLine line = createDummyOrderItemLine();

        OrderEntity orderEntity = OrderEntity.builder()
                .id(orderId)
                .customerName("name")
                .total(1)
                .status(OrderStatus.OPENED)
                .orderItemLines(List.of(line))
                .build();

        orderEntityRepository.save(orderEntity);

        MockHttpServletResponse response = mockMvc.perform(delete(ORDER_ENDPOINT + "/{id}", orderId)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertNotNull(response);

        assertTrue(orderEntityRepository.findById(orderId).isEmpty());
    }

    private String convertRequestToJson(Object request) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.writeValueAsString(request);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return "";
        }
    }

    private OrderEntityResponse convertServletResponseToDto(MockHttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), OrderEntityResponse.class);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return OrderEntityResponse.builder().build();
        }
    }

    private OrderEntityResponse[] convertServletResponseToDtoArray(MockHttpServletResponse response) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), OrderEntityResponse[].class);
        } catch (Exception e) {
            Assertions.fail(e.getMessage(), e);
            return new OrderEntityResponse[0];
        }
    }

}
