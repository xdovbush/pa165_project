package edu.pa165.orderservice.mapping;

import edu.pa165.orderservice.dto.OrderItemLineDto;
import edu.pa165.orderservice.model.OrderItemLine;
import org.junit.jupiter.api.Test;

import static edu.pa165.orderservice.TestUtils.createDummyOrderItemLine;
import static edu.pa165.orderservice.TestUtils.createDummyOrderItemLineDto;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

class OrderItemLineDtoMapperUnitTest {
    private final OrderItemLineDtoMapper mapper = new OrderItemLineDtoMapper();

    @Test
    void convertToDto() {
        OrderItemLine line = createDummyOrderItemLine();

        OrderItemLineDto lineDto = mapper.convertToDto(line);

        assertSoftly(softly -> {
            softly.assertThat(lineDto.wineId()).as("Wine id").isEqualTo(line.getWineId());
            softly.assertThat(lineDto.quantity()).as("Quantity").isEqualTo(line.getQuantity());
            softly.assertThat(lineDto.price()).as("Price").isEqualTo(line.getUnitPrice());
            softly.assertThat(lineDto.description()).as("Description").isEqualTo(line.getDescription());
        });
    }

    @Test
    void convertToEntity() {
        OrderItemLineDto lineDto = createDummyOrderItemLineDto();

        OrderItemLine line = mapper.convertToEntity(lineDto);

        assertSoftly(softly -> {
            softly.assertThat(line.getWineId()).as("Wine id").isEqualTo(lineDto.wineId());
            softly.assertThat(line.getQuantity()).as("Quantity").isEqualTo(lineDto.quantity());
            softly.assertThat(line.getUnitPrice()).as("Price").isEqualTo(lineDto.price());
            softly.assertThat(line.getDescription()).as("Description").isEqualTo(lineDto.description());
        });
    }
}