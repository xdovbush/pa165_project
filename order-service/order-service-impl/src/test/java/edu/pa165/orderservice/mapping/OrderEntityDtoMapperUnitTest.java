package edu.pa165.orderservice.mapping;

import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.dto.OrderItemLineDto;
import edu.pa165.orderservice.model.OrderEntity;
import edu.pa165.orderservice.model.OrderItemLine;
import org.junit.jupiter.api.Test;

import java.util.List;

import static edu.pa165.orderservice.TestUtils.createDummyOrderEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

class OrderEntityDtoMapperUnitTest {

    private final OrderEntityDtoMapper mapper = new OrderEntityDtoMapper();


    @Test
    void convertToEntityResponse() {
        OrderEntity entity = createDummyOrderEntity();

        OrderEntityResponse response = mapper.convertToEntityResponse(entity);

        // Assert header
        assertSoftly(softly -> {
            softly.assertThat(response.id()).as("Order id")
                    .isEqualTo(entity.getId());
            softly.assertThat(response.name()).as("Customer name")
                    .isEqualTo(entity.getCustomerName());
            softly.assertThat(response.phone()).as("Customer phone")
                    .isEqualTo(entity.getCustomerPhone());
            softly.assertThat(response.email()).as("Customer email")
                    .isEqualTo(entity.getCustomerEmail());
            softly.assertThat(response.customerAddress()).as("Customer address")
                    .isEqualTo(entity.getCustomerAddress());
            softly.assertThat(response.billingAddress()).as("Billing address")
                    .isEqualTo(entity.getBillingAddress());
            softly.assertThat(response.shippingAddress()).as("Shipping address")
                    .isEqualTo(entity.getShippingAddress());
            softly.assertThat(response.total()).as("Total")
                    .isEqualTo(entity.getTotal());
            softly.assertThat(response.status()).as("Order status")
                    .isEqualTo(entity.getStatus().getName());
        });

        // Assert line
        List<OrderItemLineDto> lines = response.orderItemLineDto();
        assertThat(lines).hasSize(1);
        OrderItemLineDto lineDto = lines.get(0);
        OrderItemLine line = entity.getOrderItemLines().get(0);
        assertSoftly(softly -> {
            softly.assertThat(lineDto.wineId()).as("Wine id").isEqualTo(line.getWineId());
            softly.assertThat(lineDto.quantity()).as("Quantity").isEqualTo(line.getQuantity());
            softly.assertThat(lineDto.price()).as("Price").isEqualTo(line.getUnitPrice());
            softly.assertThat(lineDto.description()).as("Description").isEqualTo(line.getDescription());
        });
    }
}