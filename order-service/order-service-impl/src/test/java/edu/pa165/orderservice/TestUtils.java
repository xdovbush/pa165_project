package edu.pa165.orderservice;

import edu.pa165.orderservice.dto.OrderItemLineDto;
import edu.pa165.orderservice.enums.OrderStatus;
import edu.pa165.orderservice.model.OrderEntity;
import edu.pa165.orderservice.model.OrderItemLine;

import java.util.List;

public class TestUtils {
    public static final Long ORDER_ID = 1L;
    public static final Long ORDER_LINE_ID = 2L;
    public static final Long WINE_ID = 54L;
    public static final String CUSTOMER_NAME = "Test name";
    public static final String CUSTOMER_PHONE = "+420608012345";
    public static final String CUSTOMER_EMAIL = "test@test.email";
    public static final String CUSTOMER_ADDRESS = "Bozetechova 1/2";
    public static final String BILLING_ADDRESS = "Bozetechova 2/2";
    public static final String SHIPPING_ADDRESS = "Bozetechova 3/2";
    public static final String DESCRIPTION = "Test description";
    public static final Integer TOTAL = 50;
    public static final Integer QUANTITY = 25;
    public static final Integer PRICE = 2;
    public static final OrderStatus ORDER_STATUS = OrderStatus.PAID;

    public static OrderEntity createDummyOrderEntity() {
        OrderItemLine line = createDummyOrderItemLine();
        return OrderEntity.builder()
                .id(ORDER_ID)
                .customerName(CUSTOMER_NAME)
                .customerPhone(CUSTOMER_PHONE)
                .customerEmail(CUSTOMER_EMAIL)
                .customerAddress(CUSTOMER_ADDRESS)
                .billingAddress(BILLING_ADDRESS)
                .shippingAddress(SHIPPING_ADDRESS)
                .total(TOTAL)
                .status(ORDER_STATUS)
                .orderItemLines(List.of(line))
                .build();
    }

    public static OrderItemLine createDummyOrderItemLine() {
        return OrderItemLine.builder()
                .id(ORDER_LINE_ID)
                .wineId(WINE_ID)
                .quantity(QUANTITY)
                .unitPrice(PRICE)
                .description(DESCRIPTION)
                .build();
    }

    public static OrderItemLineDto createDummyOrderItemLineDto() {
        return OrderItemLineDto.builder()
                .wineId(WINE_ID)
                .quantity(QUANTITY)
                .price(PRICE)
                .description(DESCRIPTION)
                .build();
    }

}
