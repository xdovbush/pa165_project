package edu.pa165.orderservice.repository;

import edu.pa165.orderservice.TestUtils;
import edu.pa165.orderservice.model.OrderEntity;
import edu.pa165.orderservice.model.OrderItemLine;
import edu.pa165.orderservice.service.WineStockService;
import edu.pa165.wineinventoryservice.dto.WineResponse;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@DirtiesContext
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OrderEntityRepositoryTest {
    @Autowired
    private OrderEntityRepository orderEntityRepository;

    @MockBean
    private WineStockService wineStockService;

    @BeforeAll
    public void setUp() {
        OrderEntity newOrderEntity = TestUtils.createDummyOrderEntity();
        orderEntityRepository.save(newOrderEntity);

        when(wineStockService.updateWineStock(anyLong(), anyInt())).thenReturn(WineResponse.builder().build());
    }

    @AfterAll
    public void tearDown() {
        orderEntityRepository.deleteAll();
    }

    @Test
    public void testGetOrdersShippedTo() {
        String address = "custom address";
        String customerName = "name living at custom address";
        OrderEntity newOrderEntity = OrderEntity.builder()
                .shippingAddress(address)
                .customerName(customerName)
                .build();

        orderEntityRepository.save(newOrderEntity);

        List<OrderEntity> orders = orderEntityRepository.getOrdersShippedTo(address);

        assert(orders.size() == 1);

        OrderEntity order = orders.get(0);

        Assertions.assertAll(
                () -> Assertions.assertEquals(order.getShippingAddress(), address),
                () -> Assertions.assertEquals(order.getCustomerName(), customerName)
        );
    }

    @Test
    public void testGetOrdersWithWine() {
        String address = "address with wine 34";
        String customerName = "name living at address with wine 34";

        OrderItemLine orderItemLine = OrderItemLine.builder()
                .wineId(34L)
                .quantity(56)
                .unitPrice(12)
                .description("Nice order")
                .build();
        OrderItemLine otherItemLine = TestUtils.createDummyOrderItemLine();
        otherItemLine.setId(null);

        OrderEntity newOrderEntity = OrderEntity.builder()
                .shippingAddress(address)
                .customerName(customerName)
                .orderItemLines(List.of(orderItemLine, otherItemLine))
                .build();

        orderEntityRepository.save(newOrderEntity);

        List<OrderEntity> orders = orderEntityRepository.getOrdersWithWine(34L);

        assert(orders.size() == 1);

        OrderEntity order = orders.get(0);

        Assertions.assertAll(
                () -> Assertions.assertEquals(order.getShippingAddress(), address),
                () -> Assertions.assertEquals(order.getCustomerName(), customerName)
        );
    }
}
