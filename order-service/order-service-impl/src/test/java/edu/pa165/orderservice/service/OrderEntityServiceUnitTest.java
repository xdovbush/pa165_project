package edu.pa165.orderservice.service;

import edu.pa165.orderservice.dto.OrderEntityRequest;
import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.dto.OrderEntityUpdateRequest;
import edu.pa165.orderservice.dto.OrderEntityUpdateResult;
import edu.pa165.orderservice.enums.OrderStatus;
import edu.pa165.orderservice.mapping.OrderEntityDtoMapper;
import edu.pa165.orderservice.mapping.OrderItemLineDtoMapper;
import edu.pa165.orderservice.model.OrderEntity;
import edu.pa165.orderservice.monitoring.OrderMetricsEndpoint;
import edu.pa165.orderservice.repository.OrderEntityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class OrderEntityServiceUnitTest {
    private static final Long ORDER_ID = 1L;

    private OrderEntityRepository repository;
    private OrderItemLineDtoMapper lineMapper;

    private OrderEntityDtoMapper orderMapper;
    private OrderMetricsEndpoint orderMetricsEndpoint;

    private OrderEntity oldEntity;
    private OrderEntity newEntity;
    private OrderEntityResponse response;
    private OrderEntityUpdateResult updateResult;

    private OrderEntityServiceImpl service;

    @BeforeEach
    void setUp() {
        oldEntity = new OrderEntity();
        newEntity = new OrderEntity();
        oldEntity.setId(ORDER_ID);
        newEntity.setId(ORDER_ID);

        response = OrderEntityResponse.builder().build();
        updateResult = OrderEntityUpdateResult.builder()
                .orderEntityResponse(response)
                .wineIdQuantityDifferenceMap(Map.of())
                .build();

        repository = mock(OrderEntityRepository.class);
        lineMapper = mock(OrderItemLineDtoMapper.class);
        orderMapper = mock(OrderEntityDtoMapper.class);
        orderMetricsEndpoint = mock(OrderMetricsEndpoint.class);

        when(repository.findById(ORDER_ID)).thenReturn(Optional.of(oldEntity));
        when(repository.findAll()).thenReturn(List.of(oldEntity, newEntity));

        when(orderMapper.convertToEntityResponse(any())).thenReturn(response);

        service = new OrderEntityServiceImpl(repository, lineMapper, orderMapper, orderMetricsEndpoint);
    }

    @Test
    void createOrderEntity() {
        OrderEntityResponse actualResponse = service.createOrderEntity(OrderEntityRequest.builder().build());

        assertThat(actualResponse).isSameAs(response);

        verify(lineMapper).convertToEntity(isNull(List.class));
        verify(repository).save(any());
        verify(orderMapper).convertToEntityResponse(any());
        verify(orderMetricsEndpoint).incrementOrderCount();
        verifyNoMoreInteractions(repository, lineMapper, orderMapper);
    }

    @Test
    void deleteOrderEntity() {
        service.deleteOrderEntity(ORDER_ID);

        verify(repository).deleteById(ORDER_ID);
        verifyNoMoreInteractions(repository, lineMapper, orderMapper);
    }

    @Test
    void updateOrderEntity() {
        OrderEntityUpdateResult actualResponse = service.updateOrderEntity(ORDER_ID,
                OrderEntityUpdateRequest.builder()
                        .build()
        );

        assertEquals(updateResult, actualResponse);

        verify(repository).findById(ORDER_ID);
        verify(lineMapper).convertToEntity(isNull(List.class));
        verify(repository).save(any());
        verify(orderMapper).convertToEntityResponse(any());
        verifyNoMoreInteractions(repository, lineMapper, orderMapper);
    }

    @Test
    void getOrderEntity() {
        OrderEntityResponse actualResponse = service.getOrderEntity(ORDER_ID);

        assertThat(actualResponse).isSameAs(response);

        verify(repository).findById(ORDER_ID);
        verify(orderMapper).convertToEntityResponse(any());
        verifyNoMoreInteractions(repository, lineMapper, orderMapper);
    }

    @Test
    void getAllOrderEntities() {
        List<OrderEntityResponse> actualResponse = service.getAllOrderEntities();

        assertThat(actualResponse).containsExactlyInAnyOrder(response, response);

        verify(repository).findAll();
        verify(orderMapper).convertToEntityResponse(oldEntity);
        verify(orderMapper).convertToEntityResponse(newEntity);
        verifyNoMoreInteractions(repository, lineMapper, orderMapper);
    }

    @Test
    void changeStatusOrderEntity() {
        OrderEntityResponse actualResponse = service.changeStatusOrderEntity(ORDER_ID, OrderStatus.OPENED);

        assertThat(actualResponse).isSameAs(response);

        verify(repository).findById(ORDER_ID);
        verify(repository).save(oldEntity);
        verify(orderMapper).convertToEntityResponse(oldEntity);
        verifyNoMoreInteractions(repository, lineMapper, orderMapper);
    }
}