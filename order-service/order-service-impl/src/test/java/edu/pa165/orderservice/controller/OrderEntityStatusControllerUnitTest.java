package edu.pa165.orderservice.controller;

import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.enums.OrderStatus;
import edu.pa165.orderservice.service.OrderEntityService;
import edu.pa165.orderservice.service.OrderEntityStatusService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class OrderEntityStatusControllerUnitTest {
    private static final Long ORDER_ID = 1L;

    private OrderEntityService service;
    private OrderEntityStatusService statusService;
    private OrderEntityResponse response;

    private OrderEntityStatusController controller;

    @BeforeEach
    void setUp() {
        response = OrderEntityResponse.builder().build();

        service = mock(OrderEntityService.class);
        statusService = mock(OrderEntityStatusService.class);

        when(service.changeStatusOrderEntity(any(),any())).thenReturn(response);

        controller = new OrderEntityStatusController(service, statusService);
    }

    @Test
    void updateOrderEntity() {
        OrderStatus status = OrderStatus.OPENED;

        OrderEntityResponse actualResponse = controller.updateOrderEntity(ORDER_ID, status.getName());

        assertThat(actualResponse).isSameAs(response);

        verify(service).changeStatusOrderEntity(ORDER_ID, status);
        verify(statusService).updateWineStockIfNeeded(status, response);
        verifyNoMoreInteractions(service, statusService);
    }
}