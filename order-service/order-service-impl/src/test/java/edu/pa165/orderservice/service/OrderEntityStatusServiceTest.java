package edu.pa165.orderservice.service;

import edu.pa165.orderservice.dto.OrderEntityResponse;
import edu.pa165.orderservice.dto.OrderItemLineDto;
import edu.pa165.orderservice.enums.OrderStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static edu.pa165.orderservice.TestUtils.createDummyOrderItemLineDto;
import static org.mockito.Mockito.*;

class OrderEntityStatusServiceTest {
    private WineStockService wineStockService;

    private OrderEntityStatusService service;

    @BeforeEach
    void setUp() {
        wineStockService = mock(WineStockService.class);

        service = new OrderEntityStatusService(wineStockService);
    }

    @Test
    void updateWineStockIfNeeded() {
        OrderItemLineDto lineDto = createDummyOrderItemLineDto();
        OrderEntityResponse response = OrderEntityResponse.builder()
                .status(OrderStatus.OPENED.name())
                .orderItemLineDto(List.of(lineDto))
                .build();

        service.updateWineStockIfNeeded(OrderStatus.CANCELED, response);

        verify(wineStockService).updateWineStock(lineDto.wineId(), -lineDto.quantity());
        verifyNoMoreInteractions(wineStockService);
    }
}