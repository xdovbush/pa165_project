package edu.pa165.orderservice.api;

/**
 * This is exposed class that can be used in other modules.
 * Please do not put in API module internal classes that should not be used in other services.
 * Such exposed classes should be used only to keep things consistent throughout application.
 * So under this category are: names, endpoints, request/response DTOs.
 */
public interface OrderServiceApi {
    String NAME = "order-service";
    String ORDER_ENDPOINT = "/api/orders";
    String ORDER_STATUS_ENDPOINT = "/api/order-statuses";
    String DATA_ENDPOINT = "/internal/data";
}
