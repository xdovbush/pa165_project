package edu.pa165.orderservice.dto;

import lombok.Builder;

import java.util.List;


@Builder
public record OrderEntityRequest(
        String name,
        String phone,
        String email,
        String customerAddress,
        String billingAddress,
        String shippingAddress,
        Integer total,
        List<OrderItemLineDto> orderItemLineDto
) {
}
