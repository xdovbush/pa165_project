package edu.pa165.orderservice.dto;

import lombok.Builder;


@Builder
public record OrderItemLineDto(
        Long wineId,
        Integer quantity,
        Integer price,
        String description
) {
}