package edu.pa165.orderservice.dto;

import lombok.Builder;
import java.util.Map;

@Builder
public record OrderEntityUpdateResult(
        OrderEntityResponse orderEntityResponse,
        Map<Long, Integer> wineIdQuantityDifferenceMap
) {
}
