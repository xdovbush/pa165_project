package edu.pa165.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * API Gateway server runner
 *
 * @author andrii.dovbush
 * @since 2024.03.10
 */
@SpringBootApplication
public class ApiGatewayServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayServerApplication.class, args);
    }
}
