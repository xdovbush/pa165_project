package edu.pa165.gateway.config;

import edu.pa165.harvestservice.api.HarvestServiceApi;
import edu.pa165.orderservice.api.OrderServiceApi;
import edu.pa165.productionservice.api.ProductionServiceApi;
import edu.pa165.wineinventoryservice.api.WineInventoryServiceApi;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    // Region for Admin (test_1) permissions
    private final static String[] HTTP_GET_TEST_1 = {
            "/" + ProductionServiceApi.NAME + "/**",
            "/" + HarvestServiceApi.NAME + "/**",
            "/" + OrderServiceApi.NAME + "/**",
            "/*/internal/**"
    };
    private final static String[] HTTP_POST_TEST_1 = {
            "/" + ProductionServiceApi.NAME + "/**",
            "/" + HarvestServiceApi.NAME + "/**",
            "/" + WineInventoryServiceApi.NAME + "/**",
            "/*/internal/**"
    };
    private final static String[] HTTP_PUT_TEST_1 = {
            "/" + ProductionServiceApi.NAME + "/**",
            "/" + HarvestServiceApi.NAME + "/**",
            "/" + OrderServiceApi.NAME + "/**",
            "/" + WineInventoryServiceApi.NAME + "/**",
            "/*/internal/**"
    };
    private final static String[] HTTP_DELETE_TEST_1 = {
            "/" + ProductionServiceApi.NAME + "/**",
            "/" + HarvestServiceApi.NAME + "/**",
            "/" + OrderServiceApi.NAME + "/**",
            "/" + WineInventoryServiceApi.NAME + "/**",
            "/*/internal/**"
    };

    // Region for Buyer (test_2) permissions
    private final static String[] HTTP_GET_TEST_2 = {
            "/" + WineInventoryServiceApi.NAME + "/**"
    };
    private final static String[] HTTP_POST_TEST_2 = {
            "/" + OrderServiceApi.NAME + "/**"
    };

    @Bean
    @ConditionalOnProperty(prefix = "custom", name = "ignore-auth", havingValue = "false", matchIfMissing = true)
    SecurityWebFilterChain securityFilterChain(ServerHttpSecurity http) {
        return http.csrf(ServerHttpSecurity.CsrfSpec::disable)
                .authorizeExchange(exchange -> exchange
                                .pathMatchers("/*/actuator/**").permitAll()
                                .pathMatchers(HttpMethod.GET, HTTP_GET_TEST_1).hasAuthority("SCOPE_test_1")
                                .pathMatchers(HttpMethod.POST, HTTP_POST_TEST_1).hasAuthority("SCOPE_test_1")
                                .pathMatchers(HttpMethod.PUT, HTTP_PUT_TEST_1).hasAuthority("SCOPE_test_1")
                                .pathMatchers(HttpMethod.DELETE, HTTP_DELETE_TEST_1).hasAuthority("SCOPE_test_1")
                                .pathMatchers(HttpMethod.GET, HTTP_GET_TEST_2).hasAuthority("SCOPE_test_2")
                                .pathMatchers(HttpMethod.POST, HTTP_POST_TEST_2).hasAuthority("SCOPE_test_2")
                                .anyExchange().permitAll()
                )
                .oauth2ResourceServer(oauth -> oauth.opaqueToken(Customizer.withDefaults()))
                .build();
    }

    // Credits go to team yellow for the @ConditionalOnProperty. It is a wonderful idea, I appreciate that.
    // Could be run as:
    // mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dspring.sql.init.mode=always -Dcustom.ignore-auth=true"
    @ConditionalOnProperty(prefix = "custom", name = "ignore-auth", havingValue = "true")
    @Bean
    SecurityWebFilterChain securityFilterChainIgnore(ServerHttpSecurity http) {
        return http.csrf(ServerHttpSecurity.CsrfSpec::disable)
                .authorizeExchange(x -> x.anyExchange().permitAll())
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
                .build();
    }
}
