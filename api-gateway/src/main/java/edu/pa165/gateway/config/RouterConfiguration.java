package edu.pa165.gateway.config;

import edu.pa165.productionservice.api.ProductionServiceApi;
import edu.pa165.harvestservice.api.HarvestServiceApi;
import edu.pa165.wineinventoryservice.api.WineInventoryServiceApi;
import edu.pa165.orderservice.api.OrderServiceApi;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.reactive.ServerHttpRequest;

import java.util.List;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.addOriginalRequestUrl;

@Configuration
public class RouterConfiguration {

    private static final List<String> services = List.of(
            ProductionServiceApi.NAME,
            HarvestServiceApi.NAME,
            WineInventoryServiceApi.NAME,
            OrderServiceApi.NAME
    );

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routeBuilder = routeLocatorBuilder.routes();
        for(String service: services) {
            routeBuilder= attachRouteForApiService(routeBuilder, service);
        }
        return routeBuilder.build();
    }

    private static RouteLocatorBuilder.Builder attachRouteForApiService(
            RouteLocatorBuilder.Builder builder,
            String serviceName
    ) {
        return builder.route(serviceName + "_internal",
                    route -> route.path("/" + serviceName + "/internal/**")
                            .filters(f -> f.filter((exchange, chain) -> {
                                ServerHttpRequest req = exchange.getRequest();
                                addOriginalRequestUrl(exchange, req.getURI());
                                String path = req.getURI().getRawPath();
                                String newPath = path.replaceAll(
                                        "/" + serviceName + "/internal/(?<endpoint>.*)",
                                        "/internal/${endpoint}"
                                );
                                ServerHttpRequest request = req.mutate().path(newPath).build();
                                exchange.getAttributes().put(GATEWAY_REQUEST_URL_ATTR, request.getURI());
                                return chain.filter(exchange.mutate().request(request).build());
                            }))
                            .uri("lb://"+ serviceName)
                )
                .route(serviceName + "_actuator",
                        route -> route.path("/" + serviceName + "/actuator/**")
                                .filters(f -> f.filter((exchange, chain) -> {
                                    ServerHttpRequest req = exchange.getRequest();
                                    addOriginalRequestUrl(exchange, req.getURI());
                                    String path = req.getURI().getRawPath();
                                    String newPath = path.replaceAll(
                                            "/" + serviceName + "/actuator/(?<endpoint>.*)",
                                            "/actuator/${endpoint}"
                                    );
                                    ServerHttpRequest request = req.mutate().path(newPath).build();
                                    exchange.getAttributes().put(GATEWAY_REQUEST_URL_ATTR, request.getURI());
                                    return chain.filter(exchange.mutate().request(request).build());
                                }))
                                .uri("lb://"+ serviceName)
                )
                .route(serviceName,
                    route -> route.path("/" + serviceName + "/**")
                            .filters(f -> f.filter((exchange, chain) -> {
                                ServerHttpRequest req = exchange.getRequest();
                                addOriginalRequestUrl(exchange, req.getURI());
                                String path = req.getURI().getRawPath();
                                String newPath = path.replaceAll(
                                        "/" + serviceName + "/(?<endpoint>.*)",
                                        "/api/${endpoint}"
                                );
                                ServerHttpRequest request = req.mutate().path(newPath).build();
                                exchange.getAttributes().put(GATEWAY_REQUEST_URL_ATTR, request.getURI());
                                return chain.filter(exchange.mutate().request(request).build());
                            }))
                            .uri("lb://"+ serviceName)
                );
    }
}
