# Winery management system
Winery management system is a universal solution for winery stock management and sales.
It supports not only containing the info about existing wine bottles but also allows user to manage production process and sales.
## Purpose
This system is developed to automate standard routines of wine production and sales.
## Team
- Andrii Dovbush - team lead, 555097
- Lukáš Havlíček - team member, 553637
- Nicolas Quillien - team member, 556685
- Ivann Vyslanko - team member, 556729
## Functionality
### General
Winery management system allows a person to manage its own winery in a better way.
The main user role can manage the yearly wine harvest, annotating the quality, quantity of different types of grapes.
Then he will manage the wines that can be produced depending on the type of grapes available and other ingredients.
The user can also manage the list of wine bottles sold and stocked and available to buyers.
Buyers can consult the system to buy quantities of wine bottles, also leaving feedback about
the quality of the wine they have bought.

![Use case diagram](diagrams/use_case_diagram.png)

### API gateway endpoints
Following endpoints are available through the IP and port of API gateway, when used:
- `/harvest-management-service/harvests`
  - `GET`, `POST`
  - `/{id}`: `GET`, `PUT`, `DELETE`
- `/harvest-management-service/internal/data`
  - `/clean`: `GET` - cleans DB data
  - `/seed`: `GET` - fills with dummy data
- `/production-service/batches`
  - `GET`, `POST`
  - `/{id}`: `GET`, `PUT`, `DELETE`
  - `/{id}/open`: `POST` - opens prepared wine batch
  - `/{id}/bottle`: `POST` - bottles opened wine batch
- `/production-service/recipes`
  - `GET`, `POST`
  - `/{id}`: `GET`, `PUT`, `DELETE`
- `/production-service/internal/data`
  - `/clean` - cleans DB data
  - `/seed` - fills with dummy data
- `/wine-inventory-service/wines`
  - `GET`
  - `/{id}`: `GET`
- `/wine-inventory-service/internal/wines`
  - `POST`
  - `/{id}`: `PUT`, `DELETE`
  - `/quanity`: `PUT` - bulk update of the wine quantity (used in order service)
- `/wine-inventory-service/reviews`
  - `GET`, `POST`
  - `/{id}`: `GET` 
- `/wine-inventory-service/internal/reviews`
  - `/{id}`: `PUT`, `DELETE`
- `/wine-inventory-service/internal/data`
  - `/clean` - cleans DB data
  - `/seed` - fills with dummy data
- `/order-service/orders`
  - `GET`, `POST`
  - `/{id}`: `GET`, `PUT`, `DELETE`
- `/order-service/order-statuses`
  - `/{id}`: `PUT` - sets order status
- `/order-service/internal/data`
  - `/clean` - cleans DB data
  - `/seed` - fills with dummy data

### Internal service endpoints

Following endpoint are used specifically on each service, so with the respect to service:
- Harvest management service:
    - `/api/harvests`
    - `/internal/data`
- Production service:
    - `/api/batches`
    - `/api/recipes`
    - `/internal/data`
- Wine inventory service:
    - `/api/wines`
    - `/internal/wines`
    - `/api/reviews`
    - `/internal/review`
    - `/internal/data`
- Order service:
    - `/api/orders`
    - `/api/order-statuses`
    - `/internal/data`

## Architecture
Entire reference architecture of the system looks like this:
![System architecture](diagrams/architecture_with_actors.png)

We have separated 4 main layers of our application:
- UI
- API Gateway + Authorization + Token retrieval client
- Business layer
- DB layer

Such architecture makes it easier to swap UIs or connect external services through API gateway. 

As a first step let's focus on existing services on back-end.
### Harvest management service
Harvest management service is used to store and manage data about wine harvests.
Mainly contains CRUD operations for it.

Used request and response DTOs:
![Harvest managements service DTOs](diagrams/harvest-management-service-dtos.png)
### Production service
Production service has several connected functionalities:
- Manage prepared wine batches, based on grapes from harvests.
- Bottle wine batches
- Manage wine recipes

Thus, it is only service that is communicating with two other services (Harvest management service and wine inventory service).
For this service basic validations are implemented, based on production pipeline:
- Create wine batch from grapes from harvests
- Open wine batch 
- Bottle wine batch into a bottles, creating wine stock in wine inventory service.

For changing a status, there are separate sub-endpoints (See [WineBatchController.java](production-service/production-service-impl/src/main/java/edu/pa165/productionservice/controller/winebatch/WineBatchController.java)).

Used request and response DTOs:
![Production service DTOs](diagrams/production-service-dtos.png)
### Wine inventory service
Wine inventory management service is used to store and manage data about available wine bottles and reviews.
Mainly contains CRUD operations for them.

Used request and response DTOs:
![Wine inventory service DTOs](diagrams/wine-inventory-service-dtos.png)
### Order service
Wine inventory management service is used to store and manage data about orders on wine bottles.
Additional business functionality is based on communication with wine inventory service.
Allows you to create and manage order on wine.
Used request and response DTOs:
![Order service DTOs](diagrams/order-service-dtos.png)

### Discovery server (Eureka)
This server is used to register all instances of microservices and can resolve service name into ip address.
So for URL it will be enough to use `http://microservice-name/` in Java code.

It is used to NOT hardcode IP addresses in code.

Also, it provides UI for monitoring active services that you can find at Eureka's page, when the service is running: http://localhost:8761/

### API gateway
This service provides one domain for all microservices. It generates endpoints for each microservice at:
- `localhost:8080` - if ran from IDEA
- `localhost:8181` - if ran inside the Docker container in `all-services` mode (see [Build and deployment](#build-and-deployment) section)

Endpoints are generated simply: each `/api/_endpoint_name_` will generate `/_microservice_name_/_endpoint_name_`.
Yes, `/api` prefix is mandatory, otherwise it won't be generated.
This `_microservice_name_` is defined in API module of each microservice (look for `MICROSERVICE_NAME`).

So example for endpoint defined in controller of microservice `product-service`: `/api/MyEndPoint` - the API gateway will generate endpoint that you can
find at `localhost:8080(8181)/product-service/MyEndPoint`.

### OpenAPI and Swagger
User can access swagger directly: 
- [Harvest management service Swagger UI](http://localhost:9081/swagger-ui/index.html)
- [Order service Swagger UI](http://localhost:9082/swagger-ui/index.html)
- [Production service Swagger UI](http://localhost:9083/swagger-ui/index.html)
- [Wine inventory service Swagger UI](http://localhost:9084/swagger-ui/index.html)

### Prometheus and Grafana
Gathering of the different metrics and its visualisation is done with Prometheus and Grafana.
Both services are deployed with docker-compose files present in this project.
Prometheus is working through API Gateway, so it depends on it (run gateway and Eureka before prometheus).
It can be found on port 9090: http://localhost:9090.

Same with Grafana, exposed port is 3000: http://localhost:3000

### Security and Token retrieval client
As part of the project, security was implemented on API Gateway service, since it is a gatekeeper of the app.
As such, API Gateway is playing role of resource, so the additional client service was added to retrieve a token.

This client is in the module auth-client, and it runs a client on: http://localhost:8084.

## Build and deployment
### Docker
For this project you need to have [Docker Desktop](https://www.docker.com/products/docker-desktop/) installed, because services require connection to Postgres DBs.

### Start
From root package run:
```
mvn clean compile
```
for compilation, or:
```
mvn clean install
```
for compilation, packaging and test verifying.

WARNING: If you want to run services successfully, be sure to run containers with DBs by command called from root package:
```
docker-compose -f docker-compose-only-db.yml up -d
```

After this you will be able to build and run separate modules by: 
```
mvn clean install
mvn spring-boot:run
```
from the packages of each service (`api-gateway`,`discovery-server`,`harvest-management-service`,`order-service`,`prodution-service`,`wine-inventory-service`).

### Run sequence
All business services rely on Discovery Server and API gateway, be sure to run them first.
It is possible to run only separate services, but in that case do not be afraid of Connection error in logs.
It happens because service tries to connect to Discovery Server.

Also, to try each service API you'll need to use port that is randomly generated each time you start the service. 
Port unification is done by API gateway, so if you won't run it - keep it in mind.

Running without API gateway also means that you will need to use internal endpoints (see [Internal service endpoints](#internal-service-endpoints)).

### Full deploy
This project could be fully deployed as well, with creating docker images of each service.

To create docker images be sure to provide your Docker credentials in `~/.m2` like this:

`setting.xml`:
```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/SETTING/1.0.0 https:/maven.apache.org/xsd/settings-1.0.0.xsd">
        <localRepository/>
        <interactiveMode/>
        <offline/>
        <pluginGroups/>
        <servers>
            <server>
                <id>registry.hub.docker.com</id>
                <username>YOUR_USERNAME</username>
                <password>YOUR_PASSWORD</password>
            </server>
        </servers>
        <mirrors/>
        <proxies/>
        <profiles/>
        <activeProfiles/>
</settings>
```

With these settings it should be possible to run jib:dockerBuild maven plugin goal to create docker images.

WARNING: there is known bug with docker auth error. It can be resolved with deleting insides of `~/.docker/config.json` JSON, and running:
```
docker login
```
in terminal after the deletion.

For the full deployment you need to do from project root directory:
- Create docker images with: 
```
mvn clean package jib:dockerBuild
```
- Compose images with:
```
docker-compose -f docker-compose-all-services.yml up -d
```

## Testing scenarios (part of the M3)
Attention: All scenarios were tested with Postman.
Useful links:
- [Eureka server](http://localhost:8761)
- [Auth client](http://localhost:8084)
- [Locust server](http://localhost:8089)
- [Prometheus server](http://localhost:9090)
- [Grafana server](htts://localhost:3000)

### Locust 
To execute locust scenario, first you need to have locust installed, preferably with
pip via `pip3 install locust`. You also need to insert your auth token into the
`TOKEN` variable inside the `showcase/locustfile.py`. After that, navigate into `showcase/` directory and type `locust -H http://localhost`.
A Locust UI will be created on [Locust server](http://localhost:8089) (or specified in the output of the previous command).

### Production Scenario
User registered as `owner` (oid scope: `test_1`) want to create some wine.
It is recommended to clean up the DBs by calling following endpoints with `GET` requests:
- `localhost:8080/harvest-management-service/internal/data/clean`
- `localhost:8080/production-service/internal/data/clean`
- `localhost:8080/wine-inventory-service/internal/data/clean`
- `localhost:8080/order-service/internal/data/clean` - optional (won't be needed in this scenario)

As a first step owner creates new Harvest by sending `POST` request on
harvest management service endpoint `localhost:8080/harvest-management-service/harvests` with JSON:

```
{
  "typeOfGrape": "Chardonnay",
  "year": 2023,
  "harvestDate": "2023-09-15",
  "quantity": 500.0,
  "location": "Napa Valley",
  "quality": "High Quality",
  "additionalNotes": "Sunny weather during harvest",
  "ripenessLevels": "Perfectly Ripe",
  "skinAndSeedMaturity": "Fully Mature",
  "healthOfGrapes": "Excellent"
}
```  

This request will create nice Harvest object on back-end and will be available to see with `GET` request
on `localhost:8080/harvest-management-service/harvests`. By doing that, owner can retrieve generated harvest ID that he will use on next step.

As a second step owner wants to create Wine batch from this Harvest.
He can do this by sending `POST` request on `localhost:8080/production-service/batches` with JSON:

```
{
  "number": 123456,
  "wineType": "Chardonnay Sauvignon",
  "ingredients": {
    "<id_of_the_created_harvest>": 300
  },
  "madeTime": "2023-12-01",
  "litersInBatch": 300.0
}
```

Resulting wine batch can be seen on `localhost:8080/production-service/batches` with `GET` request.
Note that related harvest will have decreased quantity by 300.

Then owner should open the barrels and retrieve info about their sugar levels and accidity.
After that owner can use `POST` request on `localhost:8080/production-service/batches/{wine_batch_id}/open` with JSON to open the batch:

```
{
  "openedTime": "2024-05-10",
  "sugarLevel": 4.5,
  "acidityLevel": 6.8,
  "alcoholLevel": 14.2,
  "dryness": "Dry"
}
```

If the wine is fantastic, it is time to bottle it up.
It can be done by `POST` request on `localhost:8080/production-service/batches/{wine_batch_id}/bottle` with JSON:

```
{
  "bottledTime": "2024-04-20",
  "litersInBottle": 0.75,
  "price": 29.99
}
```

This will create 300/0.75 = 400 bottles of exquisite wine in wine inventory service.
Owner can find it by `GET` request on `localhost:8080/wine-inventory-service/wines`.

### Order scenario
This scenario is for two users, or one user with two scopes: owner (test_1) and buyer (test_2).
Scenario can be done on data from Production scenario, or on data that can be generated by cleaning and seeding the DBs by GET requests on:
- `localhost:8080/wine-inventory-service/internal/data/clean`
- `localhost:8080/order-service/internal/data/clean`
- `localhost:8080/wine-inventory-service/internal/data/seed`
- `localhost:8080/order-service/internal/data/seed`

User with buyer scope can view all wines by GET request on `localhost:8080/wine-inventory-service/wines`.
There user can create a reviews on wines by POST request on `localhost:8080/wine-inventory-service/reviews` with JSON:

```
{
  "rating": 4,
  "comment": "This wine has a rich aroma and a smooth finish.",
  "wineId": <wine_id_of_your_choosing>
}
```

After the buyer has selected wine bottles to buy he can create an order on them
by using POST request on `localhost:8080/order-service/orders` with JSON:

```
{
  "name": "John Doe",
  "phone": "+1234567890",
  "email": "john.doe@example.com",
  "customerAddress": "123 Main St, City, Country",
  "billingAddress": "123 Main St, City, Country",
  "shippingAddress": "456 Oak St, City, Country",
  "total": 250,
  "orderItemLineDto": [
    {
      "wineId": <wine_id_of_your_choosing>,
      "quantity": 2,
      "price": 50,
      "description": "Cabernet Sauvignon 2019"
    },
    {
      "wineId": <wine_id_of_your_choosing>,
      "quantity": 1,
      "price": 150,
      "description": "Chardonnay 2020"
    }
  ]
}
```

After order placement, user with owner scope can process this order by updating the order status by PUT request on `localhost:8080/order-service/order-statuses/{id}` with body:

```
Paid
```

Order status can be: Opened, Paid, Fulfilled, Closed, Canceled.

In such way owner can track orders of his/her customers.
